@echo off 

IF "%1" == "rna" GOTO RunAndroid
IF "%1" == "rni" GOTO RunIos
IF "%1" == "rla" GOTO ReleaseAndroid
IF "%1" == "cla" GOTO CleanAndroid
IF "%1" == "install" GOTO Install
IF "%1" == "up" GOTO Upgrade

:Install
 CALL npm install
 GOTO end

:Upgrade
 CALL react-native upgrade
 GOTO end

:RunAndroid
 CALL react-native run-android
 GOTO end

:RunIos
 CALL react-native run-ios
 GOTO end

:CleanAndroid
 CALL cd android && gradlew clean && cd ..
 GOTO end

:ReleaseAndroid
 CALL cd android && gradlew clean && gradlew assembleRelease && cd ..
 GOTO end

:Debug
 CALL cd android && gradlew clean && gradlew assembleDebug && cd ..
 GOTO end

:end


