import React, { useState } from 'react';
import { StyleSheet, Text, KeyboardAvoidingView, Alert, ToastAndroid } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import FormHeader from '../com/FormHeader';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonPrimary from '../com/ButtonPrimary';

import { Button } from 'react-native-elements';
import { useEform, useSetEform } from '../context/EformContext';
import { useSelector } from 'react-redux';
import { SmsService } from '../rx/service.sms';
import { AcquisitionService } from '../rx/service.acquisition';
import { getDataByField } from '../common/function';
import Config from '../common/config';

const EformCifPks = ({navigation}) => {
    const [loading, setLoading] = useState(false);
    const SESSION = useSelector(store => store.Auth.session)

    const form = useEform()
    const setForm = useSetEform()

    const sendSmsLink = async (data, userId) => {    
        return SmsService.sendSmsLink(data, userId)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return error;
            })
            .finally((res) => {
                return res;
            });
    }

    const insertAcquisitionProspera = async (data, userId, cifCode) => {    
        return AcquisitionService.postAcquisitionProspera(data, userId, cifCode)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return error;
            })
            .finally((res) => {
                return res;
            });
    }

    const getSelectedFromText  = (value, data) => {
        let selected = data.find( element => element['text'].toLowerCase() == value.toLowerCase())

        if (selected != null) return selected.value;
    }

    const submitCifForm = async (data, userId) => {
        const userPhone = await SESSION.user.response_login_by_nik.phone
        const masterRelations = global.MASTER.bankRelations;

        console.log('data relation :', data.Relation);
        console.log('masterRelations :', masterRelations);

        const bankRelations = getSelectedFromText(data.Relation, masterRelations)

        console.log('bankRelations', bankRelations)
        
        let body = {
            "AppAcqReferralBankClerkNoHp": userPhone,
            "AppAcqHp": String(data.Phone_No),
            "AppAcqEmail": data.Email ? data.Email : Config.agentDefaultEmail,
            "AppAcqLatlongBusinessLocation": data.Location,
            "AppAcqOpeningAccountPurpose": data.Purpose_Of_Account,
            "AppAcqBankRelationship": data.Relation,
            "AppAcqIdCardDocPath": data.AppAcqIdCardDocPath,
            "AppAcqSelfieDocPath": data.AppAcqSelfieDocPath,
            "AppAcqSignatureDocPath": '',
            "AppAcqBusinessCertDocPath": data.AppAcqBusinessCertDocPath,
            "AppAcqBitaddDocPath": data.AppAcqBitaddDocPath,
            "AppAcqPaofDocPath": data.AppAcqPaofDocPath,
            "AppAcqNpwpDocPath": data.AppAcqNpwpDocPath,
        };

        console.log('body submit cif form:', body)
        
        const insertData = await insertAcquisitionProspera(body, userId, form.Cif_No)

        return insertData
    }

    const onSendSms = async (data) => {
        setLoading(true);
        const userId = SESSION.user.response_login_by_nik.userId
        
        let body = {
            "phone": String(data.Phone_No)
        };

        const insertData = await submitCifForm(data, userId)
        console.log('status code insert data', insertData)
        
        if (insertData.code) {
            setLoading(false)
            
            Alert.alert(
                'Gagal Input Data',
                `${insertData.message}`,
            )
            setLoading(false)

            return false
        }

        const sendSms = await sendSmsLink(body, userId)
        console.log('send sms', sendSms)

        if (sendSms.code) {
            Alert.alert(
                'Gagal Mengirim SMS',
                `${sendSms.message}`,
            )
    
            setLoading(false)
        } else {
            setLoading(false);
            setForm({})
            // removeDraft(data.Index, '@dataDraft');
            navigation.navigate('EformCifSubmitted')
        }
    }

    const exitToMain = () => {
        setForm({})         
        navigation.navigate('Eform')
    }

    return (
        <View animation="fadeIn" style={styles.container} useNativeDriver>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => exitToMain()}
                closeToSave={true}
            />
            <KeyboardAvoidingView 
                style={styles.bodyContainer} 
                enabled={false} 
                behavior="height"
            >
                <View style={{flex: 1}}>
                    <View style={styles.screenHeader}>
                        <Text style={styles.screenHeaderText}>Kirim Syarat Ketentuan,</Text>
                        <Text style={styles.screenHeaderText}>Dan Surat Perjanjian</Text>
                    </View>
                    <View style={styles.bodyTextContainer}>
                        <Text style={styles.text}>Untuk proses selanjutnya seperti syarat dan ketentuan, dan Surat Perjanjian akan dikirim melalui SMS ke nomor telepon calon Mitra Tepat.</Text>
                    </View>
                    {/* <View style={styles.urlContainer}>
                        <Button 
                            type="clear"
                            titleStyle={styles.url}
                            title="Kirim Ulang" 
                            onPress={() => onSendSms(form)}
                            disabled={loading}
                        />
                    </View> */}
                </View>
                <ButtonPrimary 
                    text="Kirim SMS" 
                    onPress={() => onSendSms(form)} 
                    // onPress={() => navigation.navigate('EformCifSubmitted')} 
                    disabled={loading}
                    loading={loading}
                />
            </KeyboardAvoidingView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    bodyContainer: {
        flex: 1,
        paddingTop: 32,
        paddingBottom: 16,
        paddingHorizontal: 24,
    },
    screenHeader: {
        marginBottom: 32
    },
    screenHeaderText: {
        fontSize: 24,
        lineHeight: 36,
        color: CUSTCOLOR.textDark,
        fontFamily: MARKOT.book
    },
    bodyTextContainer: {
        marginBottom: 40,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
    },
    urlContainer: {
        alignItems: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: NUNITO.extraBold,
        fontSize: 14,
    },
    verifButton: {
        bottom: 16,
        zIndex: 10, 
        borderWidth: 2,
        borderColor: 'red'
    }
});


export default EformCifPks;
