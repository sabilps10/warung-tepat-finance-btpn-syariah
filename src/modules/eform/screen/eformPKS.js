import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import FormHeader from '../com/FormHeader';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import Checkbox from '../com/Checkbox';
import RenderTnc from '../com/RenderTnc';

import { CUSTCOLOR, NUNITO } from '../common/styles';
import { pksKeagenan } from '../texts/pksKeagenan'
import { useEform } from '../context/EformContext';
import { string } from 'prop-types';

const EformPKS = ({route, navigation}) => {
    const form = useEform();
    
    const [check, setCheck] = useState(false)

    const dateToObj = (dateStr) => {
        return new Date(dateStr)
    }

    const getDateDay = (dateStr) => {
        if (dateObj == undefined) {
            return
        }
        
        const dateObj = dateToObj(dateStr)
        const days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
        
        console.log('asdf', dateObj.getDay())
        let day = days[dateObj.getDay()]

        return day
    }

    const getDateDate = (dateStr) => {
        if (dateStr == undefined) {
            return
        }

        const dateObj = dateToObj(dateStr)

        console.log('asdf', dateObj.getDate())
        return dateObj.getDate()
    }

    const getDateMonth = (dateStr) => {
        if (dateStr == undefined) {
            return
        }

        const dateObj = dateToObj(dateStr)
        const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

        return months[dateObj.getMonth()]
    }

    const getDateYear = (dateStr) => {
        if (dateStr == undefined) {
            return
        }

        const dateObj = dateToObj(dateStr)
        console.log('asdf', dateObj.getFullYear())
        return dateObj.getFullYear()
    }

    const renderDate = (dateStr) => {
        if (dateStr == undefined) {
            return
        }

        const dateObj = dateToObj(dateStr)
        console.log('asdf', dateObj)

        return dateObj.toLocaleDateString()
    }
  
    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
        <FormHeader 
            text="Perjanjian Kerjasama"
            onClose={() => navigation.navigate('Eform')}
            closeToSave={true}
        />
        <ScrollView style={{padding: 24}}>
                <View style={{paddingBottom: 61}}>
                <Text style={styles.header}>Perjanjian Kerjasama Penunjukan Mitra Tepat BTPN Wow! Syariah</Text>
                <Text style={styles.header}>Antara</Text>
                <Text style={styles.header}>PT Bank Tabungan Pensiunan Nasional Syariah TBK</Text>
                <Text style={styles.header}>Dan</Text>
                <Text style={styles.header}>{form.Name}</Text>


                <View style={styles.section}>
                    <Text style={styles.tncText}>
                    Pada hari ini <Text style={styles.textBold}>{getDateDay(form.Created_At)}</Text>, tanggal <Text style={styles.textBold}>{getDateDate(form.Created_At)}</Text>, bulan <Text style={styles.textBold}>{getDateMonth(form.Created_At)}</Text> tahun <Text style={styles.textBold}>{getDateYear(form.Created_At)}</Text> ({renderDate(form.Created_At)}) telah dibuat Perjanjian Kerja Sama Penunjukan Mitra Tepat BTPN Wow! Syariah (“Perjanjian”) antara pihak-pihak yang bertandatangan di bawah ini :
                    </Text>
                    <View style={styles.verseRow}>
                        <Text style={[styles.verseIndex, styles.tncText]}>1.</Text>
                        <View style={{flex: 1}}>
                            <Text style={styles.tncText}>
                            [                                            ] dan [                                            ] masing-masing dalam jabatannya selaku Agent Distribution Head dan Agent Community Manager dari dan karenanya sah bertindak berdasarkan Kuasa Direksi Nomor : [*] tanggal [*] dan Nomor : [*] tanggal [*], oleh karenanya sah bertindak untuk dan atas nama Direksi PT BANK TABUNGAN PENSIUNAN NASIONAL SYARIAH Tbk, berkedudukan di Jakarta, dan untuk selanjutnya disebut ”BTPNS”
                            </Text>
                            <Text style={[styles.tncText, {paddingVertical: 24}]}>
                                Dan;
                            </Text>
                        </View>
                    </View>
                    <View style={styles.verseRow}>
                        <Text style={[styles.verseIndex, styles.tncText]}>1.</Text>
                        <View style={{flex: 1}}>
                            <View style={styles.flexContainer}>
                                <Text style={[styles.tncText, styles.tableLeft]}>Nama</Text>
                                <Text style={[styles.tncText, styles.tableRight]}>: {form.Name}</Text>
                            </View>
                            <View style={styles.flexContainer}>
                                <Text style={[styles.tncText, styles.tableLeft]}>Alamat</Text>
                                <Text style={[styles.tncText, styles.tableRight]}>: {form.Street}</Text>
                            </View>
                            <View style={styles.flexContainer}>
                                <Text style={[styles.tncText, styles.tableLeft]}>Kelurahan, Kecamatan</Text>
                                <Text style={[styles.tncText, styles.tableRight]}>: {form.Subdistrict}</Text>
                            </View>
                            <View style={styles.flexContainer}>
                                <Text style={[styles.tncText, styles.tableLeft]}>Propinsi</Text>
                                <Text style={[styles.tncText, styles.tableRight]}>: {form.Province}</Text>
                            </View>
                            <View style={styles.flexContainer}>
                                <Text style={[styles.tncText, styles.tableLeft]}>Pemegang Kartu Tanda Penduduk Nomor:</Text>
                                <Text style={[styles.tncText, styles.tableRight]}>: {form.Id_Card_No}</Text>
                            </View>
                            <View style={styles.flexContainer}>
                                <Text style={styles.tncText}>Untuk selanjutnya disebut "<Text style={styles.textBold}>Mitra Tepat</Text>"</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.verseRow}>
                        <Text style={styles.tncText}>
                        BTPNS dan Mitra Tepat secara bersama sama selanjutnya disebut “Para Pihak”. Para Pihak terlebih dahulu menerangkan bahwa :
                        </Text>
                    </View>
                    <View style={styles.verseRow}>
                        <Text style={[styles.verseIndex, styles.tncText]}>a.</Text>
                        <Text style={[styles.tncText, {flex: 1}]}>
                        Berdasarkan pertimbangan dan penilaian BTPNS dan dalam rangka memberikan layanan perbankan yang berbasis Mitra Tepat kepada nasabah BTPNS terutama nasabah yang tidak mempunyai akses perbankan, maka BTPNS setuju untuk menunjuk Mitra Tepat dalam melayani kegiatan perbankan dan Mitra Tepat setuju menerima penunjukan dan memberikan layanan perbankan kepada para nasabah.
                        </Text>
                    </View>
                    <View style={styles.verseRow}>
                        <Text style={[styles.verseIndex, styles.tncText]}>b.</Text>
                        <Text style={[styles.tncText, {flex: 1}]}>
                        Para pihak tunduk pada Perjanjian ini serta Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah sebagaimana lampiran 1.
                        </Text>
                    </View>
                    <View style={styles.verseRow}>
                        <Text style={styles.tncText}>
                        Berdasarkan hal-hal tersebut diatas, Para Pihak sepakat untuk saling mengikatkan diri dengan membuat dan menandatangani Perjanjian ini dengan ketentuan dan syarat-syarat sebagai berikut :
                        </Text>
                    </View>

                    <View style={styles.section}>
                        <RenderTnc tncText={pksKeagenan} />
                    </View>

                        <Text style={styles.tncText}>
                        Demikian Perjanjian ini dibuat dan ditandatangani pada hari, tanggal, bulan dan tahun seperti tersebut dimuka dalam rangkap 2 (dua) asli yang bermaterai cukup, dimana masing-masing memiliki kekuatan hukum yang sama dan mengikat.
                        </Text>
                </View>
            </View>
        </ScrollView>
        <EformFooter>
            <View style={styles.section}>
                <View style={styles.flexContainer}>
                    <Checkbox value={check} 
                        callback={value => setCheck(value)} />
                    <Text style={[styles.text, {flex: 1}]}>
                        Dengan memasukkan One Time Password (OTP) pada eForm proses akusisi Mitra Tepat, Saya menyatakan telah membaca, memahami dan menyetujui Perjanjian Kerjasama Penunjukan Mitra Tepat BTPN Wow! Syariah dan Syarat dan Ketentuan Umum Penunjukan Mitra Tepat BTPN Wow! Syariah.
                    </Text>
                </View>
            </View>
            <ButtonPrimary text="Setuju" 
                disabled={!check}
                onPress={() => {
                    navigation.navigate('EformTnCGen');
                }} 
            />
        </EformFooter>
      </View>
    )
}

const styles = StyleSheet.create({
    flexContainer: {
        flexDirection: 'row'
    }, 
    header: {
        fontFamily: NUNITO.bold,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
        marginBottom: 8,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 20,
        color: CUSTCOLOR.text,
    },
    textBold: {
        fontFamily: NUNITO.bold
    },
    section: {
        marginBottom: 24,
    },
    verseRow: {
        flexDirection: 'row',
    },
    verseIndex: {
        width: 32
    },
    tncText: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: COLOR.off,
    },
    tableLeft: {
        width: 112
    },
    tableRight: {
        alignSelf: 'flex-end'
    }
})

export default EformPKS
