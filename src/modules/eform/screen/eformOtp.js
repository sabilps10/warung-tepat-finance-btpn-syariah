import React, { Component, useEffect, useReducer, useState } from 'react';
import { StyleSheet, Text, KeyboardAvoidingView, Image, Alert, TouchableOpacity, ToastAndroid } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import OtpInputs from '../com/OtpInputs';
import FormHeader from '../com/FormHeader';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonPrimary from '../com/ButtonPrimary';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';
import { AcquisitionService } from '../rx/service.acquisition';
import { OtpService } from '../rx/service.otp';
import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';
import { removeData } from '../com/FunctionStorage';
import { Button } from 'react-native-elements';
import { useSelector } from 'react-redux';
import Config from '../common/config';

const insertAcquisition = async (data, userId) => {    
    return AcquisitionService.postAcquisition(data, userId)
        .then((res) => {
            return res;
        })
        .catch((error) => {
            return [false, error];
        })
        .finally((res) => {
            return res;
        });
}

const requestOtp = async (data) => {
    return OtpService.sendOtp(data)
        .then((res) => {
            return [true, res];
        })
        .catch((error) => {            
            return [false, error];
        })
        .finally((res) => {
            return [true, res];
        });
}

const EformOtp = ({navigation}) => {
    const [loading, setLoading] = useState(false);
    const form = useEform();
    const updateForm = useUpdateEform()
    const setForm = useSetEform();
    const SESSION = useSelector(store => store.Auth.session)

    const getOtp = (otp) => {
        updateForm(otp, 'Otp')
        console.log('otp', otp)
    };

    useEffect(() => {
        console.log('halaman otp eform :', form)
        onRequestOtp(form.Phone_No)
    }, [])

    const onRequestOtp = async (phoneNo) => {

        let body = {
            "phone": phoneNo
        }

        const sendOtp = await requestOtp(body)

        if (sendOtp[0]) {
            ToastAndroid.show(
                'OTP berhasil dikirimkan.',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        } else if (sendOtp[0] == false) {
            Alert.alert(
                'Gagal Mengirim SMS',
                `Gagal mengirimkan OTP. ${sendOtp[1]}`
            )
        } else {
            Alert.alert(
                'Gagal Mengirim SMS',
                'Terjadi kesalahan.'
            )
        }
    }

    const onVerification = async (data) => {
        setLoading(true);
        const userId = Number(SESSION.user.response_login_by_nik.userId);
        const userPhone = SESSION.user.response_login_by_nik.phone;

        let renderDate = (value) => {
            if (value != 'undefined') {
                let dateArray = (value).split('/')
    
                return `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`
            }
        }
    
        let dataIdExpDate = () => {
            if (data.id_card_lifetime) {
                return '2100-12-31'
            } else {
                return renderDate(data.Id_Card_Exp_Date)
            }
        } 

        let renderGrossIncome = (value) => {
            const thousandSeparator = '.';
            const thousandSeparatorPatt = new RegExp(`\\${thousandSeparator}`, 'g')

            const result = value.replace(thousandSeparatorPatt, '')

            return Number(result)
        }
        
        let body = {
            "AppAcqCode": "Hello",
            "AppAcqReferralBankClerkNoHp": userPhone,
            "AppAcqFullname": data.Name,
            "AppAcqHp": String(data.Phone_No),
            "AppAcqEmail": data.Email ? data.Email : Config.agentDefaultEmail,
            "AppAcqIdCardType": data.Id_Card_Type,
            "AppAcqIdCardNo": String(data.Id_Card_No),
            "AppAcqIdCardExpiredDate": dataIdExpDate(),
            "AppAcqNpwp": data.Is_Tax_Exempted && data.Tax_Card_Number != '' ? data.Tax_Card_Number : null,
            "AppAcqPob": data.Place_Of_Birth,
            "AppAcqDob": renderDate(data.Date_Of_Birth),
            "AppAcqSex": data.Gender,
            "AppAcqAnnualEstimatedDebitTrans": data.Forecast_Transaction,
            "AppAcqAddress": data.Street,
            "AppAcqResidentialAddress": data.is_current_address ? data.Street : data.Street_2,
            "AppAcqRt": Number(data.RT),
            "AppAcqRw": Number(data.RW),
            "AppAcqProvince": data.Province,
            "AppAcqCity": data.City,
            "AppAcqDistrict": data.District,
            "AppAcqSubDistrict": data.Subdistrict,
            "AppAcqZipcode": String(data.Zip_Code),
            "AppAcqReligion": data.Religion,
            "AppAcqMaritalStatus": data.Marital_Status,
            "AppAcqNationality": 'Indonesia',
            "AppAcqMotherMaidenName": data.Mother_Maiden_Name,
            "AppAcqEducation": data.Education,
            "AppAcqLatlongBusinessLocation": data.Location,
            "AppAcqOccupation": data.Occupation,
            "AppAcqOccupationPosition": data.Job,
            "AppAcqBusinessField": Number(data.Industry_Sector_of_Employer),
            "AppAcqIncomeSource": data.Source_of_Fund,
            "AppAcqMonthlyAvgIncome": data.Income,
            "AppAcqAnnualGrossIncome": renderGrossIncome(data.Annual_Income),
            "AppAcqOpeningAccountPurpose": data.Purpose_Of_Account,
            "AppAcqBankRelationship": data.Relation,
            "AppAcqSelfieDocPath": data.AppAcqSelfieDocPath,
            "AppAcqBitaddDocPath": data.AppAcqBitaddDocPath,
            "AppAcqPaofDocPath": data.AppAcqPaofDocPath,
            "AppAcqNpwpDocPath": data.AppAcqNpwpDocPath,
            "AppAcqStatusApproval": data.AppAcqStatusApproval,
            // "AppAcqSignatureDocPath": data.AppAcqSignatureDocPath,
            "AppAcqSignatureDocPath": 'pathString',
            "AppAcqIdCardDocPath": data.AppAcqIdCardDocPath,
            "AppAcqBusinessCertDocPath": data.AppAcqBusinessCertDocPath,
            "AppAcqOtp": data.Otp
        };

        console.log('body', body)
        
        const insertData = await insertAcquisition(body, userId)

        if (typeof insertData == 'object' && insertData[0] == false) {
            setLoading(false)

            Alert.alert(
                'Gagal',
                'Terjadi kesalahan. ' + insertData[1],
            )

            return false
        }

        if (insertData.message != null && insertData.message != undefined) {
            Alert.alert(
                'Gagal',
                `Gagal memproses data. ${insertData.message}`,
            )
    
            setLoading(false)
        } else {
            setLoading(false);
            setForm({})
            removeDraft(data.Index, '@dataDraft');
            navigation.navigate('EformSubmitted')
        }

        // if (insertData == true) {
        //     // navigation.navigate('EformSubmitted')
        // } else {
        //     Alert.alert([
        //         'Gagal',
        //         'Gagal memproses data',
        //     ])
        // }
    }

    const removeDraft = async (index, storageKey) => {
        console.log('delete ', index)
        await removeData(index, storageKey);
    }

    return (
        <View animation="fadeIn" style={styles.container} useNativeDriver>
            <FormHeader 
                text="Kembali" 
                leftButton={
                    <TouchableOpacity
                        disabled={loading}
                        onPress={() => {
                            navigation.goBack()
                            console.log('back')
                        }}
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
                // onClose={() => navigation.navigate('Eform')}
                // closeToSave={false}
            />
            <KeyboardAvoidingView 
                style={styles.bodyContainer} 
                enabled={false} 
                behavior="height"
            >
                <View style={{flex: 1}}>
                    <View style={styles.screenHeader}>
                        <Text style={styles.screenHeaderText}>Verifikasi Nomor</Text>
                        <Text style={styles.screenHeaderText}>Telepon Anda</Text>
                    </View>
                    <View style={styles.bodyTextContainer}>
                        <Text style={styles.text}>Masukkan 6 nomor kode yang</Text>
                        <Text style={styles.text}>dikirimkan ke nomor telepon Anda</Text>
                    </View>
                    <OtpInputs 
                        getOtp={(value) => getOtp(value)} 
                        // isError={this.props.errors.otp_code} 
                        // resetError={this.resetError} 
                    />
                    <View style={styles.urlContainer}>
                        <Button 
                            type="clear"
                            titleStyle={styles.url}
                            title="Kirim Ulang" 
                            onPress={() => onRequestOtp(form.Phone_No)}
                        />
                    </View>
                </View>
                <ButtonPrimary 
                    text="Verifikasi" 
                    onPress={() => onVerification(form)} 
                    disabled={(loading || !(form.Otp?.length == 6))}
                    loading={loading}
                />
            </KeyboardAvoidingView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    bodyContainer: {
        flex: 1,
        paddingTop: 32,
        paddingBottom: 16,
        paddingHorizontal: 24,
    },
    screenHeader: {
        marginBottom: 32
    },
    screenHeaderText: {
        fontSize: 24,
        lineHeight: 36,
        color: CUSTCOLOR.textDark,
        fontFamily: MARKOT.book
    },
    bodyTextContainer: {
        marginBottom: 40,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
    },
    urlContainer: {
        alignItems: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: NUNITO.extraBold,
        fontSize: 14,
    },
    verifButton: {
        bottom: 16,
        zIndex: 10, 
        borderWidth: 2,
        borderColor: 'red'
    }
});


export default EformOtp;
