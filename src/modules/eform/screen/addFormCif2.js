import React, { useState, useEffect } from 'react';
import { 
    StyleSheet, 
    Text, 
    ScrollView, 
    Animated,
    Image, 
    TouchableOpacity, 
    Alert, 
    ToastAndroid ,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native-animatable';

import iconChevronDown from '@assets/icons/icon_eform_chevron_down.png';

import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import HeaderText from '../com/HeaderText';

import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';
import { CUSTCOLOR, NUNITO } from '../common/styles';
import { COLOR } from '../../../common/styles';
import ButtonSecondary from '../com/ButtonSecondary';
import { getDataByField, ToggleExpand } from '../common/function';
import options from '../common/data';

const Section = (props) => {
    const [open, setOpen] = useState(false);

    useEffect(() => {
        setOpen(props.defaultOpen)
    }, [])

    const toggleOpen = (open) => {
        ToggleExpand()
        setOpen(open)
    }

    return (
        <View style={styles.sectionContainer}>
            <TouchableOpacity 
                activeOpacity={0.9}
                style={styles.sectionTitle}
                onPress={() => toggleOpen(!open)}
            >
                <HeaderText>{props.title}</HeaderText>
                <Image source={iconChevronDown} style={{width: 24, height: 24, justifyContent: 'center', alignItems: 'center'}} />
            </TouchableOpacity>
            {
                open ? (
                <View style={{overflow: 'hidden'}}>
                    <View style={styles.sectionBody}>
                        {props.children}
                    </View>
                </View>)
                :
                <View></View>
            }
        </View>
    )
}

const AddFormCif2 = ({route, navigation}) => {
    const form = useEform();
    const setForm = useSetEform();
    const [OPTIONS, setOPTIONS] = useState({})
    
    useEffect(() => {
        getDataMaster(global.MASTER)
    }, [])

    const getDataMaster = async (master) => {
        const masterData = {
            businessFields: await getOptions(master.businessFields),
            bankRelations: await getOptions(master.bankRelations),
            occupation: options.occupation,
            job: options.job,
            income: options.income,
            forecastTransaction: options.forecastTransaction,
            sourceFund: options.sourceFund,
            purposeOfAccount: options.purposeOfAccount,
        }
        
        setOPTIONS(masterData)
    }

    const getOptions = (data) => {
        const options = data.map(item => {
            return {
                ...item,
                label: item.text,
            }
        })

        return options
    }

    const getTextFromValue  = (value, data) => {
        const selected = getDataByField(data, 'value', value)

        if (selected != null) return selected.label
    }

    const handleNextPage = async (page) => {
        navigation.navigate(page);
    }

    const parseExpDate = (date) => {
        let idCardLifetime = date == '31/12/2100' ? true : false;

        if (idCardLifetime) return "Seumur Hidup"
        if (!idCardLifetime) return date
    }

    const exitToMain = () => {
        setForm({})         
        navigation.navigate('Eform')
    }

    return (
        <View style={{height: '100%', backgroundColor: '#dadada', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => exitToMain()}
            >
                <Pagination active={2} pageText={['1', '2', '3']} />
            </FormHeader>
            <ScrollView 
                contentContainerStyle={{paddingVertical: 16}}
                style={{height: '100%'}}
            >
                <Section title="Data Identitas Calon Mitra Tepat" defaultOpen={true}>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Jenis Identitas</Text>
                        <Text style={styles.fieldContent}>{form.Id_Card_Type}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Masa Berlaku</Text>
                        <Text style={styles.fieldContent}>{parseExpDate(form.Id_Card_Exp_Date)}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Nomor Identitas</Text>
                        <Text style={styles.fieldContent}>{form.Id_Card_No}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Nama Lengkap (Sesuai KTP)</Text>
                        <Text style={styles.fieldContent}>{form.Name}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Tempat Lahir</Text>
                        <Text style={styles.fieldContent}>{form.Place_Of_Birth}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Tanggal Lahir</Text>
                        <Text style={styles.fieldContent}>{form.Date_Of_Birth}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Alamat Tempat Tinggal</Text>
                        <Text style={styles.fieldContent}>{form.Street_2}</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={styles.sectionField}>
                            <Text style={styles.fieldTitle}>RT</Text>
                            <Text style={styles.fieldContent}>{form.RT}</Text>
                        </View>
                        <View style={styles.sectionField}>
                            <Text style={styles.fieldTitle}>RW</Text>
                            <Text style={styles.fieldContent}>{form.RW}</Text>
                        </View>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Provinsi</Text>
                        <Text style={styles.fieldContent}>{form.Province}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Kabupaten/Kota</Text>
                        <Text style={styles.fieldContent}>{form.City}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Kecamatan</Text>
                        <Text style={styles.fieldContent}>{form.District}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Kelurahan</Text>
                        <Text style={styles.fieldContent}>{form.Subdistrict}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Kode Pos</Text>
                        <Text style={styles.fieldContent}>{form.Zip_Code}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Pendidikan Terakhir</Text>
                        <Text style={styles.fieldContent}>{form.Education}</Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Nama Gadis Ibu Kandung (Tanpa Gelar)</Text>
                        <Text style={styles.fieldContent}>{form.Mother_Maiden_Name}</Text>
                    </View>
                </Section>
                <Section title="Data Usaha &amp; Keuangan">
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Pekerjaan</Text>
                        <Text style={styles.fieldContent}>
                            {getTextFromValue(form.Occupation, OPTIONS.occupation)}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Bidang/Sektor Usaha</Text>
                        <Text style={styles.fieldContent}>
                            {getTextFromValue(form.Industry_Sector_of_Employer, OPTIONS.businessFields)}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Sumber Dana</Text>
                        <Text style={styles.fieldContent}>
                            {getTextFromValue(form.Source_of_Fund, OPTIONS.sourceFund)}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Penghasilan Rata-Rata Sebulan</Text>
                        <Text style={styles.fieldContent}>
                            {getTextFromValue(form.Income, OPTIONS.income)}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Penghasilan Kotor Setahun</Text>
                        <Text style={styles.fieldContent}>
                            {form.Annual_Income}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Perkiraan Transaksi Debit Setahun</Text>
                        <Text style={styles.fieldContent}>
                            {getTextFromValue(form.Forecast_Transaction, OPTIONS.forecastTransaction)}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Tujuan Pembukaan Rekening</Text>
                        <Text style={styles.fieldContent}>
                            {getTextFromValue(form.Purpose_Of_Account, OPTIONS.purposeOfAccount)}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Hubungan Dengan Bank</Text>
                        <Text style={styles.fieldContent}>
                            {/* {getTextFromValue(form.Relation, OPTIONS.bankRelations)} */}
                            {form.Relation}
                        </Text>
                    </View>
                    <View style={styles.sectionField}>
                        <Text style={styles.fieldTitle}>Nomor NPWP</Text>
                        <Text style={styles.fieldContent}>
                            {form.Tax_Card_Number}
                        </Text>
                    </View>
                </Section>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}>
                        <ButtonSecondary 
                            text="Kembali" 
                            onPress={() => navigation.navigate('AddFormCif1')} 
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary text="Lanjut" 
                            onPress={() => handleNextPage('AddFormCif3')}
                        />
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    sectionContainer: {
        backgroundColor: '#fff',
        borderRadius: 8,
        overflow: 'hidden',
        marginVertical: 8,
        marginHorizontal: 24,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
    sectionTitle: {
        padding: 16,
        paddingBottom: 0,
        flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'space-between',
    },
    sectionBody: {
        padding: 16,
        paddingBottom: 0,
        borderTopWidth: 1,
        borderTopColor: '#d8d8d8'
    },
    sectionField: {
        marginBottom: 16,
        flex: 1
    },
    fieldTitle: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: CUSTCOLOR.textLight
    },
    fieldContent: {
        fontFamily: NUNITO.bold,
        fontSize: 16,
        lineHeight: 26,
        color: CUSTCOLOR.text
    }
})

export default AddFormCif2
