import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';

import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import ButtonSecondary from '../com/ButtonSecondary';
import HeaderText from '../com/HeaderText';
import FormField from '../com/FormField';
import InputText from '../com/InputText';
import InputPicker from '../com/InputPicker';
import ButtonSwitch from '../com/ButtonSwitch';
import { useEform, useUpdateEform } from '../context/EformContext';
import { MastersService } from '../rx/service.masters';
import { MARKOT, NUNITO } from '../common/styles';
import ExpandableView from '../com/ExpandableView';

import { useSelector } from 'react-redux';
import options from '../common/data';

const AddForm3 = ({route, navigation}) => { 
    const form = useEform();
    const updateForm = useUpdateEform();
    const SESSION = useSelector(store => store.Auth.session)

    const [warning, setWarning] = useState({})

    const [OPTIONS, setOPTIONS] = useState({})

    useEffect(() => {
        getDataMaster(global.MASTER)
        
        if ((form.Is_Tax_Exempted == null && form.Tax_Card_Number == null) ||form.Tax_Card_Number == 0) {
            updateForm(false, 'Is_Tax_Exempted')
            updateForm(null, 'Tax_Card_Number')
        }
        if (form.Relation == null) updateForm('9900', 'Relation')

        if (form.repairFormMode) onValidate();
    }, [])

    const getDataMaster = async (master) => {
        const masterData = {
            businessFields: await getOptions(master.businessFields),
            bankRelations: await getOptions(master.bankRelations),
            occupation: options.occupation,
            job: options.job,
            income: options.income,
            forecastTransaction: options.forecastTransaction,
            sourceFund: options.sourceFund,
            purposeOfAccount: options.purposeOfAccount,
        }
        
        setOPTIONS(masterData)
    }

    const getOptions = (data) => {
        const options = data.map(item => {
            return {
                ...item,
                label: item.text,
            }
        })

        return options
    }

    const updateWarning = async (value, index) => {
        setWarning(current => ({
            ...current,
            [index]: value
        }))
    }

    const validateIsNull = async (field, text) => {
        if (form[field] == undefined || form[field] == '' || form[field] == null) {
            updateWarning(`${text} harus diisi`, field)
            return false
        } else {
            updateWarning('', field)
            return true
        }
    }
    
    const validateTaxCard = async () => {
        const patt = /^\d{15}$/;

        if (form.Is_Tax_Exempted) {
            if (form.Tax_Card_Number == undefined || form.Tax_Card_Number == '' || form.Tax_Card_Number == null) {
                updateWarning('No NPWP harus diisi', 'Tax_Card_Number')
                return false
            } else if (!form.Tax_Card_Number.match(patt)) {
                updateWarning('Format tidak sesuai', 'Tax_Card_Number')
                return false
            } else {
                updateWarning('', 'Tax_Card_Number')
                return true
            }
        } else {
            updateWarning('', 'Tax_Card_Number')
            return true
        }
    }

    const validateAnnualIncome = async () => {
        const thousandSeparator = '.';
        const decimalSeparator = ',';

        const thousandSeparatorPatt = new RegExp(`\\${thousandSeparator}`, 'g')

        if (form.Annual_Income == null || form.Annual_Income == '') {
            updateWarning('Penghasilan kotor harus diisi', 'Annual_Income')
            return false
        }
        
        const clearedValue = form.Annual_Income.replace(thousandSeparatorPatt, '')

        const patt = new RegExp(`^\\d+(\\${decimalSeparator}?\\d*)$`);
        // const patt = /^\d+(\,?\d*)$/;
        
        if (!clearedValue.match(patt)) {
            updateWarning('Format tidak sesuai', 'Annual_Income')
            return false
        } else {
            updateWarning('', 'Annual_Income')
            return true
        }
    }

    const validateLocation = async () => {
        if (form.Location == undefined || form.Location == '' || form.Location == null ) {
            updateWarning('Lokasi harus diisi', 'Location')
            return false
        }
        
        const locationArray = (form.Location).split(',')
        const patt = /(\-?\d+\.?\d*)\,(\-?\d+\.?\d*)/
        
        if (locationArray.length != 2 && !form.Location.match(patt)) {
            updateWarning('Format tidak sesuai', 'Location')
            return false
        } else {
            updateWarning('', 'Location')
            return true
        }
    }

    const onValidate = async () => {
        const Location = await validateLocation()
        const Occupation = await validateIsNull('Occupation', 'Pekerjaan')
        const Job = await validateIsNull('Job', 'Posisi pekerjaan')
        const Industry_Sector_of_Employer = await validateIsNull('Industry_Sector_of_Employer', 'Bidang sektor usaha')
        const Source_of_Fund = await validateIsNull('Source_of_Fund', 'Sumber dana')
        const Income = await validateIsNull('Income', 'Penghasilan rata-rata')
        const Annual_Income = await validateAnnualIncome()
        const Forecast_Transaction = await validateIsNull('Forecast_Transaction', 'Perkiraan transaksi')
        const Purpose_Of_Account = await validateIsNull('Purpose_Of_Account', 'Tujuan pembukaan rekening')
        const Relation = await validateIsNull('Relation', 'Hubungan')
        const NPWP = form.Is_Tax_Exempted ? await validateTaxCard() : true

        const validate = [
            Location,
            Occupation,
            Job,
            Industry_Sector_of_Employer,
            Source_of_Fund,
            Income,
            Annual_Income,
            Forecast_Transaction,
            Purpose_Of_Account,
            Relation,
            NPWP
        ]
        
        return validate.every(item => item == true)
    }

    const handleNextPage = async (page) => {
        await setWarning({})
        
        const canContinue = await onValidate()

        if (canContinue) navigation.navigate(page);
        if (!canContinue) {
            ToastAndroid.show(
                'Mohon periksa kembali data yang Anda masukkan',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        }
    }


    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => navigation.navigate('Eform')}
                closeToSave={true}
            >
                <Pagination active={3} pageText={['1', '2', '3', '4']} />
            </FormHeader>
            <ScrollView style={{height: '100%', backgroundColor: '#fff'}}>
                <View style={{padding: 24, paddingBottom: 4}}>

                <HeaderText>Data Usaha & Keuangan</HeaderText>
                <FormField
                    text="Lokasi"
                    warning={warning.Location}
                >
                    {
                    form.Location != null && form.Location != undefined ? 
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', height: 40}}>
                        <Text style={{color: COLOR.tertiary}}>{form.Location}</Text>
                        <TouchableOpacity 
                            onPress={() => navigation.navigate('EformMap')}
                        >
                            <Text style={{fontWeight: 'bold', color: COLOR.tertiary}}>Ubah</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <ButtonSecondary text="Tambah Lokasi" 
                        onPress={() => navigation.navigate('EformMap')}
                    />
                    }
                </FormField>
                <FormField
                    text="Pekerjaan"
                    warning={warning.Occupation}
                >
                    <InputPicker 
                    placeholder="Pilih pekerjaan"
                    options={OPTIONS.occupation}
                    selectedValue={form.Occupation}
                    onValueChange={(value) => updateForm(value, 'Occupation')}
                    />
                </FormField>
                <FormField
                    text="Posisi Pekerjaan"
                    warning={warning.Job}
                >
                    <InputPicker 
                    placeholder="Pilih posisi"
                    options={OPTIONS.job}
                    selectedValue={form.Job}
                    onValueChange={(value) => updateForm(value, 'Job')}
                    />
                </FormField>
                <FormField
                    text="Bidang/Sektor Usaha"
                    warning={warning.Industry_Sector_of_Employer}
                >
                    <InputPicker 
                    placeholder="Pilih bidang/sektor usaha"
                    options={OPTIONS.businessFields}
                    selectedValue={form.Industry_Sector_of_Employer}
                    onValueChange={(value) => {
                        updateForm(value, 'Industry_Sector_of_Employer')
                    }}
                    />
                </FormField>
                <FormField
                    text="Sumber Dana"
                    warning={warning.Source_of_Fund}
                >
                    <InputPicker 
                    placeholder="Pilih sumber dana"
                    options={OPTIONS.sourceFund}
                    selectedValue={form.Source_of_Fund}
                    onValueChange={(value) => updateForm(value, 'Source_of_Fund')}
                    />
                </FormField>
                <FormField
                    text="Penghasilan Rata-Rata Sebulan"
                    warning={warning.Income}
                >
                    <InputPicker 
                    placeholder="Pilih rentang penghasilan"
                    options={OPTIONS.income}
                    selectedValue={form.Income}
                    onValueChange={(value) => updateForm(value, 'Income')}
                    />
                </FormField>
                <FormField
                    text="Penghasilan Kotor Setahun"
                    warning={warning.Annual_Income}
                >
                    <InputText 
                        placeholder="0" 
                        value={form.Annual_Income}
                        onChangeText={(value) => updateForm(value, 'Annual_Income')}
                        keyboardType="numeric"
                        leftText="Rp. "
                        numberFormat
                    />
                </FormField>
                <FormField
                    text="Perkiraan Transaksi Debit Setahun"
                    warning={warning.Forecast_Transaction}
                >
                    <InputPicker 
                    placeholder="Pilih perkiraan transaksi"
                    options={OPTIONS.forecastTransaction}
                    selectedValue={form.Forecast_Transaction}
                    onValueChange={(value) => updateForm(value, 'Forecast_Transaction')}
                    />
                </FormField>
                <FormField
                    text="Tujuan Pembukaan Rekening"
                    warning={warning.Purpose_Of_Account}
                >
                    <InputPicker 
                        placeholder="Pilih Tujuan"
                        options={OPTIONS.purposeOfAccount}
                        selectedValue={form.Purpose_Of_Account}
                        onValueChange={(value) => updateForm(value, 'Purpose_Of_Account')}
                    />
                </FormField>
                <FormField
                    text="Hubungan Dengan Bank"
                    warning={warning.Relation}
                >
                    <InputPicker 
                        placeholder="Pilih Hubungan"
                        options={OPTIONS.bankRelations}
                        selectedValue={form.Relation}
                        onValueChange={(value) => {
                            updateForm(value, 'Relation')
                        }}
                    />
                </FormField>
                <View style={{paddingTop: 12, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 28}}>
                    <Text style={[styles.text, {flex: 1, marginRight: 24}]}>Memiliki NPWP</Text>
                    <ButtonSwitch 
                    value={form.Is_Tax_Exempted}
                    callback={value => {
                        updateForm(value, 'Is_Tax_Exempted')
                        !value && updateForm(null, 'Tax_Card_Number')
                    }} />
                </View>
                {form.Is_Tax_Exempted ? (
                    <View>
                        <FormField
                            text="Nomor NPWP"
                            warning={warning.Tax_Card_Number}
                        >
                        <InputText 
                            // placeholder="00.000.000.0-000.000"
                            placeholder="000000000000000"
                            value={form.Tax_Card_Number}
                            onChangeText={value => updateForm(value, 'Tax_Card_Number')}
                            keyboardType="numeric"
                            maxLength={15}
                        />
                        </FormField>
                    </View>
                ) : <View></View>}
                </View>
                <View style={styles.pageSection}>
                <HeaderText>Data Petugas</HeaderText>
                <View style={styles.refferalContainer}>
                    <Text style={[styles.text, {fontSize: 12}]}>Refferal Number</Text>
                    <Text style={[styles.textBold, {fontSize: 16}]}>{SESSION?.user?.response_login_by_nik?.phone}</Text>
                </View>
                </View>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}>
                        <ButtonSecondary 
                            text="Kembali" 
                            onPress={() => navigation.navigate('AddForm2', {form})} 
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary 
                            text="Lanjut" 
                            onPress={() => handleNextPage('AddForm4')} 
                        />  
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    pageSection: {
        padding: 24,
        paddingBottom: 4,
        borderTopWidth: 8,
        borderTopColor: '#dadada' 
    },
    refferalContainer: {
        padding: 16, 
        borderRadius: 8, 
        borderWidth: 1, 
        borderColor: '#ddd', 
        marginBottom: 28,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
    text: { fontFamily: NUNITO.regular },
    textBold: { fontFamily: NUNITO.bold }
})

export default AddForm3