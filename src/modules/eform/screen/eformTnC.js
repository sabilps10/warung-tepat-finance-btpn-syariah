import React, { Component, useState, useEffect } from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import FormHeader from '../com/FormHeader';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import HeaderText from '../com/HeaderText';
import Checkbox from '../com/Checkbox';

import { CUSTCOLOR, NUNITO } from '../common/styles';
import RenderTnc from '../com/RenderTnc';
import {syartumRekeningWow} from '../texts/syartumRekeningWow';

const EformTnC = ({route, navigation}) => {
    const [btnDisabled, setBtnDisabled] = useState(true);

    const [check1, setCheck1] = useState(false)
    const [check2, setCheck2] = useState(false)
    const [check3, setCheck3] = useState(false)

    // const [checks, setChecks] = useState([false, false, false])
    
    // const checkCheckbox = async (value, index) => {
    //     let check = [...checks]
        
    //     check[index] = value

    //     await setChecks(check)

    //     let disableButton = check.every(item => item == true)

    //     setBtnDisabled(!disableButton)
    // }

    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Syarat dan Ketentuan"
                onClose={() => navigation.navigate('Eform')}
                closeToSave={true}
            />
            <ScrollView style={{padding: 24}}>
                <View style={{paddingBottom: 61}}>
                    <Text style={styles.header}>Syarat & Ketentuan Umum Pembukaan Rekening</Text>

                    <View style={styles.section}>
                        <RenderTnc tncText={syartumRekeningWow} />
                    </View>

                    <View style={styles.section}>
                        <Text style={styles.header}>Statement 1</Text>
                        <View style={styles.flexContainer}>
                            <Checkbox value={check1} 
                                callback={async (value) => {
                                    setCheck1(value)
                                    // checkCheckbox(value, 0)
                                }} 
                            />
                            <Text style={[styles.text, {flex: 1}]}>
                            Memberikan persetujuan kepada Bank untuk menyerahkan dan/atau menyebarluaskan data pribadi saya kepada pihak lain (di luar Bank) dan saya memahami penjelasan yang diberikan oleh Bank mengenai tujuan dan konsekuensi dari pemberian dan/atau penyebarluasan data pribadi tersebut kepada pihak lain (di luar Bank), termasuk keuntungan, resiko dan biaya-biaya yang timbul.
                            </Text>
                        </View>
                    </View>
                    <View style={styles.section}>
                        <Text style={styles.header}>Statement 2</Text>
                        <View style={styles.flexContainer}>
                            <Checkbox value={check2} 
                                callback={(value) => {
                                    setCheck2(value)
                                    // checkCheckbox(value, 1)
                                }}
                            />
                            <Text style={[styles.text, {flex: 1}]}>
                            Memberikan persetujuan kepada Bank untuk melakukan penawaran produk/layanan melalui media komunikasi pribadi Nasabah.
                            </Text>
                        </View>
                    </View>
                    <View style={styles.section}>
                        <Text style={styles.header}>Statement 3</Text>
                        <View style={styles.flexContainer}>
                            <Checkbox value={check3} 
                                callback={(value) => {
                                    setCheck3(value)
                                    // checkCheckbox(value, 2)
                                }}
                            />
                            <Text style={[styles.text, {flex: 1}]}>
                            Bersedia menerima korespondensi / informasi / surat menyurat dari Bank.
                            </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <EformFooter>
                <ButtonPrimary text="Lanjut" 
                    // disabled={btnDisabled}
                    onPress={() => {
                        navigation.navigate('EformPKS');
                    }} 
                />
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    flexContainer: {
        flexDirection: 'row'
    }, 
    header: {
        fontFamily: NUNITO.bold,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
        marginBottom: 8,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 20,
        color: CUSTCOLOR.text,
    },
    section: {
        marginBottom: 24,
    },
    tncText: {
        fontFamily: NUNITO.regular,
        fontSize: 12,
        lineHeight: 20,
        color: COLOR.off,
    },
})

export default EformTnC
