import React, { useState, useEffect } from 'react';
import { StyleSheet, BackHandler, Keyboard, Text, ScrollView, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader'

import iconOption from '@assets/icons/icon_eform_option.png';
import iconLeft from '@assets/icons/icon_eform_arrow_left.png';
import iconOrangeRight from '@assets/icons/icon_eform_orange_chevron_right.png';

import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import { withNavigation } from 'react-navigation';

import Config from '../common/config';
import HeaderText from '../com/HeaderText';
import { useSetEform } from '../context/EformContext';

const EformNewInput = ({navigation}) => { 
    const setForm = useSetEform();
       
    const MenuButton = (props) => {
        return (
            <TouchableOpacity 
                activeOpacity={0.9}
                style={styles.menuButton}
                onPress={props.onPress}
            >
                {/* <Image source={props.iconSource} style={{width: 24, height: 24, marginRight: 8}} /> */}
                <Text style={styles.menuButtonText}>{props.text}</Text>
                <Image source={iconOrangeRight} style={{height: 24, width: 24}} />
            </TouchableOpacity>
        )
    }

    return (
        <View style={{height: '100%', backgroundColor: '#dadada', justifyContent: 'space-between'}}>
        <FormHeader 
            text="Input Data" 
            leftButton={
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack()
                        console.log('back')
                    }}
                >
                    <Image source={iconLeft} style={{height: 24, width: 24}} />
                </TouchableOpacity>
            }
            rightButton={(
                <TouchableOpacity>
                    <Image source={iconOption} style={{width: 24, height: 24}} />
                </TouchableOpacity>
            )}
        />
        <ScrollView style={{paddingTop: 24}}>
            <View style={{marginHorizontal: 24}}>
                <HeaderText style={{marginBottom: 8}}>Pilih Kategori Calon Mitra Tepat</HeaderText>
            </View>
            <MenuButton 
                text="Calon Mitra Tepat Baru"
                onPress={() => {
                    setForm({})
                    navigation.navigate('AddForm1')
                }}
            />
            <MenuButton 
                text="Mempunyai Nomor CIF"
                onPress={() => navigation.navigate('EformInputCif')}
            />
        </ScrollView>
      </View>
    )
}

const styles = StyleSheet.create({
    menuButton: {
        height: 56,
        backgroundColor: '#fff',
        borderRadius: 8,
        alignItems: 'center',
        flexDirection: 'row',
        padding: 16,
        marginVertical: 8,
        marginHorizontal: 24,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
    menuButtonText: {
        fontFamily: MARKOT.bold,
        color: CUSTCOLOR.textDark,
        fontSize: 18,
        lineHeight: 24,
        flex: 1
    },
    badge: {
        width: 27,
        height: 27,
        borderRadius: 27,
        backgroundColor: '#ef3636',
        justifyContent: 'center',
        alignItems: 'center'
    },
    badgeText: {
        fontFamily: NUNITO.bold,
        color: '#fff',
        fontSize: 14,
        lineHeight: 22
    }
})

export default EformNewInput;
