import React, { useEffect, useRef, useState } from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    PermissionsAndroid,
    Image,
    TextInput,
    Alert
} from 'react-native'
import FusedLocation from 'react-native-fused-location';

// import Geolocation from 'react-native-geolocation-service';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import FormHeader from '../com/FormHeader';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';

import { COLOR } from '@common/styles';

import iconTarget from '@assets/icons/icon_eform_target.png';
import iconSearch from '@assets/icons/icon_eform_search.png';
import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';
import { CUSTCOLOR, NUNITO } from '../common/styles';
import Config from '../common/config';

const EformMap = ({route, navigation}) => {
    const form = useEform();
    const updateForm = useUpdateEform();
    const setForm = useSetEform();

    const [location, setLocation] = useState({
        latitude: 37.78825,
        longitude: -122.4324
    })

    const _map = useRef(null);

    const askPermission = () => {
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        ).then(granted => {
            findCoordinates()
        });
    }

    const parseLocation = async (location) => {
        const locArray = location.split(',');
        
        console.log(locArray, locArray.length)
        if (locArray.length !== 2) {
            readLocation()
        } else {
            const [latitude, longitude] = locArray;
            
            console.log('parse location', latitude, typeof parseFloatlatitude, parseFloat(latitude), typeof parseFloat(latitude))
            
            await setLocation({
                latitude: parseFloat(latitude),
                longitude: parseFloat(longitude)
            })
            getAnimateCamera({latitude: parseFloat(latitude), longitude: parseFloat(longitude)})
        }
    }

    useEffect(() => {
        if (form.Location) {
            parseLocation(form.Location)
        } else {
            readLocation()
            // console.log('read loc')
        }
    }, []);

    const readLocation = async () => {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
            title: 'App needs to access your location',
            message: 'App needs access to your location so we can let our app be even more awesome.',
        });
        if (granted) {
            const on = await FusedLocation.areProvidersAvailable();
            if (on) {
                const location = await FusedLocation.getFusedLocation();

                setLocation({
                    latitude: location.latitude,
                    longitude: location.longitude
                });
                getAnimateCamera(location)
            } else {
                AndroidOpenSettings.locationSourceSettings();
            }
        }
    }

    const getAnimateCamera = (coords) => {
        _map.current.animateCamera({
            center: {
                latitude: coords.latitude,
                longitude: coords.longitude
            },
            zoom: 15,
            heading: 0, 
            pitch: 0,
        },
        {duration: 1000})
    }
  
    const onPress = (evt) => {
        let coords = evt.nativeEvent.coordinate;
        setLocation(coords)
        getAnimateCamera(coords)
    }

    const onSaveLocation = async () => {
        await updateForm(`${location.latitude},${location.longitude}`, 'Location');

        console.log('onSaveLocation', location, `${location.latitude},${location.longitude}`, form)
        navigation.navigate('AddForm3')
    }

    const onCancel = () => {
        Alert.alert(
            'Batal',
            'Yakin untuk batal simpan lokasi?',
            [
                {
                    text: "Kembali",
                    style: "cancel"
                },
                { 
                    text: "Batal", 
                    onPress: () => {
                        setForm({...form})
                        console.log(form.Location)
                        navigation.goBack()
                    }
                }
            ], 
        )
    }

    const renderSearchButton = () => {
        return (
            <TouchableOpacity style={styles.searchButton}>
                <Image source={iconSearch} style={{width: 24, height: 24}} />
            </TouchableOpacity> 
        )
    }

    const GooglePlacesInput = () => {
        return (
            <GooglePlacesAutocomplete
                placeholder='Lokasi disini'
                onPress={(data, details) => {
                    // 'details' is provided when fetchDetails = true
                    const {lat: latitude, lng: longitude} = details.geometry.location
                    setLocation({
                        latitude: latitude,
                        longitude: longitude
                    });
                    getAnimateCamera({latitude, longitude})
                }}
                fetchDetails
                enablePoweredByContainer={false}
                renderRightButton={() => renderSearchButton()}
                query={{
                    key: Config.apiKey.google_places,
                    language: 'id',
                    components: 'country:id',
                    types: 'geocode'
                }}
                styles={{
                    container: {
                        padding: 0,
                    },
                    textInputContainer: {
                        padding: 0,
                        height: 40,
                        borderWidth: 0,
                        paddingVertical: 7,
                        alignItems: 'center'
                    },
                    textInput: {
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        fontSize: 16,
                        fontFamily: NUNITO.regular,
                        color: CUSTCOLOR.text,
                        marginLeft: 0,
                        marginBottom: 0,
                        flex: 1,
                        borderWidth: 0,
                    },
                    row: {
                        paddingHorizontal: 0
                    },
                    description: {
                        fontFamily: NUNITO.regular,
                        color: CUSTCOLOR.text
                    }
                }}
            />
        );
    };
    
    return (
        <View style={styles.container}>
            <FormHeader 
                text="Pilih Lokasi" 
                onClose={() => onCancel()}
            />
            <View style={styles.mapcontainer}>
                <MapView
                    provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    ref={_map}
                    initialRegion={{
                        latitude: location.latitude,
                        longitude: location.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                    // region={{
                    //     latitude: location.latitude,
                    //     longitude: location.longitude,
                    //     latitudeDelta: 0.015,
                    //     longitudeDelta: 0.0121,
                    // }}
                    initialCamera={{
                        center: {
                            latitude: location.latitude,
                            longitude: location.longitude
                        },
                        heading: 0, 
                        altitude: 0, 
                        zoom: 15, 
                        pitch: 0
                    }}
                    showUserLocation={true} 
                    showsMyLocationButton={true} 
                    loadingEnabled={true}
                    onPress={onPress}
                >
                    {
                        location && 
                        <Marker coordinate={{
                        latitude: location.latitude,
                        longitude: location.longitude,
                        }} />
                    }
                </MapView>
                <View style={styles.searchContainer}>
                    <GooglePlacesInput />
                </View>
                <TouchableOpacity style={styles.gpsButton}
                onPress={readLocation}
                >
                    <Image source={iconTarget} style={{width: 18, height: 18}} />
                </TouchableOpacity>
            </View>
            <EformFooter>
                <ButtonPrimary 
                    text="Gunakan Lokasi" 
                    onPress={() => onSaveLocation()} />
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: '100%'
    },
    mapcontainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position: 'relative',
        paddingHorizontal: 24,
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    searchContainer: {
        position: 'absolute',
        top: 16,
        width: '100%',
        paddingHorizontal: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        // backgroundColor: 'red',
        // height: 40,
        borderRadius: 8, 
        borderWidth: 1, 
        borderColor: COLOR.off,
        zIndex: 10,
        shadowColor: "#393939",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.20,
        shadowRadius: 4.65,
        elevation: 2,
        overflow: 'hidden',
    },
    searchInputContainer: {
        width: '100%',
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 8, 
    },
    searchInput: {
        padding: 0,
        fontSize: 16,
        fontFamily: NUNITO.regular,
        flex: 1,
        marginRight: 16
    },
    searchButton: {
        paddingLeft: 8, 
        marginLeft: 16,
    },
    gpsButton: {
        width: 40,
        height: 40,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 20,
        bottom: 20,
        right: 12,
        backgroundColor: '#fff',
        borderRadius: 40,
        shadowColor: "#393939",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 4,
    },
})

export default EformMap