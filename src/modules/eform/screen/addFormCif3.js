import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Alert,
  Text,
  ToastAndroid,
  TouchableOpacity
} from 'react-native';

import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import ButtonSecondary from '../com/ButtonSecondary';
import ButtonPhotoUpload from '../com/ButtonPhotoUpload';
import HeaderText from '../com/HeaderText';
import FormField from '../com/FormField';
import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';

import { AcquisitionService } from '../rx/service.acquisition';
import { removeData } from '../com/FunctionStorage';
import { COLOR } from '../../../common/styles';
import { useSelector } from 'react-redux';

const AddFormCif3 = ({route, navigation}) => {
    const [loading, setLoading] = useState(false)
    const [warning, setWarning] = useState({})

    const form = useEform();
    const updateForm = useUpdateEform();
    const setForm = useSetEform();
    const SESSION = useSelector(store => store.Auth.session)

    const editAcquisition = (data, id) => {    
        return AcquisitionService.putAcquisition(data, id)
            .then((res) => {
    
                console.log('editAcquisition res')
                console.log(res)
                return res;
            })
            .catch((error) => {
                
                console.log('editAcquisition error', error)
                return [false, error];
            })
            .finally((res) => {
    
                console.log('editAcquisition finally')
                console.log(res)
                return res;
            });
    }

    const updateWarning = async (value, index) => {
        setWarning(current => ({
            ...current,
            [index]: value
        }))
    }

    const validateIsNull = async (field) => {
        if (form[field] == undefined || form[field] == '' || form[field] == null) {
            updateWarning(`Dokumen harus terlampir`, field)
            return false
        } else {
            updateWarning('', field)
            return true
        }
    }

    const onValidate = async () => {
        const IdCardDocPath = await validateIsNull('AppAcqIdCardDocPath')
        // const SelfieDocPath = await validateIsNull('AppAcqSelfieDocPath')
        // const BusinessCertDocPath = await validateIsNull('AppAcqBusinessCertDocPath')
        // const BitaddDocPath = await validateIsNull('AppAcqBitaddDocPath')
        // const PaofDocPath = await validateIsNull('AppAcqPaofDocPath')

        const validate = [
            IdCardDocPath,
        ]

        return validate.every(item => item == true)
    }

    const handleNextPage = async (page) => {     
        await setWarning({})

        const canContinue = await onValidate()
        
        if (canContinue) {
            navigation.navigate(page)
        } else {
            ToastAndroid.show(
                'Mohon periksa kembali data yang Anda masukkan',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50,
            );
        }
    }

    useEffect(() => {
        console.log('add form cif 3, form :', form)
    }, [])

    const exitToMain = () => {
        setForm({})         
        navigation.navigate('Eform')
    }
    
    return (
        <View style={{height: '100%'}}>
            <FormHeader 
                text="Form Pendaftaran Calon Mitra Tepat"
                onClose={() => exitToMain()}
            >
                <Pagination active={3} pageText={['1', '2', '3']} />
            </FormHeader>
            <ScrollView 
                style={{height: '100%', backgroundColor: '#fff'}}
            >
                <View style={{padding: 24, paddingBottom: 0}}>
                    <HeaderText>Data Lokasi Usaha</HeaderText>
                    <FormField
                        text="Lokasi"
                        warning={warning.Location}
                    >
                        {
                        form.Location != null && form.Location != undefined ? 
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', height: 40}}>
                            <Text style={{color: COLOR.tertiary}}>{form.Location}</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('EformCifMap')}>
                            <Text style={{fontWeight: 'bold', color: COLOR.tertiary}}>Ubah</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <ButtonSecondary text="Pilih Lokasi" 
                            onPress={() => navigation.navigate('EformCifMap')}
                        />
                        }
                    </FormField> 
                </View>
                <View style={styles.pageSection}>
                    <HeaderText>Dokumen Foto</HeaderText>
                    <FormField
                        // AppAcqIdCardDocPath
                        // AppAcqSignatureDocPath
                        text="Dokumen KTP dan Tanda Tangan Basah"
                        warning={warning.AppAcqIdCardDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqIdCardDocPath}
                            imageNamePrefix="AppAcqIdCardDocPath"
                            title="Dokumen KTP dan Tanda Tangan Basah"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqIdCardDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqSelfieDocPath
                        text="Foto Diri Calon Mitra Tepat"
                        info="Wajah calon Mitra Tepat melihat kedepan"
                        warning={warning.AppAcqSelfieDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqSelfieDocPath}
                            imageNamePrefix="AppAcqSelfieDocPath"
                            title="Foto Diri Calon Mitra Tepat"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqSelfieDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqBusinessCertDocPath
                        text="Surat Keterangan Usaha (optional)"
                        warning={warning.AppAcqBusinessCertDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqBusinessCertDocPath}
                            imageNamePrefix="AppAcqBusinessCertDocPath"
                            title="Surat Keterangan Usaha"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqBusinessCertDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqBitaddDocPath
                        text="Bast Pelaksanaan Pelatihan &amp; Ujian Kelayakan Mitra Tepat (optional)"
                        warning={warning.AppAcqBitaddDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqBitaddDocPath}
                            imageNamePrefix="AppAcqBitaddDocPath"
                            title="Bast Pelaksanaan Pelatihan &amp; Ujian Kelayakan Mitra Tepat"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqBitaddDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqPaofDocPath
                        text="Form Observasi Calon Mitra Tepat (optional)"
                        warning={warning.AppAcqPaofDocPath}
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqPaofDocPath}
                            imageNamePrefix="AppAcqPaofDocPath"
                            title="Form Observasi Calon Mitra Tepat"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqPaofDocPath')}
                            />
                    </FormField>
                    <FormField
                        // AppAcqNpwpDocPath
                        text="NPWP (optional)"
                    >
                        <ButtonPhotoUpload 
                            imageUrl={form.AppAcqNpwpDocPath}
                            imageNamePrefix="AppAcqNpwpDocPath"
                            title="NPWP (optional)"
                            onUploadPhoto={(url) => updateForm(url, 'AppAcqNpwpDocPath')}
                        />
                    </FormField>
                </View>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1, marginRight: 28}}>
                        <ButtonSecondary 
                            text="Kembali" 
                            onPress={() => navigation.navigate('AddFormCif2')} 
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <ButtonPrimary 
                            loading={loading}
                            disabled={loading}
                            text={form.repairFormMode ? 'Submit' : 'Lanjut'} 
                            onPress={() => handleNextPage('EformCifPks')} 
                            // onPress={() => handleNextPage('EformTnC')} 
                        />  
                    </View>
                </View>
            </EformFooter>
        </View>
    )
}

const styles = StyleSheet.create({
    pageSection: {
        padding: 24,
        paddingBottom: 4,
        borderTopWidth: 8,
        borderTopColor: '#dadada' 
    },
})

export default AddFormCif3