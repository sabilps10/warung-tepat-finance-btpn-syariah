import React, { useState, useEffect } from 'react';
import { 
    StyleSheet, 
    Text,
    Image, 
    Dimensions,
    TouchableOpacity
} from 'react-native';
import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader';
import ButtonPrimary from '../com/ButtonPrimary';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';

import imgCifError from '@assets/img/eform_cif_error.png';

const windowWidth = Dimensions.get('window').width;

import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';

const EformCifCheckError = ({route, navigation}) => {
    const [errorMessage, setErrorMessage] = useState({
        title: 'Data Tidak Tersedia',
        body: 'Data yang Anda cari tidak ditemukan, silahkan cari nomor CIF yang lain.'
    })
    
    const handleNextPage = async (page) => { 
        navigation.navigate(page);
    }

    useEffect(() => {
        const requiredFields = navigation.getParam('requiredFields', false)

        if (requiredFields) {
            console.log('required fields', requiredFields)
            
            setErrorMessage({
                title: 'Data Tidak Lengkap',
                body: `${requiredFields.join(', ')} tidak lengkap.`
            })
        }
    }, [])

    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Kembali"
                leftButton={
                    <TouchableOpacity
                        onPress={() => {
                            navigation.goBack()
                        }}
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
            />
            <View style={{padding: 24, flex: 1}}>
                <View style={styles.bodyContainer} animation="bounceIn" useNativeDriver>
                    <View style={styles.illustrationContainer}>
                        <Image 
                            source={imgCifError} 
                            style={styles.illustration} 
                            resizeMethod="scale" 
                            resizeMode="contain" 
                        />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.headerText}>
                            {errorMessage.title}
                        </Text>
                        <Text style={styles.text}>
                            {errorMessage.body}
                        </Text>
                    </View>
                </View>
                <ButtonPrimary 
                    text="Cari Lagi" 
                    onPress={() => navigation.navigate('EformInputCif')}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    summaryContainer: {
        borderRadius: 8,
        padding: 16,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    bodyContainer: {
        flex: 1,
    },
    illustrationContainer: {
        flex: 1,
        // marginBottom: 38,
        marginHorizontal: 8,
        // marginTop: 14,
    },
    illustration: {
        height: '100%',
        width: '100%',
    },
    headerText: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: 32,
        textAlign: 'center',
        marginBottom: 8,
        color: CUSTCOLOR.textDark,
    },
    bodyTextContainer: {
        marginBottom: 40,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 16,
        lineHeight: 24,
        color: CUSTCOLOR.text,
        textAlign: 'center',
    },
})

export default EformCifCheckError
