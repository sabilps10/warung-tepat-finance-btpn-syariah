import React, { Component, useEffect, useReducer, useState } from 'react';
import { StyleSheet, Text, KeyboardAvoidingView, Image, Alert, TouchableOpacity, ToastAndroid } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import OtpInputs from '../com/OtpInputs';
import FormHeader from '../com/FormHeader';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonPrimary from '../com/ButtonPrimary';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';
import { AcquisitionService } from '../rx/service.acquisition';
import { OtpService } from '../rx/service.otp';
import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';
import { removeData } from '../com/FunctionStorage';
import { Button } from 'react-native-elements';
import { useSelector } from 'react-redux';

const insertAcquisitionProspera = async (data, userId, cifCode) => {    
    return AcquisitionService.postAcquisitionProspera(data, userId, cifCode)
        .then((res) => {
            return res;
        })
        .catch((error) => {
            return [false, error];
        })
        .finally((res) => {
            return res;
        });
}

const requestOtp = async (data) => {
    return OtpService.sendOtp(data)
        .then((res) => {
            return [true, res];
        })
        .catch((error) => {            
            return [false, error];
        })
        .finally((res) => {
            return [true, res];
        });
}

const EformOtpCif = ({navigation}) => {
    const [loading, setLoading] = useState(false);
    const [generatedOtp, setGeneratedOtp] = useState('')
    const form = useEform();
    const updateForm = useUpdateEform()
    const setForm = useSetEform();
    const SESSION = useSelector(store => store.Auth.session)

    const getOtp = (otp) => {
        updateForm(otp, 'Otp')
        console.log('otp', otp)
    };

    useEffect(() => {
        console.log('halaman otp eform cif :', form)
        onRequestOtp(form.Phone_No)
    }, [])

    const onRequestOtp = async (phoneNo) => {

        let body = {
            "phone": phoneNo
        }

        const sendOtp = await requestOtp(body)
        let toastText = ''

        if (sendOtp[0]) {
            toastText = 'OTP berhasil dikirimkan.'
            console.log('otp:', sendOtp[1]["otp"])
            setGeneratedOtp(sendOtp[1]["otp"])
        } else if (sendOtp[0] == false) {
            toastText = `Gagal mengirimkan OTP. ${sendOtp[1]}`
        } else {
            toastText = 'Terjadi kesalahan.'
        }

        ToastAndroid.show(
            toastText,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
        );
    }

    const onVerification = async (data) => {
        setLoading(true);
        const userId = Number(SESSION.user.response_login_by_nik.userId);
        
        const otpCheck = verifyOtp(data.Otp)
        
        if (otpCheck == false) {
            Alert.alert(
                'Gagal',
                'Otp yang Anda masukkan salah',
            )

            setLoading(false);

            return false
        }
        
        let body = {
            "": userId,
            "AppAcqHp": String(data.Phone_No),
            "AppAcqEmail": data.Email,
            "AppAcqLatlongBusinessLocation": data.Location,
            "AppAcqOpeningAccountPurpose": data.Purpose_Of_Account,
            "AppAcqBankRelationship": data.Relation,
            "AppAcqIdCardDocPath": data.AppAcqIdCardDocPath,
            "AppAcqSelfieDocPath": data.AppAcqSelfieDocPath,
            "AppAcqSignatureDocPath": '',
            "AppAcqBusinessCertDocPath": data.AppAcqBusinessCertDocPath,
            "AppAcqBitaddDocPath": data.AppAcqBitaddDocPath,
            "AppAcqPaofDocPath": data.AppAcqPaofDocPath,
            "AppAcqNpwpDocPath": data.AppAcqNpwpDocPath,
        };

        console.log('body', body)
        
        const insertData = await insertAcquisitionProspera(body, userId, form.Cif_No)

        if (typeof insertData == 'object' && insertData[0] == false) {
            setLoading(false)

            Alert.alert(
                'Gagal',
                'Terjadi kesalahan. ' + insertData[1],
            )

            return false
        }

        if (insertData.message != null && insertData.message != undefined) {
            Alert.alert(
                'Gagal',
                `Gagal memproses data. ${insertData.message}`,
            )
    
            setLoading(false)
        } else {
            setLoading(false);
            setForm({})
            removeDraft(data.Index, '@dataDraft');
            navigation.navigate('EformSubmitted')
        }

        // if (insertData == true) {
        //     // navigation.navigate('EformSubmitted')
        // } else {
        //     Alert.alert([
        //         'Gagal',
        //         'Gagal memproses data',
        //     ])
        // }
    }

    const removeDraft = async (index, storageKey) => {
        console.log('delete ', index)
        await removeData(index, storageKey);
    }

    const verifyOtp = (otp) => {
        if (otp !== generatedOtp) return false
        if (otp === generatedOtp) return true
    }

    return (
        <View animation="fadeIn" style={styles.container} useNativeDriver>
            <FormHeader 
                text="Kembali" 
                leftButton={
                    <TouchableOpacity
                        disabled={loading}
                        onPress={() => {
                            navigation.goBack()
                            console.log('back')
                        }}
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
                // onClose={() => navigation.navigate('Eform')}
                // closeToSave={false}
            />
            <KeyboardAvoidingView 
                style={styles.bodyContainer} 
                enabled={false} 
                behavior="height"
            >
                <View style={{flex: 1}}>
                    <View style={styles.screenHeader}>
                        <Text style={styles.screenHeaderText}>Verifikasi Nomor</Text>
                        <Text style={styles.screenHeaderText}>Telepon Anda</Text>
                    </View>
                    <View style={styles.bodyTextContainer}>
                        <Text style={styles.text}>Masukkan 6 nomor kode yang</Text>
                        <Text style={styles.text}>dikirimkan ke nomor telepon Anda</Text>
                    </View>
                    <OtpInputs 
                        getOtp={(value) => getOtp(value)} 
                        // isError={this.props.errors.otp_code} 
                        // resetError={this.resetError} 
                    />
                    <View style={styles.urlContainer}>
                        <Button 
                            type="clear"
                            titleStyle={styles.url}
                            title="Kirim Ulang" 
                            onPress={() => onRequestOtp(form.Phone_No)}
                        />
                    </View>
                </View>
                <ButtonPrimary 
                    text="Verifikasi" 
                    onPress={() => onVerification(form)} 
                    disabled={(loading || !(form.Otp?.length == 6))}
                    loading={loading}
                />
            </KeyboardAvoidingView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    bodyContainer: {
        flex: 1,
        paddingTop: 32,
        paddingBottom: 16,
        paddingHorizontal: 24,
    },
    screenHeader: {
        marginBottom: 32
    },
    screenHeaderText: {
        fontSize: 24,
        lineHeight: 36,
        color: CUSTCOLOR.textDark,
        fontFamily: MARKOT.book
    },
    bodyTextContainer: {
        marginBottom: 40,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        lineHeight: 22,
        color: CUSTCOLOR.text,
    },
    urlContainer: {
        alignItems: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: NUNITO.extraBold,
        fontSize: 14,
    },
    verifButton: {
        bottom: 16,
        zIndex: 10, 
        borderWidth: 2,
        borderColor: 'red'
    }
});


export default EformOtpCif;
