import React, { Component } from 'react';
import { StyleSheet, Text, KeyboardAvoidingView, Image, Dimensions } from 'react-native';
import { View } from 'react-native-animatable';
import { COLOR } from '@common/styles';

import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';
import ButtonPrimary from '../com/ButtonPrimary';

import imgSubmitted from '@assets/img/eform_submitted.png';

const windowWidth = Dimensions.get('window').width;

const EformCifSubmitted = ({navigation}) => {
    
    return (
        <View style={styles.container}>
            <View style={styles.bodyContainer} animation="bounceIn" useNativeDriver>
                <View style={styles.illustrationContainer}>
                    <Image 
                        source={imgSubmitted} 
                        style={styles.illustration} 
                        resizeMethod="scale" 
                        resizeMode="contain" 
                    />
                </View>
                <View style={{flex: 1}}>
                    <Text style={styles.headerText}>
                        Form Pendaftaran Terkirim
                    </Text>
                    <Text style={styles.text}>
                        Link syarat ketentuan dan surat perjanjian sudah berhasil terkirim ke nomor telepon calon Mitra Tepat.
                    </Text>
                </View>
            </View>
            <ButtonPrimary 
                style={{backgroundColor: COLOR.darkGreen}} 
                text="Selesai" 
                onPress={() => navigation.navigate('Eform')}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
        paddingHorizontal: 24,
        paddingVertical: 16,
        width: windowWidth,
        justifyContent: 'space-between'
    },
    bodyContainer: {
        flex: 1,
        marginHorizontal: 8,
        justifyContent: 'center',
    },
    illustrationContainer: {
        width: '100%', flex: 1,
        marginBottom: 32,
        marginTop: 88,
    },
    illustration: {
        height: '100%',
        width: '100%',
    },
    headerText: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: 32,
        textAlign: 'center',
        marginBottom: 8,
        color: CUSTCOLOR.textDark,
    },
    bodyTextContainer: {
        marginBottom: 40,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 16,
        lineHeight: 24,
        color: CUSTCOLOR.text,
        textAlign: 'center',
    },
});


export default EformCifSubmitted;
