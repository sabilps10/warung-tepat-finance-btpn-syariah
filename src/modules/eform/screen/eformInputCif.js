import React, { useState, useEffect } from 'react';
import { 
    StyleSheet, 
    Text, 
    ScrollView, 
    Image, 
    TouchableOpacity, 
    Alert, 
    ToastAndroid 
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { View } from 'react-native-animatable';

import FormHeader from '../com/FormHeader';
import Pagination from '../com/Pagination';
import EformFooter from '../com/EformFooter';
import ButtonPrimary from '../com/ButtonPrimary';
import HeaderText from '../com/HeaderText';
import FormField from '../com/FormField';
import InputText from '../com/InputText';

import iconLeft from '@assets/icons/icon_eform_arrow_left.png';

import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';

import Config from '../common/config';
import { googlePlaceSearch } from '../common/function';
import { Modal } from 'react-native';
import ProgressBar from '../com/ProgressBar';
import { AccountsService } from '../rx/service.accounts';

const EformInputCif = ({route, navigation}) => {
    const form = useEform({});
    const updateForm = useUpdateEform();
    const setForm = useSetEform();

    const [Cif_No, setCif_No] = useState('')
    const [loading, setLoading] = useState(false)

    const [warning, setWarning] = useState({
        Cif_No: '',
    });

    useEffect(() => {
        
    }, [])

    const updateWarning = async (value, index) => {
        setWarning(current => ({
            ...current,
            [index]: value
        }))
        console.log('update', value, index, warning)
    }

    const validateIsNull = async (text) => {
        if (Cif_No == undefined || Cif_No == '' || Cif_No == null) {
            updateWarning(`${text} harus diisi`, 'Cif_No')
            return false
        } else {
            updateWarning('', 'Cif_No')
            return true
        }
    }

    const getDataFromProspera = async (query) => {
        return AccountsService.getDataFromProspera(query)
        .then((res) => {
            // if (res.code == 3) 
            
            return res;
        })
        .catch((error) => {
            return error;
        })
        .finally((res) => {
            return res;
        });
    }

    const locationGeotag = async (item) => {
        const googleResult = await googlePlaceSearch(`${item.AppAcqResidentialAddress}, ${item.AppAcqSubDistrict}, ${item.AppAcqDistrict}, ${item.AppAcqCity}, ${item.AppAcqProvince}, ${item.AppAcqZipcode}`);

        console.log('hasil googleResult :', googleResult)

        if (googleResult.status !== 'OK') return ''; 

        const location = googleResult.results[0].geometry.location;
        console.log('hasil location :', location)
        
        return `${location.lat},${location.lng}`
    }

    const fullDocPath = (docPath) => {
        let bucketName;
        
        global.ENVI == 'prod' ? bucketName = 'images' : bucketName = 'bucket03';
        
        if (docPath) {
            const fullPath = `${Config.apiURL.storage[global.ENVI]}${bucketName}/${docPath}`
            return fullPath
        }
    }

    const getData = async (cifCode) => {
        const resp = await getDataFromProspera(cifCode);
        const item = resp.data

        console.log('resp get data: ', resp)
        
        if (resp?.code == 3) {
            setLoading(false)
            navigation.navigate('EformCifCheckError', {errorStatus: ''})

            return false
        } else if (item != null) {    
            if (item.AppAcqLatlongBusinessLocation == "") {
                item.AppAcqLatlongBusinessLocation = await locationGeotag(item)
            }
                    
            const data = {
                Cif_No: cifCode,
                AppAcqCode: item.AppAcqCode,
                AppAcqId: item.AppAcqId,
                Index: item.AppAcqId,
                Name: item.AppAcqFullname,
                Phone_No: item.AppAcqHp,
                Email: item.AppAcqEmail,
                Id_Card_Type: item.AppAcqIdCardType,
                Id_Card_No: item.AppAcqIdCardNo,
                Id_Card_Exp_Date: item.AppAcqIdCardExpiredDate ? parseDate(item.AppAcqIdCardExpiredDate) : '31/12/2100',
                Tax_Card_Number: item.AppAcqNpwp,
                Place_Of_Birth: item.AppAcqPob,
                Date_Of_Birth: parseDate(item.AppAcqDob),
                Gender: item.AppAcqSex,
                Forecast_Transaction: item.AppAcqAnnualEstimatedDebitTrans,
                Street: item.AppAcqAddress,
                Street_2: item.AppAcqResidentialAddress,
                RT: String(item.AppAcqRt),
                RW: String(item.AppAcqRw),
                Province: item.AppAcqProvince,
                City: item.AppAcqCity,
                District: item.AppAcqDistrict,
                Subdistrict: item.AppAcqSubDistrict,
                Zip_Code: item.AppAcqZipcode,
                Religion: item.AppAcqReligion,
                Marital_Status: item.AppAcqMaritalStatus,
                Nationality: item.AppAcqNationality,
                Mother_Maiden_Name: item.AppAcqMotherMaidenName,
                Education: item.AppAcqEducation,
                Location: item.AppAcqLatlongBusinessLocation,
                Occupation: item.AppAcqOccupation,
                Job: item.AppAcqOccupationPosition,
                Industry_Sector_of_Employer: item.AppAcqBusinessField,
                Source_of_Fund: item.AppAcqIncomeSource,
                Income: item.AppAcqMonthlyAvgIncome,
                Annual_Income: String(item.AppAcqAnnualGrossIncome),
                Purpose_Of_Account: item.AppAcqOpeningAccountPurpose,
                Relation: item.AppAcqBankRelationship,
                AppAcqSelfieDocPath: fullDocPath(item.AppAcqSelfieDocPath),
                AppAcqBitaddDocPath: fullDocPath(item.AppAcqBitaddDocPath),
                AppAcqPaofDocPath: fullDocPath(item.AppAcqPaofDocPath),
                AppAcqNpwpDocPath: fullDocPath(item.AppAcqNpwpDocPath),
                AppAcqSignatureDocPath: fullDocPath(item.AppAcqSignatureDocPath),
                AppAcqIdCardDocPath: fullDocPath(item.AppAcqIdCardDocPath),
                AppAcqBusinessCertDocPath: fullDocPath(item.AppAcqBusinessCertDocPath),
                Status_Approval: item.AppAcqStatusApproval,
            }

            console.log('data : ', data)

            return data
        } else {
            return false
        }
    }

    const parseDate = (dateStr) => {
        const dateObj = new Date(dateStr);

        const strPadStart = (str) => {
            return String(str).padStart(2, '0')
        }
        
        const strDate = strPadStart(dateObj.getDate())
        const strMonth = strPadStart(dateObj.getMonth() + 1)
        const strYear = dateObj.getFullYear()

        return `${strDate}/${strMonth}/${strYear}`
    }

    const onValidate = async (data) => {
        let requiredFields = []
        
        if (!data.Id_Card_No) requiredFields.push('No Identitas')
        if (!data.Name) requiredFields.push('Nama')
        if (!data.Place_Of_Birth) requiredFields.push('Tempat Lahir')
        if (!data.Date_Of_Birth) requiredFields.push('Tanggal Lahir')
        if (!data.Gender) requiredFields.push('Jenis Kelamin')
        if (!data.Street) requiredFields.push('Alamat')
        if (!data.RT) requiredFields.push('RT')
        if (!data.RW) requiredFields.push('RW')
        if (!data.Province) requiredFields.push('Provinsi')
        if (!data.City) requiredFields.push('Kabupaten/Kota')
        if (!data.District) requiredFields.push('Kecamatan')
        if (!data.Subdistrict) requiredFields.push('Kelurahan')
        if (!data.Religion) requiredFields.push('Agama')
        if (!data.Marital_Status) requiredFields.push('Status Pernikahan')
        if (!data.Nationality) requiredFields.push('Kewarganegaraan')
        if (!data.Id_Card_Exp_Date) requiredFields.push('Masa berlaku identitas')
        if (!data.Mother_Maiden_Name) requiredFields.push('Nama gadis ibu kandung')
        if (!data.Zip_Code) requiredFields.push('Kode Pos')
        if (!data.Education) requiredFields.push('Pendidikan')
        if (!data.Occupation) requiredFields.push('Pekerjaan')
        if (!data.Job) requiredFields.push('Posisi Pekerjaan')
        if (!data.Industry_Sector_of_Employer) requiredFields.push('Bidang/Sektor Usaha')
        if (!data.Source_of_Fund) requiredFields.push('Sumber Dana')
        if (!data.Income) requiredFields.push('Penghasilan rata-rata bulanan')
        if (!data.Annual_Income) requiredFields.push('Penghasilan kotor per tahun')
        if (!data.Forecast_Transaction) requiredFields.push('Perkiraan transaksi debit per tahun')
        if (!data.Purpose_Of_Account) requiredFields.push('Tujuan pembukaan rekening')
        if (!data.Relation) requiredFields.push('Hubungan dengan Bank')
        // if (!data.Tax_Card_Number) requiredFields.push('NPWP')

        if (requiredFields.length == 0) {
            return true
        } else {
            navigation.navigate('EformCifCheckError', {requiredFields})
            return false
        }
    }

    const handleCifCheck = async (page) => {
        setLoading(true)
        const validateCif = await validateIsNull('Nomor CIF')

        if (validateCif == false) return false

        const data = await getData(Cif_No);

        if (data == false) {
            setLoading(false)
            return navigation.navigate('EformCifCheckError')
        }

        const canContinue = await onValidate(data)

        if (canContinue === true) {
            await setForm(data);
            setLoading(false)

            return navigation.navigate('EformCifCheckSuccess');
        } else {
            setLoading(false)
        }

        console.log('data from cif', data)
    }

    const raiseProgress = (value) => {
        const rand = Math.ceil(Math.random() * 20) 
        if (value < 100) {
            setTimeout(() => {
                value += rand
                setProgress(value)
            }, 1)
        }
    }
    const [visible, setVisible] = useState(false)
    const [progress, setProgress] = useState(0)
    
    useEffect(() => {
        raiseProgress(progress)
    }, [progress])

    
    return (
        <View style={{height: '100%', backgroundColor: '#fff', justifyContent: 'space-between'}}>
            <FormHeader 
                text="Kembali"
                leftButton={
                    <TouchableOpacity
                        onPress={() => {
                            navigation.goBack()
                        }}
                    >
                        <Image source={iconLeft} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                }
            />
            <ScrollView>
                <View style={{padding: 24}}>
                    <HeaderText>Masukkan Nomor CIF</HeaderText>
                    <FormField
                        text="Nomor CIF"
                        warning={warning.Cif_No}
                    >
                        <InputText
                            placeholder="W000000000"
                            value={Cif_No}
                            onChangeText={(value) => setCif_No(value)}
                        />
                    </FormField>
                </View>
            </ScrollView>
            <EformFooter>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flex: 1}}>
                        <ButtonPrimary text="Periksa"
                            loading={loading}
                            disabled={loading}
                            onPress={() => handleCifCheck()}
                            // onPress={() => {
                            //     setVisible(true)
                            //     // raiseProgress()
                            //     setTimeout(() => raiseProgress(), 100)
                            // }}
                        />
                    </View>
                </View>
            </EformFooter>
            {/* <ProgressBar visible={visible} progress={progress} /> */}
        </View>
    )
}

export default EformInputCif
