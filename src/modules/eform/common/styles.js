import { Platform, StyleSheet } from 'react-native';
import { COLOR } from '@common/styles';

const platform = Platform.OS;

/**
 * NunitoSans
 */
export const NUNITO = {
    bold: 'NunitoSans-Bold',
    regular: 'NunitoSans-Regular',
    extraBold: 'NunitoSans-ExtraBold',
};
/**
 * MarkOT
 */
export const MARKOT = {
    regular: 'MarkOT',
    bold: 'MarkOT-Bold',
    book: 'MarkOT-Book'
};

/**
 * Colors
 */
export const CUSTCOLOR = {
    // Color
    primary: COLOR.primary,
    secondary: '#00777a',
    primaryLight: '#eeb333',

    checkGreen: '#00c781',
    border: '#CCCCCC',

    text: '#555555',
    textDark: '#333333',
    textLight: '#999990',
};
