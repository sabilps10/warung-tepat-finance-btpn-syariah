import { MastersService } from "../rx/service.masters";

const getMaster = async (queries, id = '') => {
    return MastersService.getMasters(queries, id)
    .then((res) => {
        return res;
    })
    .catch((error) => {
        return error;
    })
    .finally((res) => {
        return res;
    });
}

export const getData = async (type, maxRowCount = 999) => {
    const queries = {
        type,
        maxRowCount
    };
    
    const items = await getMaster(queries);

    return items.data;
}

export const getMasterBankRelations = async () => {   
    return await getData('BANK_RELATIONS');
}

export const getMasterBusinessFields = async () => {   
    return await getData('BUSINESS_FIELDS');
}

export const getAllMasterDatas = async () => {
    const datas = {
        bankRelations: await getMasterBankRelations(),
        businessFields: await getMasterBusinessFields()
    }

    return datas;
}