import React from 'react';
import {
  StyleSheet
} from 'react-native';
import { CUSTCOLOR, NUNITO } from '../common/styles';
import { Button } from 'react-native-elements';

const ButtonPrimary = (props) => {
    return (
        <Button
            {...props}
            buttonStyle={[
                styles.button,
                {
                    ...props.style
                }
            ]}
            title={props.text}
            titleStyle={styles.text}
        />
    )
}

const styles = StyleSheet.create({
    button: {
        padding: 8,
        borderRadius: 8,
        alignItems: 'center',
        backgroundColor: CUSTCOLOR.primary
    },
    text: {
        color: '#fff',
        fontFamily: NUNITO.extraBold,
        fontSize: 16,
    }
})

export default ButtonPrimary;