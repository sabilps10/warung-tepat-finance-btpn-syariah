import React, { useState, useEffect } from 'react';
import {  
    View,
    LayoutAnimation, Platform, UIManager,
} from 'react-native';

const ExpandableView = (props) => {    
    const [open, setOpen] = useState(false)

    if (Platform.OS === 'android') {
        if (UIManager.setLayoutAnimationEnabledExperimental) {
          UIManager.setLayoutAnimationEnabledExperimental(true);
        }
      }

    const toggleExpand = (open) => {
        console.log('toggleExpand expandable start')
        // LayoutAnimation.configureNext(
        //     LayoutAnimation.create(
        //       200, 'easeOut', 'scaleY'
        //     )
        // );
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
        setOpen(open)
        console.log('toggleExpand expandable end')
    }

    useEffect(() => {
        toggleExpand(props.open)
        console.log('use effect expandable')
    }, [props.open])

    return (
        <View style={{height: open ? null : 0, overflow: 'hidden'}}>
            {props.children}
        </View>
    )
}

export default ExpandableView