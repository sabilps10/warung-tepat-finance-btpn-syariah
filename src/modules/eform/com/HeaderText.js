import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { CUSTCOLOR, MARKOT } from '../common/styles';

const HeaderText = (props) => {
    return (
        <Text 
            {...props}
            style={[styles.text, props.style]}
        >
            {props.children}
        </Text>
    )
}

const styles = StyleSheet.create({
    text: {
        fontFamily: MARKOT.bold,
        color: CUSTCOLOR.textDark, 
        fontSize: 18,
        marginBottom: 16,
        lineHeight: 24
    }
})

export default HeaderText;