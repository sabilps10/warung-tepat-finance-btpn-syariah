import React, { useState, useEffect, useRef } from 'react';
import { TouchableOpacity } from 'react-native';
import {
    View,
    Text,
    Modal,
    Dimensions,
    StyleSheet,
    Animated
} from 'react-native';

import { COLOR } from '../../../common/styles';
import { CUSTCOLOR, MARKOT, NUNITO } from '../common/styles';

const radius = 72;
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

export const SemiCircle = ({color = CUSTCOLOR.primaryLight, style}) => {
    return (
        <Animated.View style={[{
            width: radius,
            height: radius,
            position: 'absolute'
        }, {
            ...style
        }]}>
            <View style={{
                width: radius,
                aspectRatio: 2,
                borderTopLeftRadius: radius,
                borderTopRightRadius: radius,
                backgroundColor: color
            }} />
            <View style={{
                width: radius,
                aspectRatio: 2,
                borderBottomLeftRadius: radius,
                borderBottomRightRadius: radius,
                backgroundColor: null            
            }} />
        </Animated.View>
    )
}

const ProgressBar = ({progress = 0, visible}) => {
    const animatedProgress = useRef(new Animated.Value(0)).current;
    const progressText = useRef()
    const [count, setCount] = useState(0)
    const [modalVisible, setModalVisible] = useState(false)

    const animateProgress = useRef(toValue => {
        Animated.spring(animatedProgress, {
            toValue,
            useNativeDriver: true,
            speed: 6
        }).start();
        // console.log('animate', animatedProgress)
    }).current;

    const raiseNumber = () => {
        while (count <= progress && count <= 100) {
            setInterval(() => setCount(count => count + 1), 100)
        }
    }

    useEffect(() => {
        animateProgress(progress);
        setCount(progress)

        // raiseNumber(progress)

        // return () => {
        //     clearInterval(progressText.current)
        // }
    }, [animateProgress, progress])

    const firstIndicatorRotate = animatedProgress.interpolate({
        inputRange: [0, 50],
        outputRange: ['0deg', '180deg'],
        extrapolate: 'clamp'
    })

    const secondIndicatorRotate = animatedProgress.interpolate({
        inputRange: [0, 100],
        outputRange: ['0deg', '360deg'],
        extrapolate: 'clamp'
    })

    const secondIndicatorVisibility = animatedProgress.interpolate({
        inputRange: [0, 49, 50, 100],
        outputRange: [0, 0, 1, 1],
        extrapolate: 'clamp'
    })

    useEffect(() => {
        setModalVisible(visible)
    }, [visible])

    useEffect(() => {
        if (progress >= 100) return setTimeout(() => setModalVisible(false), 100)
    }, [progress])
    
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
        >
            <View style={styles.modal}>
                <TouchableOpacity 
                    style={styles.modalOverlay} 
                    onPress={() => setModalVisible(false)}
                    activeOpacity={0}
                />
                <View style={styles.background}>
                    <View style={styles.circleContainer}>
                        <SemiCircle color={COLOR.primary} style={{transform: [{rotate: firstIndicatorRotate}]}} />
                        <SemiCircle />
                        <SemiCircle color={COLOR.primary} style={{
                            transform: [{rotate: secondIndicatorRotate}],
                            opacity: secondIndicatorVisibility
                        }} />
                        <View style={{
                            backgroundColor: '#fff',
                            width: radius - 9,
                            aspectRatio: 1,
                            borderRadius: radius,
                            position: 'absolute',
                            zIndex: 10,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Text style={{
                                fontFamily: MARKOT.bold,
                                fontSize: 18,
                                color: CUSTCOLOR.textDark,
                                lineHeight: 24,
                                transform: [{rotate: '-90deg'}]
                            }}>
                                {count <= 100 ? count : 100}%
                            </Text>
                        </View>
                    </View>
                    <Text style={styles.text}>Dalam proses...</Text>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    pageOverlay: {
        position: 'absolute',
        top: 0,
        width: '100%',
        height: '100%',
        padding: 40,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1000
    },
    background: {
        backgroundColor: '#fff',
        position: 'relative',
        width: 180,
        height: 180,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        paddingVertical: 35
    },
    circleContainer: {
        backgroundColor: CUSTCOLOR.primaryLight,
        width: radius,
        aspectRatio: 1,
        borderRadius: radius,
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
        transform: [{rotate: '90deg'}],
        marginBottom: 16
    },
    modal: {
        position: 'relative',
        flexDirection: 'column',
        height: '100%',
        width: '100%',
        paddingTop: 40,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalOverlay: {
        backgroundColor: '#000',
        height: windowHeight,
        width: windowWidth,
        opacity: 0.5,
        position: 'absolute',
        zIndex: 0,
        top: 0,
        left: 0,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: 14,
        color: CUSTCOLOR.text
    }
})

export default ProgressBar