import React, { useEffect, useState } from 'react';

import AreaPicker from './AreaPicker';

import { MastersService } from '../rx/service.masters';

const StatePicker = (props) => {
    const [options, setOptions] = useState([]);
    
    const getAreas = async (provinceId = 0, stateId = 0, districtId = 0, subDistrictId = 0) => {
        const query = {
            maxRowCount: 999
        }
        
        return MastersService.getAreas(query, provinceId, stateId, districtId, subDistrictId)
        .then((res) => {
            let mappedOptions = res.data.map((data) => {
                return {
                    ...data,
                    id: data.unitId,
                    label: data.name, 
                    value: data.unitId
                }
            })

            setOptions(mappedOptions)
        })
        .catch((error) => {
            return error;
        })
        .finally((res) => {
            return res;
        });
    }

    useEffect(() => {
        if (props.provinceId) {
            getAreas(props.provinceId)
        }
    }, [props.provinceId])

    return (
        <AreaPicker 
            searchInput={true}
            placeholder={props.placeholder}
            areaPicker
            options={options}
            disabled={props.disabled}
            selectedValue={props.selectedValue}
            onValueChange={(value) => {
                props.areaValue(value)
            }}
            selectedValue={props.selectedValue}
        />
    )
}

export default StatePicker;