import React from 'react';
import {
  TouchableOpacity,
  View,
  Image,
  Alert,
  StyleSheet
} from 'react-native';

import HeaderText from './HeaderText'

import iconClose from '@assets/icons/icon_eform_close.png';
import { useEform, useSetEform, useUpdateEform } from '../context/EformContext';
import { addData } from './FunctionStorage';


const FormHeader = (props) => {
    const form = useEform();
    const updateForm = useUpdateEform();
    const setForm = useSetEform();

    const addStorage = async () => {
        const index = form.Index
        const storage = await addData(form, '@dataDraft', index);
        
        return storage;
    }

    const onAddStorage = async () => {
        await addStorage()        
    }
    
    const closeToSave = () => {
        Alert.alert(
            'Kembali',
            'Apakah Anda ingin menyimpan draft dan kembali ke halaman utama?',
            [
                {
                    text: "Batal",
                    style: "cancel"
                },
                {
                    text: "Kembali",
                    onPress: () => {
                        props.onClose();
                        updateForm({});
                        setForm({});
                    }
                },
                { 
                    text: "Simpan dan Kembali", 
                    onPress: () => {
                        const storage = onAddStorage()

                        if (storage) {
                            props.onClose();
                            updateForm({})
                            setForm({})
                        }
                    }
                }
            ],
            { cancelable: true }
        )
    }

    return (
        <View style={Styles.headerContainer}>
            <View style={Styles.headerText}>
                <View style={{flexDirection: 'row'}}>
                    {props.leftButton}
                    <HeaderText style={{marginBottom: 0, marginLeft: props.leftButton != undefined ? 8 : 0 }}>{props.text}</HeaderText>
                </View>
                <View style={{flexDirection: 'row'}}>
                    {props.rightButton}
                    {
                        (props.onClose != undefined && props.onClose != null) &&
                        <TouchableOpacity 
                            onPress={() => {
                                if (props.closeToSave) {
                                    closeToSave()
                                } else {
                                    props.onClose()
                                    setForm({})
                                }
                            }}
                        >
                            <Image source={iconClose} style={{width: 24, height: 24}} />
                        </TouchableOpacity>
                    }
                </View>
            </View>
            {props.children}
        </View>
    )
}

const Styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    headerText: {
        paddingHorizontal: 24,
        paddingVertical: 14,
        marginTop: 2,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    }
})

export default FormHeader;