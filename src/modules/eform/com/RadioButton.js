import React, { useState, useEffect } from 'react';
import {
  TouchableOpacity,
//   View,
  Text,
  StyleSheet
} from 'react-native';

import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import { CUSTCOLOR, NUNITO } from '../common/styles';

const RadioButton = (props) => {
    const { options, selectedValue } = props;
    const [value, setValue] = useState(selectedValue)

    useEffect(() => {
        props.callback(value)
    }, [value])

    return (
        <View style={{flexDirection: 'row'}}>
        {options.map(res => {
            return (
            <TouchableOpacity 
                key={res.key} style={styles.container}
                activeOpacity={1}
                onPress={() => {
                    setValue(res.key);
                }}
            >
                <View
                    style={[
                    styles.radioButton,
                    {borderColor: value === res.key ? COLOR.primary : COLOR.off}
                    ]}
                >
                    {
                        value === res.key && 
                        <View style={styles.innerCircle} 
                            animation="pulse" useNativeDriver
                        />
                    }
                </View>
                <Text style={styles.radioText}>{res.text}</Text>
            </TouchableOpacity>
            );
        })}
        </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 6,
  },
  radioButton: {
    width: 20,
    height: 20,
    borderRadius: 20,
    borderWidth: 2,
    padding: 2, 
    alignItems: 'center',
    marginRight: 18
  },
  innerCircle: {
    width: 12,
    height: 12,
    borderRadius: 12,
    backgroundColor: COLOR.primary
  },
  radioText: {
    fontFamily: NUNITO.regular,
    fontSize: 16,
    lineHeight: 26,
    color: CUSTCOLOR.text
  }
})

export default RadioButton;