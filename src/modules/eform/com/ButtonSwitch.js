import React, { useEffect } from 'react';
import {
    TouchableOpacity,
    View,
    StyleSheet,
    LayoutAnimation, Platform, UIManager
} from 'react-native';
import { COLOR } from '@common/styles';
import { func } from 'prop-types';

if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}

const ButtonSwitch = (props) => {
    const { value } = props;

    const toggleSlide = () => {
        LayoutAnimation.configureNext(
            LayoutAnimation.create(200, 'easeOut', 'opacity')
        );
    }

    const handleSwitch = async () => {
        const newVal = await !value;
        props.callback(newVal)
        toggleSlide()
    }
    
    useEffect(() => {
        return function cleanUp() {
            
        }
    }, [value])
    
    return (
        <TouchableOpacity 
            activeOpacity={1}
            style={[
                Style.switchContainer,
                {
                backgroundColor: value == true ? COLOR.primary : '#cccccc',
                // alignItems: value == true ? 'flex-end' : 'flex-start'
                }
            ]} 
            onPress={() => handleSwitch()}
        >
            <View>
                <View style={[Style.innerCircle, value == true ? Style.slideRight : null]}></View>
            </View>
        </TouchableOpacity>
    )
}

const Style = StyleSheet.create({
    switchContainer: {
        width: 44,
        height: 24,
        borderRadius: 24,
        justifyContent: 'center'
    },
    innerCircle: {
        height: 16,
        width: 16,
        borderRadius: 16,
        backgroundColor: '#fff',
        margin: 2
    },
    slideRight: {
        alignSelf: 'flex-end'
    }
})

export default ButtonSwitch;