import OpeningAccountsRest from './rest.openingAccounts';

export const service = class OpeningAccountsService {
    constructor() {
        this.rest = new OpeningAccountsRest();
    }

    getOpeningAccountsHistory(query, id = '', userId) {
        // console.log('GET Acquisition')
        return this.rest.getOpeningAccountsHistory(query, id, userId)
    }
};

export const OpeningAccountsService = new service();