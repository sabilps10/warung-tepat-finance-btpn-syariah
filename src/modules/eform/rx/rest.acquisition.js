import Rest from './rest'

export default class AcquisitionRest extends Rest {
    postAcquisition(data, userId) {
        const headers = [{ 
            'X-User-Id': userId,
            'X-Correlation-Id': `eform${userId}`
        }]
        return this.POST('v1/acquisitions', data, headers)
    }

    postAcquisitionTemporary(data, userId) {
        const headers = [{ 
            'X-User-Id': userId,
            'X-Correlation-Id': `eform${userId}`
        }]
        return this.POST('v1/acquisitions/temp', data, headers)
    }
    
    postAcquisitionProspera(data, userId, cifCode) {
        const headers = [{ 
            'X-User-Id': userId,
            'X-Correlation-Id': `eform${userId}`
        }]
        return this.POST(`v1/acquisitions/${cifCode}/prospera`, data, headers)
    }
    
    getAcquisition(query, id = '', userId) {
        const headers = [{ 'X-User-Id': userId }]
        return this.GET('v1/acquisitions', query, id, headers)
    }
    
    putAcquisition(query, id, userId) {
        const headers = [{ 'X-User-Id': userId }]
        return this.PUT('v1/acquisitions', query, id, headers)
    }
}