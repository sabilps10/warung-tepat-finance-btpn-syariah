import AccountsRest from './rest.accounts';

export const service = class MastersService {
    constructor() {
        this.rest = new AccountsRest();
    }

    getDataFromProspera(query) {
        return this.rest.getDataFromProspera(query)
    }
};

export const AccountsService = new service();