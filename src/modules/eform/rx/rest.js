import { ToastAndroid } from 'react-native';
import Config from '../common/config'

export default class Rest {
    constructor() {
        this.headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
    }
    
    async _fetch(route, method, body, isQuery = false, headers = null) {
        const envi = global.ENVI
        const api_url = Config.apiURL.eform[envi];
        const api_key = global.API_KEY;

        let fullRoute = `${api_url}${route}`;
        
        console.log('fetch body',body)
        if (isQuery && body) {
            let qs = require('qs');
            const query = qs.stringify(body);
            fullRoute = `${fullRoute}?${query}`;
            body = undefined;
        }

        if (headers) {
            headers.map(header => {
                Object.assign(this.headers, header);
            })
            console.log('rest headers : ', this.headers)
        }

        if (envi === 'prod') {
            Object.assign(this.headers, { 'X-API-KEY': api_key })
        }

        // if (xUserId != '') {
        //     Object.assign(this.headers, { 'X-User-Id': xUserId });
        //     console.log('rest headers : ', this.headers)
        // }

        let opts = {
            method,
            headers: this.headers,
        };

        if (body) {
            Object.assign(opts, { body: JSON.stringify(body) });
        }

        console.log('fetch route',fullRoute, opts)

        let response = {};

        try {
            // console.log('req 1')
            let req = await fetch(fullRoute, opts)
                .then(res => {
                    console.log('req fetch', res)
                    return res;
                })
                .catch((error) => {
                    if (!isObject(error)) {
                        ToastAndroid.show(
                            'Tidak Dapat Terhubung Dengan Server',
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50,
                        );
                    }

                    throw error;
                });
                
            let resp = await req.json();

            // console.log('resp api', resp)

            return resp;
        } catch (e) {
            throw e;
        }
    }

    POST(route, body, headers = null) {
        // console.log('POST')
        return this._fetch(route, 'POST', body, false, headers)
    }

    GET(route, query, id='', headers = null) {
        // console.log('GET')
        if (id != '') {
            route = `${route}/${id}`
        }
        return this._fetch(route, 'GET', query, true, headers)
    }

    PUT(route, body, id, headers = null) {
        // console.log('PUT')
        if (id != '') {
            route = `${route}/${id}`
        } else {
            return false
        }
        return this._fetch(route, 'PUT', body, false, headers)
    }
}