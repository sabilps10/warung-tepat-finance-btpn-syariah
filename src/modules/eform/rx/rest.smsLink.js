import Rest from './rest'

export default class SmsRest extends Rest {
    sendSms(data) {
        return this.POST(`v1/sms`, data)
    }

    sendSmsLink(data, userId = '') {
        const headers = [{ 'X-Correlation-Id': userId }]
        return this.POST(`v1/sms-link`, data, headers)
    }
}