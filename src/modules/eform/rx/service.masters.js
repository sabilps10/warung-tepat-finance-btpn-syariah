import MastersRest from './rest.masters';

export const service = class MastersService {
    constructor() {
        this.rest = new MastersRest();
    }

    getMasters(query, id='') {
        // console.log('GET Acquisition')
        return this.rest.getMasters(query, id)
    }

    getAreas(query, provinceId, stateId, districtId, subDistrictId) {
        return this.rest.getAreas(query, provinceId, stateId, districtId, subDistrictId)
    }
};

export const MastersService = new service();