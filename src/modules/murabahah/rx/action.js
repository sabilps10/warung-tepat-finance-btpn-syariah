import { AkadService } from './service';

export const TYPE = {
    FETCH_AKAD_CONTENT: 'akad::fetch.content',
};

export function $load(data) {
    return (dispatch) => {
        return AkadService.post(data)
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_AKAD_CONTENT,
                    payload: res.data,
                });

                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                return res;
            });
    };
}
