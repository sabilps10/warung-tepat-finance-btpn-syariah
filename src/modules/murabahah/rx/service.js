import AkadRest from '../../../services/rest/rest.akad';

export const service = class AkadService {
    constructor() {
        this.rest = new AkadRest();
    }

    post(body) {
        return this.rest.postAkad(body);
    }
};

export const AkadService = new service();
