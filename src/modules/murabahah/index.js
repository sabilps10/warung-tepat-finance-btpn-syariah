import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, ActivityIndicator } from 'react-native';
import { Col, Row } from 'react-native-easy-grid';
import { Button, CheckBox } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/id';

import { $load } from './rx/action';
import { COLOR, PADDING, SSP, FONT_SIZE } from '../../common/styles';
import { CurrencyFormatter } from '../../common/utils';

class MurabahahScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            buttonDisabled: true,
        };

        this._onPressCheckbox = this._onPressCheckbox.bind(this);
        this._enableButton = this._enableButton.bind(this);
        this._goToVerify = this._goToVerify.bind(this);
    }

    componentDidMount() {
        this.onLoad();
    }

    _paylaterType = () => {
        const { akadData } = this.props;
        if (akadData.paylater_type === 'REGULAR') {
            return <Text style={styles.textIndent}>Bayar Bulanan</Text>;
        } else if (akadData.paylater_type === 'IRREGULAR') {
            return <Text style={styles.textIndent}>Bayar di Akhir</Text>;
        } else {
            return <Text style={styles.textIndent}>-</Text>;
        }
    };

    onLoad() {
        this.props.$load({ bank_id: 3 });
    }

    isCloseToBottom({ layoutMeasurement, contentOffset, contentSize }) {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
    }

    _onPressCheckbox() {
        this.setState({ checked: !this.state.checked, buttonDisabled: !this.state.buttonDisabled });
    }

    _enableButton() {
        if (!this.state.checked && this.state.buttonDisabled) {
            this.setState({ checked: true, buttonDisabled: false });
        }
    }

    _goToVerify() {
        this.props.navigation.navigate('VerifyPin', { bank_id: 3 });
    }

    render() {
        const { akadData } = this.props;
        moment().locale('id');

        if (akadData.length < 1) {
            return <ActivityIndicator style={styles.containerLoading} size={65} color={COLOR.primary} />;
        }

        return (
            <View style={styles.container}>
                <View style={styles.containerNumber}>
                    <Text style={styles.subTitle}>Nomor</Text>
                    <Text style={styles.title}>{akadData.kode_akad}</Text>
                </View>
                <ScrollView
                    onScroll={({ nativeEvent }) => {
                        if (this.isCloseToBottom(nativeEvent)) {
                            this._enableButton();
                        }
                    }}
                >
                    <View style={styles.textContent}>
                        <Text style={styles.textDescription}>
                            Akad Pembiayaan Murabahah ini (selanjutnya disebut “Akad Pembiayaan”) dibuat dan disetujui
                            pada hari ini, {moment().format('dddd')} tanggal {moment().format('DD')} bulan{' '}
                            {moment().format('MMMM')} tahun {moment().format('YYYY')} ({moment().format('DD/MM/YYYY')}),
                            bertempat di {akadData.lokasi}, oleh dan di antara:
                        </Text>
                        <Text style={styles.textDescription}>
                            1. PT Bank BTPN Syariah Tbk, berkedudukan di Jakarta Selatan, dalam hal ini diwakili oleh{' '}
                            {akadData.pejabat[0].name} dan {akadData.pejabat[1].name}, dalam kedudukannya berturut-turut
                            selaku {akadData.pejabat[0].jabatan} dan {akadData.pejabat[1].jabatan} (selanjutnya disebut
                            “Bank”);
                        </Text>
                        <View style={styles.textDescription}>
                            <Text style={styles.textNormal}>2. Nama: {akadData.nama}</Text>
                            <Text style={styles.textIndent}>Nomor KTP: {akadData.ktp}</Text>
                            <Text style={styles.textIndent}>(selanjutnya disebut “Nasabah”)</Text>
                        </View>
                        <Text style={[styles.textDescription, styles.mb30]}>
                            Bank dan Nasabah selanjutnya secara bersama-sama disebut “Para Pihak”.
                        </Text>
                        <Text style={styles.textDescription}>
                            Sebelumnya Para Pihak menerangkan terlebih dahulu bahwa dengan tunduk pada Perjanjian
                            Penyediaan Fasilitas (<Text style={styles.italic}>Line Facility</Text>) Nomor{' '}
                            {akadData.limit_induk_id} tanggal {moment(akadData.limit_induk_date).format('Do MMMM YYYY')}{' '}
                            (selanjutnya disebut “Perjanjian <Text style={styles.italic}>Line Facility</Text>”) yang
                            telah disetujui oleh Nasabah, Para Pihak setuju untuk membuat Akad Pembiayaan ini dengan
                            syarat dan ketentuan sebagai berikut:
                        </Text>
                        <Text style={styles.textDescription}>PASAL 1. PEMBERIAN FASILITAS PEMBIAYAAN</Text>
                        <View style={styles.mb30}>
                            <Text style={styles.textNormal}>1. Tujuan Pembiayaan:</Text>
                            <Text style={styles.textIndent}>Modal Usaha</Text>
                            <View>
                                <Text style={styles.textNormal}>2. Jumlah Pembiayaan:</Text>
                                <Text style={styles.textIndent}>
                                    {CurrencyFormatter(akadData.total_order)} dengan perincian sebagai berikut
                                </Text>
                                <View style={styles.textFlex}>
                                    <Text style={styles.textLeft}>Harga Beli</Text>
                                    <Text style={styles.textRight}>: {CurrencyFormatter(akadData.total_order)}</Text>
                                </View>
                                <View style={styles.textFlex}>
                                    <Text style={styles.textLeft}>Margin</Text>
                                    <Text style={styles.textRight}>: {CurrencyFormatter(akadData.biaya_margin)}</Text>
                                </View>
                                <View style={styles.textFlex}>
                                    <Text style={styles.textLeft}>Harga Jual</Text>
                                    <Text style={styles.textRight}>
                                        : {CurrencyFormatter(akadData.jml_fasilitas_pembiayaan)}
                                    </Text>
                                </View>
                            </View>
                            <Text style={styles.textNormal}>3. Tenor Pembiayaan:</Text>
                            <Text style={styles.textIndent}>{akadData.tenor} bulan</Text>
                            <Text style={styles.textNormal}>4. Jumlah Angsuran:</Text>
                            <Text style={styles.textIndent}>{CurrencyFormatter(akadData.cicilan)}</Text>
                            <Text style={styles.textNormal}>5. Jatuh Tempo Pembayaran:</Text>
                            <Text style={styles.textIndent}>
                                Setiap Tanggal {moment(akadData.jatuh_tempo).format('DD')}
                            </Text>
                            <Text style={styles.textNormal}>6. Tanggal Berakhir Pembiayaan:</Text>
                            <Text style={styles.textIndent}>{moment(akadData.jatuh_tempo).format('DD MMMM YYYY')}</Text>
                            <Text style={styles.textNormal}>7. Cara Pembayaran:</Text>
                            {this._paylaterType()}
                            <Text style={styles.textNormal}>8. Nomor Rekening Nasabah:</Text>
                            <Text style={styles.textIndent}>{akadData.raw_data.data.accountWow} </Text>
                        </View>
                        <Text style={styles.textDescription}>PASAL 2. KETENTUAN PENUTUP</Text>
                        <View style={styles.mb30}>
                            <Text style={styles.textDescription}>
                                1. Nasabah telah membaca dan memahami seluruh Akad Pembiayaan ini serta Nasabah telah
                                memperoleh informasi yang jelas, lengkap dan benar.
                            </Text>
                            <Text style={styles.textDescription}>
                                2. Akad Pembiayaan ini tunduk pada serta merupakan satu kesatuan dan bagian yang tidak
                                terpisahkan dari Perjanjian <Text style={styles.italic}>Line Facility</Text> dan
                                Ketentuan Pembiayaan.
                            </Text>
                            <Text style={styles.textDescription}>
                                3. Nasabah setuju dengan memasukan nomor PIN Nasabah, maka Nasabah dengan ini memberikan
                                persetujuan Akad Pembiayaan.
                            </Text>
                            <Text style={styles.textDescription}>
                                4. Nasabah setuju bahwa persetujuan yang diberikan berfungsi juga sebagai tanda terima
                                Akad Pembiayaan dan uang atas Fasilitas Pembiayaan yang diberikan Bank sebesar Jumlah
                                Pembiayaan sesuai yang tercantum pada Akad Pembiayaan.
                            </Text>
                        </View>
                        <View style={styles.mb30}>
                            <Text>Bank</Text>
                            <Text style={styles.title}>PT Bank BTPN Syariah Tbk</Text>
                            <Text style={styles.title}>{akadData.pejabat[0].name}</Text>
                        </View>
                        <View style={styles.mb30}>
                            <Text>Nasabah</Text>
                            <Text style={styles.title}>{akadData.nama}</Text>
                        </View>
                    </View>
                    <View style={styles.borderBottom} />
                    <View>
                        <CheckBox
                            title="Dengan ini Saya menyatakan telah membaca dan memahami isi dari Akad Pembiayaan Murabahah ini."
                            checked={this.state.checked}
                            onPress={this._onPressCheckbox}
                        />
                    </View>
                </ScrollView>
                <Row style={styles.footerButton}>
                    <Col style={styles.colStyle}>
                        <Button
                            title="Batal"
                            buttonStyle={[styles.button, { backgroundColor: COLOR.lightRed }]}
                            titleStyle={styles.buttonText}
                            containerStyle={styles.width100}
                            onPress={() => this.props.navigation.goBack()}
                        />
                    </Col>
                    <Col style={styles.colStyle}>
                        <Button
                            title="Setuju"
                            buttonStyle={styles.button}
                            titleStyle={styles.buttonText}
                            containerStyle={styles.width100}
                            disabled={this.state.buttonDisabled}
                            onPress={this._goToVerify}
                        />
                    </Col>
                </Row>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        akadData: state.Murabahah.akadData,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $load,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(MurabahahScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerLoading: {
        flex: 1,
        justifyContent: 'center',
    },
    containerNumber: {
        padding: 16,
    },
    subTitle: {
        fontSize: 12,
        color: COLOR.off,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    textContent: {
        backgroundColor: COLOR.white,
        padding: 16,
    },
    textDescription: {
        marginBottom: 14,
        fontSize: 16,
        color: COLOR.textGray,
    },
    textIndent: {
        marginBottom: 6,
        fontSize: 16,
        color: COLOR.textGray,
        marginLeft: 19,
    },
    textFlex: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: 6,
        marginLeft: 19,
    },
    textLeft: {
        fontSize: 16,
        color: COLOR.textGray,
        width: 100,
    },
    textRight: {
        fontSize: 16,
        color: COLOR.textGray,
    },
    textNormal: {
        marginBottom: 6,
        fontSize: 16,
        color: COLOR.textGray,
        fontWeight: 'bold',
    },
    footerButton: {
        bottom: 0,
        height: 72,
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    buttonOutlined: {
        borderColor: COLOR.secondary,
        width: '100%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        letterSpacing: 1.2,
        fontSize: FONT_SIZE.MEDIUM,
    },
    colStyle: {
        width: '50%',
        paddingRight: 15,
    },
    button: {
        backgroundColor: COLOR.darkGreen,
        width: '100%',
    },
    width100: { width: '100%' },
    width85: { width: '85%' },
    mb30: {
        marginBottom: 30,
    },
    borderBottom: {
        height: 8,
        backgroundColor: COLOR.backgroundCart,
    },
    switchContainer: {
        width: 24,
        height: 24,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 16,
    },
    innerCheck: {
        height: 24,
        width: 24,
    },
    italic: { fontStyle: 'italic' },
});
