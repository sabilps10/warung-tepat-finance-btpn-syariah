import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from './auth/index';
import PasswordScreen from './auth/screen/password';
import ForgotPasswordScreen from './auth/screen/forgotPassword';
import NewPasswordScreen from './auth/screen/newPassword';
import VerifyOtpScreen from './auth/screen/verifyOtp';
import SuccessScreen from './auth/screen/success';
import LimitOtpScreen from './auth/screen/limitOtp';
import LimitUserScreen from './auth/screen/limitUser';
import NotUserScreen from './auth/screen/notUser';
import BlockedUserScreen from './auth/screen/blockedUser';

const LoginStack = createStackNavigator(
    {
        Login: {
            screen: LoginScreen,
        },
        Password: {
            screen: PasswordScreen,
        },
        ForgotPassword: {
            screen: ForgotPasswordScreen,
        },
        NewPassword: {
            screen: NewPasswordScreen,
        },
        VerifyOtp: {
            screen: VerifyOtpScreen,
        },
        Success: {
            screen: SuccessScreen,
        },
        LimitOtp: {
            screen: LimitOtpScreen,
        },
        LimitUser: {
            screen: LimitUserScreen,
        },
        NotUser: {
            screen: NotUserScreen,
        },
        BlockedUser: {
            screen: BlockedUserScreen,
        },
    },
    {
        lazy: true,
        headerMode: 'none',
        initialRouteName: 'Login',
    },
);

const LoginRoutes = createAppContainer(LoginStack);
export default LoginRoutes;
