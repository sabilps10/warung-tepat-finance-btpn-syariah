import { TYPE } from './action';

const INITIAL_STATE = {
    data: {},
    isConnected: false,
    error: false,
    verified: false,
    isLoading: false,
    connectLoading: true,
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.BEGIN_CONNECT:
            return {
                ...state,
                isLoading: action.payload.isLoading,
            };
        case TYPE.CONNECT_SUCCESS:
            return {
                ...state,
                verified: action.payload,
                error: action.payload.error,
                isLoading: action.payload.isLoading,
            };
        case TYPE.CONNECT_FAILED:
            return {
                ...INITIAL_STATE,
                verified: action.payload,
                isLoading: action.payload.isLoading,
            };
        case TYPE.CONNECT_ERROR:
            return {
                ...INITIAL_STATE,
                verified: action.payload,
                error: action.payload.error,
                isLoading: action.payload.isLoading,
            };
        case TYPE.VERIFY_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isConnected: true,
                connectLoading: false,
            };
        case TYPE.VERIFY_ERROR:
            return {
                ...INITIAL_STATE,
                isConnected: false,
                connectLoading: false,
            };
        case TYPE.RESET:
            return {
                ...state,
                error: false,
            };
        default:
            return state;
    }
}

export function persister({ error, isLoading }) {
    return {
        error,
        isLoading,
    };
}
