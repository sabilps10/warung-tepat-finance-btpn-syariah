import ConnectSaldoRest from '@src/services/rest/rest.link.saldo';

const rest = new ConnectSaldoRest();

export const ConnectSaldoServiceImplementation = class ConnectSaldoService {
    verifyUser(data) {
        return rest.getVerifyUser(data);
    }

    connectUser(data) {
        return rest.postConnectUser(data);
    }
};

export const ConnectSaldoService = new ConnectSaldoServiceImplementation();
