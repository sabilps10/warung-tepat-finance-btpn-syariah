import React from 'react';
import { StyleSheet } from 'react-native';
import { COLOR, NUNITO } from '../../../common/styles';
import { Button } from 'react-native-elements';

const ButtonPrimary = (props) => {
    return (
        <Button
            {...props}
            buttonStyle={[
                styles.button,
                {
                    ...props.style,
                },
            ]}
            title={props.text}
            titleStyle={styles.text}
            onPress={props.onPress}
        />
    );
};

const styles = StyleSheet.create({
    button: {
        padding: 8,
        borderRadius: 8,
        alignItems: 'center',
        backgroundColor: COLOR.primary,
    },
    text: {
        color: '#fff',
        fontFamily: NUNITO.extraBold,
        fontSize: 14,
    },
});

export default ButtonPrimary;
