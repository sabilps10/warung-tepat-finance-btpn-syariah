import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { View } from 'react-native-animatable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { COLOR } from '@common/styles';

import ButtonSubmit from './com/button';
import PinInputs from '../../components/PinInputs';
import { $connectUser, $resetError } from './rx/action';

class verifyPinSaldoScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pin: '',
        };
    }

    resetError = () => {
        this.props.$resetError();
    };

    getPin = (pin) => {
        this.setState({ pin });
    };

    submit = () => {
        const data = {
            mobile_no: this.props.username,
            pin: this.state.pin,
        };

        this.props.$connectUser(data);
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <View style={styles.header}>
                    <Text style={styles.title}>Masukkan PIN</Text>
                    <Text style={styles.title}>Rekening Anda</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>
                        Masukkan 6 digit PIN rekening anda yang telah didaftarkan di Cabang.
                    </Text>
                    <PinInputs getOtp={this.getPin} isError={this.props.error} resetError={this.resetError} />
                    <ButtonSubmit
                        title="Submit"
                        loading={this.props.isLoading}
                        onPress={this.submit}
                        disabled={this.state.pin.length < 6 ? true : false}
                        flexGrow
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 24,
        marginTop: 16,
        marginBottom: 32,
    },
    title: {
        fontFamily: 'Roboto-Bold',
        fontSize: 24,
        lineHeight: 36,
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.textGray,
        marginBottom: 16,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        username: state.Auth.session.user.username,
        error: state.LinkSaldo.error,
        isLoading: state.LinkSaldo.isLoading,
        summary: state.Cart.summary,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $connectUser, $resetError }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(verifyPinSaldoScreen);
