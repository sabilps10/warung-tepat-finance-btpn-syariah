import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { $show, $checkCart } from './rx/action';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { ListItem } from 'react-native-elements';
import PropTypes from 'prop-types';
import CheckBox from '@react-native-community/checkbox';

import { COLOR, FONT_SIZE, SSP } from '../../common/styles';
import { CurrencyFormatter } from '../../common/utils';
import CartDetail from './detail';

class CartList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: false,
            checklistItem: true,
            isEmpty: false,
            hasData: false,
            disableButton: false,
            dataNew: [],
        };
    }

    componentDidMount() {
        const { cart } = this.props;

        if (cart.checkout_mark === 1) {
            this.setState({
                checklistItem: true,
            });
        } else if (cart.checkout_mark === 0) {
            this.setState({
                checklistItem: false,
            });
        }
        this.onLoad();
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.cart !== prevProps.cart) {
            this.onLoad();
        } else if (this.props.checkData !== prevProps.checkData) {
            this.props.onRender();
        } else if (this.props.cart.subtotal !== prevProps.cart.subtotal) {
            this.props.onRender();
        }
    }

    onLoad() {
        this.props.$show(this.props.cart.id).then((res) => {
            if (res) {
                this.setState({ data: res });
            } else {
                this.props.onRender();
            }
        });
    }

    onDelete = () => {
        this.props.onDelete(this.props.cart.id);
    };

    toSupplierPage = () => {
        const { cart } = this.props;
        const supplierName = cart.cart_items[0].supplier.name;
        const supplierCode = cart.cart_items[0].supplier.code;
        const type = true;
        this.props.navigation.push(
            'Supplier',
            {
                supplierCode,
                supplierName,
                type,
            },
            true,
        );
    };

    checkCart = (value) => {
        const { cart } = this.props;

        if (cart.checkout_mark === 0) {
            if (value === true) {
                this.props.$checkCart({ cart_id: cart.id, mark_checkout: true });
                this.setState({
                    checklistItem: value,
                });
            }
        } else if (cart.checkout_mark === 1) {
            if (value === false) {
                this.props.$checkCart({ cart_id: cart.id, mark_checkout: false });
                this.setState({
                    checklistItem: value,
                });
            }
        }
    };

    renderKK() {
        const { cart } = this.props;
        const supplierName = cart.cart_items[0].supplier.name.toUpperCase();

        return (
            <ListItem
                leftElement={
                    <CheckBox
                        disabled={false}
                        value={this.state.checklistItem}
                        onValueChange={(value) => this.checkCart(value)}
                    />
                }
                title={supplierName}
                rightElement={
                    <View>
                        <TouchableOpacity onPress={() => this.toSupplierPage()}>
                            <Text style={styles.add} navigation={this.props.navigation}>
                                + Tambah Barang
                            </Text>
                        </TouchableOpacity>
                    </View>
                }
                titleStyle={styles.listTitle}
                subtitleStyle={styles.listSubTitle}
                containerStyle={styles.listContainer}
            />
        );
    }

    renderItem() {
        const { data } = this.state;
        const { cart, error } = this.props;
        const supplierCode = Number(cart.cart_items[0].supplier.code);
        let isError = false;

        if (!data.cart_items) {
            return null;
        }

        if (error) {
            error.map((item) => {
                if (item.supplier_id === supplierCode) {
                    isError = item.min_order;
                }
            });
        }

        return (
            <View>
                <View style={styles.row}>
                    {isError ? (
                        <ListItem
                            title={'Minimal pemesanan ' + CurrencyFormatter(isError)}
                            leftIcon={{
                                type: 'Ionicons',
                                name: 'error',
                                color: COLOR.info,
                                containerStyle: {
                                    padding: 0,
                                },
                            }}
                            titleStyle={styles.errorTitle}
                            containerStyle={styles.errorContainer}
                        />
                    ) : null}
                </View>
                <View style={[styles.row, styles.flex]}>
                    <Text style={styles.order}>{data.cart_items.length} ORDER ITEM</Text>
                    <TouchableOpacity onPress={() => this.onDelete()}>
                        <Text style={styles.delete}>Hapus</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        const { cart, checkData } = this.props;
        const stock = cart.cart_items[0].is_available;

        return (
            <View style={styles.container}>
                {this.renderKK()}
                {this.renderItem()}
                <View>
                    <CartDetail
                        disabled={stock === 0}
                        cart={cart}
                        navigation={this.props.navigation}
                        onRender={this.props.onRender}
                        onChange={this.props.onChange}
                    />
                </View>
                <View style={styles.sectionContainer} />
            </View>
        );
    }
}

CartList.propTypes = {
    disabled: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {
    return {
        carts: state.Cart.carts,
        checkData: state.Cart.checkData,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $show,
            $checkCart,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(CartList);

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 0.5,
        borderBottomColor: COLOR.border,
    },
    listContainer: {
        marginBottom: 0,
        paddingBottom: 0,
    },
    add: {
        color: COLOR.darkGreen,
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    delete: {
        color: COLOR.danger,
        fontSize: FONT_SIZE.SMALL,
        letterSpacing: 2,
        fontFamily: SSP.bold,
    },
    order: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        letterSpacing: 2,
    },
    listTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        letterSpacing: 1,
    },
    listSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    row: {
        paddingTop: 10,
        paddingRight: 16,
        paddingBottom: 10,
        paddingLeft: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: COLOR.border,
    },
    flex: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    errorTitle: {
        color: COLOR.lightGray,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    errorContainer: {
        backgroundColor: '#d3efff',
        borderRadius: 8,
        padding: 8,
    },
    sectionContainer: {
        backgroundColor: COLOR.backgroundCart,
        height: 8,
    },
});
