import React, { Component } from 'react';
import { RefreshControl, View, ScrollView, StyleSheet, BackHandler } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { $delete, $load, $loadSummary, $counter } from './rx/action';
import CartHeader from './com/cart_header';
import CartList from './cartList';
import CartSummary from './com/cart_summary';
import CartFooter from './com/cart_footer';
import CartEmpty from './empty';
import { COLOR, FLEX, MARGIN } from '../../common/styles';
import PkaDialog from '../../components/dialog';
import { LmdToast } from '../auth/com/lmdToast';
import LoadingDialog from './com/loading_dialog';

class CartIndex extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isEmpty: false,
            hasData: false,
            cartId: false,
            showDialog: false,
            showLoadingDialog: false,
            disableButton: false,
            data: [],
        };
        this.doLoad = this.doLoad.bind(this);
    }

    componentDidMount() {
        this.onRender();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.navigate('Main');
        return true;
    };

    doLoad() {
        this.props.$load().then((res) => {
            if (res) {
                const newData = res.map((item, index) => {
                    return { ...item };
                });
                this.setState({
                    isEmpty: false,
                    hasData: true,
                    data: newData,
                    disableButton: false,
                });
            } else {
                this.setState({
                    isEmpty: true,
                    hasData: false,
                });
            }
        });
        this.props.$loadSummary();
        this.props.$counter();
    }

    doDelete = () => {
        this.props.$delete(this.state.cartId).then((res) => {
            this.toggleDialog();
            this.doLoad();
        });
    };

    onRender = () => {
        this.doLoad();
    };

    onChange = () => {
        this.doLoad();
    };

    onDelete = (id) => {
        this.setState({ cartId: id });
        this.toggleDialog();
    };

    toggleDialog = () => {
        this.setState({ showDialog: !this.state.showDialog });
    };

    renderCartLists() {
        let error = false;

        if (this.props.navigation.state.params) {
            error = this.props.navigation.state.params.error;
        }

        const items = [];
        // eslint-disable-next-line no-unused-vars
        for (const [index, item] of this.state.data.entries()) {
            items.push(
                <CartList
                    key={item.id}
                    id={item.id}
                    title={item.kk.name.toUpperCase()}
                    cart={item}
                    navigation={this.props.navigation}
                    onRender={this.onRender}
                    onChange={this.onChange}
                    onDelete={this.onDelete}
                    error={error}
                />,
            );
        }
        return items;
    }

    renderSummary() {
        const { data } = this.state;
        let discount = 0,
            saving = 0;

        // eslint-disable-next-line no-unused-vars
        for (const item of data) {
            discount += item.discount;
            saving += item.total_saving;
        }

        return <CartSummary discount={discount} saving={saving} />;
    }

    renderDialog() {
        const { showDialog } = this.state;

        return (
            <PkaDialog
                onCancel={this.toggleDialog}
                onSubmit={this.doDelete}
                title="Hapus Keranjang"
                text={'Anda yakin akan menghapus Keranjang Belanja ini?'}
                visible={showDialog}
            />
        );
    }

    renderLoadingDialog() {
        const { showLoadingDialog } = this.state;
        return <LoadingDialog visible={showLoadingDialog} />;
    }

    toCheckout() {
        let { summary } = this.props;

        if (summary.total_transaction !== 0) {
            this.setState({ showLoadingDialog: true });
            this.doValidateStockAndCheckout();
        } else {
            LmdToast.showToast(
                'Anda belum memilih produk yang akan di beli. Mohon memilih salah satu barang.',
                'danger',
                3500,
                ...Array(1),
                'X',
                { alignSelf: 'center' },
                {
                    backgroundColor: '#ef3636',
                    borderRadius: 6,
                    alignSelf: 'center',
                    top: -64,
                    maxWidth: 400,
                    minHeight: 71,
                },
            );
            this.setState({ disableButton: true });
        }
    }

    doValidateStockAndCheckout() {
        this.props.$load(false).then((res) => {
            if (res) {
                const newData = res.map((item, index) => {
                    return { ...item };
                });
                this.setState({
                    data: newData,
                    showLoadingDialog: false,
                });

                if (this.doSearchEmptyStock(res)) {
                    this.props.navigation.navigate('Checkout');
                    return;
                }

                this.setState({ disableButton: true });
            }
        });
    }

    doSearchEmptyStock(cart) {
        // eslint-disable-next-line no-unused-vars
        for (const item of cart) {
            // eslint-disable-next-line no-unused-vars
            for (const cartItem of item.cart_items) {
                if (cartItem.is_available === 0 && cartItem.cart.checkout_mark === 0) {
                    return true;
                }
            }
            for (const cartItem of item.cart_items) {
                if (cartItem.is_available === 0 && cartItem.cart.checkout_mark === 1) {
                    LmdToast.showToast(
                        'Ada stok produk yang habis, hapus produk untuk melanjutkan chekout',
                        'danger',
                        3500,
                        ...Array(1),
                        'X',
                        { alignSelf: 'center' },
                        {
                            backgroundColor: '#ef3636',
                            borderRadius: 6,
                            alignSelf: 'center',
                            top: -64,
                            maxWidth: 400,
                            minHeight: 71,
                        },
                    );
                    return false;
                }
            }
        }
        return true;
    }

    renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this.onRender} colors={[COLOR.primary]} />;
    }

    render() {
        let { isEmpty, hasData, disableButton } = this.state;

        if (isEmpty === hasData) {
            return null;
        }
        if (isEmpty && !hasData) {
            return <CartEmpty navigation={this.props.navigation} />;
        }

        return (
            <View style={styles.view}>
                <CartHeader navigation={this.props.navigation} />
                <ScrollView style={styles.scrollView} refreshControl={this.renderRefresh()}>
                    {this.renderCartLists()}
                    {this.renderSummary()}
                </ScrollView>
                <CartFooter
                    navigation={this.props.navigation}
                    goToCheckout={this.toCheckout.bind(this)}
                    disabled={disableButton}
                />
                {this.renderDialog()}
                {this.renderLoadingDialog()}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        carts: state.Cart.carts,
        loading: state.Activity.page,
        summary: state.Cart.summary,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $load,
            $delete,
            $loadSummary,
            $counter,
        },
        dispatch,
    );
};

const styles = StyleSheet.create({
    view: {
        flex: FLEX.full,
        backgroundColor: COLOR.white,
    },
    scrollView: {
        marginBottom: MARGIN.accordion,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CartIndex);
