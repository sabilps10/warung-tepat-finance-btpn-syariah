import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { $process } from '../rx/action';
import { connect } from 'react-redux';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { CurrencyFormatter } from '../../../common/utils';
import CartQuantity from './cart_quantity';
import { ListItem } from 'react-native-elements';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';
import { Icon } from 'react-native-elements';

class CartItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            quantity: this.props.quantity ? this.props.quantity : 1,
            subtotal: this.props.quantity * this.props.item.selling_price,
        };

        this.onChanges = this.onChanges.bind(this);
        this.toCatalog = this.toCatalog.bind(this);
    }

    toCatalog() {
        let { navigation, catalog } = this.props;

        setTimeout(() => {
            navigation.navigate('CatalogDetail', {
                catalog_id: catalog.id,
                product: catalog,
            });
        }, 0);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (
            this.state.subtotal !== nextState.subtotal ||
            this.state.quantity !== nextState.quantity ||
            this.props.item !== nextProps.item
        ) {
            return true;
        }

        return false;
    }

    onChanges(qty) {
        let { kk, catalog } = this.props;
        let subtotal = qty * catalog.selling_price;
        let body = {
            catalog_id: catalog.id,
            quantity: qty,
        };

        this.props.$process(kk.id, body).then((res) => {
            if (!res) {
                this.setState({
                    quantity: qty - 1,
                    subtotal: subtotal - catalog.selling_price,
                });
            } else {
                this.setState({
                    quantity: qty,
                    subtotal: subtotal,
                });
                this.props.onChange();
            }
        });
    }

    render() {
        const { catalog, item, is_group_buying, isWebLink, itemsCart } = this.props;
        const { quantity, subtotal } = this.state;
        const addCart = false;
        const stock = item.is_available;
        const available = item.available_stock;
        const catalogSize = itemsCart.catalog.size;

        return (
            <View>
                <ListItem
                    leftAvatar={{ source: { uri: catalog.image_default } }}
                    title={
                        <TouchableOpacity
                            disabled={stock === 0}
                            onPress={this.toCatalog}
                            activeOpacity={7}
                            style={stock === 0 ? styles.disableContainer : styles.enableContainer}
                        >
                            <Text style={styles.cartItemTitle}>{catalog.name.toUpperCase()}</Text>
                            {catalogSize && <Text style={styles.cartItemSubTitle}>{`Isi: ${catalogSize}`}</Text>}
                            <Text style={styles.cartItemRightTitle}>{`${CurrencyFormatter(subtotal)}`}</Text>
                        </TouchableOpacity>
                    }
                    rightElement={
                        <View style={styles.counterContainer}>
                            <TouchableOpacity
                                style={{
                                    ...styles.deleteContainer,
                                    backgroundColor: isWebLink ? COLOR.lightGray : COLOR.danger,
                                }}
                                onPress={isWebLink ? false : () => this.onChanges(0)}
                                activeOpacity={7}
                            >
                                <Icon name="delete" type="Feather" color={COLOR.white} size={16} />
                            </TouchableOpacity>
                            <CartQuantity
                                available={available}
                                addCart={addCart}
                                stock={stock}
                                quantity={quantity}
                                disabled={is_group_buying || isWebLink}
                                onChange={this.onChanges}
                                delay={0.1}
                            />
                        </View>
                    }
                    bottomDivider={true}
                />
            </View>
        );
    }
}

CartItem.propTypes = {
    navigation: PropTypes.object.isRequired,
    kk: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    catalog: PropTypes.object.isRequired,
    quantity: PropTypes.number,
    onChange: PropTypes.func,
    is_group_buying: PropTypes.bool,
    disabled: PropTypes.bool,
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $process }, dispatch);
};

export default connect(null, mapDispatchToProps)(CartItem);

const styles = StyleSheet.create({
    productList: {
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
        marginTop: -30,
        borderColor: COLOR.border,
        backgroundColor: 'transparent',
    },
    counterContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    deleteContainer: {
        marginRight: 14.5,
        height: 25,
        width: 25,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cartItemContainer: {
        paddingTop: 5,
    },
    cartItemTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    cartItemRightTitle: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    cartItemCODTitle: {
        color: COLOR.darkGreen,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    cartItemSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    row: {
        paddingTop: 10,
        paddingRight: 16,
        paddingBottom: 10,
        paddingLeft: 16,
        borderBottomWidth: 0.5,
        borderBottomColor: COLOR.border,
    },
    stockTitle: {
        color: COLOR.white,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    stockContainer: {
        backgroundColor: COLOR.danger,
        borderRadius: 8,
        padding: 8,
    },
    disableContainer: {
        opacity: 0.2,
    },
    enableContainer: {
        backgroundColor: COLOR.white,
    },
    CODContainer: {
        flexDirection: 'row',
        marginLeft: '10%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 3,
    },
});
