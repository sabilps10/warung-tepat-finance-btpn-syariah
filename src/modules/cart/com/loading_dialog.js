import React, { Component } from 'react';
import { Text, ActivityIndicator, StyleSheet } from 'react-native';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

class loading_dialog extends Component {
    render() {
        const { visible } = this.props;
        return (
            <Dialog visible={visible} width={0.5} height={180}>
                <DialogContent style={styles.dialogContainer}>
                    <ActivityIndicator size={80} color="#ff7c00" style={styles.spinnerStyle} />
                    <Text style={styles.textStyle}>Dalam proses...</Text>
                </DialogContent>
            </Dialog>
        );
    }
}

const styles = StyleSheet.create({
    spinnerStyle: {
        height: 72,
        width: 72,
    },
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 35,
        height: 180,
    },
    textStyle: {
        fontSize: 16,
        color: '#555555',
    },
});

export default loading_dialog;
