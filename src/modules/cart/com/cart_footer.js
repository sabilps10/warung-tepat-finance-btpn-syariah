import React, { PureComponent } from 'react';
import { Grid, Row } from 'react-native-easy-grid';
import { Button } from 'react-native-elements';
import { StyleSheet, Text } from 'react-native';
import { CurrencyFormatter } from '../../../common/utils';
import { connect } from 'react-redux';
import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';

import PropTypes from 'prop-types';

class CartFooter extends PureComponent {
    toCheckout() {
        const { goToCheckout } = this.props;
        goToCheckout();
    }

    render() {
        let { summary, disabled } = this.props;
        return (
            <Row style={styles.footerContainer}>
                <Grid style={styles.gridSummary}>
                    <Row>
                        <Text style={styles.subtitle}>TOTAL BELANJA</Text>
                    </Row>
                    <Row>
                        <Text style={styles.title}>{`${CurrencyFormatter(summary.total_transaction)}`}</Text>
                    </Row>
                </Grid>
                <Grid style={styles.grid}>
                    <Button
                        title="CHECK OUT"
                        buttonStyle={styles.button}
                        titleStyle={styles.buttonText}
                        onPress={() => this.toCheckout()}
                        disabled={disabled}
                    />
                </Grid>
            </Row>
        );
    }
}

CartFooter.propTypes = {
    navigation: PropTypes.object.isRequired,
    goToCheckout: PropTypes.func,
    disabled: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {
    return {
        summary: state.Cart.summary,
    };
};

export default connect(mapStateToProps)(CartFooter);

const styles = StyleSheet.create({
    footerContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        paddingLeft: 16,
        paddingRight: 0,
        paddingTop: 16,
        paddingBottom: 14,
    },
    gridSummary: {
        width: 10,
    },
    grid: {
        marginLeft: -10,
    },
    button: {
        backgroundColor: COLOR.tertiaryLight,
        width: '110%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        letterSpacing: 1.2,
        fontSize: FONT_SIZE.MEDIUM,
    },
    subtitle: {
        fontSize: FONT_SIZE.SMALL,
        letterSpacing: 1.2,
    },
    title: {
        fontSize: FONT_SIZE.LARGE,
        fontFamily: SSP.bold,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        letterSpacing: 1,
    },
});
