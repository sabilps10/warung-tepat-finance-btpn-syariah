import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ListItem } from 'react-native-elements';
import { StyleSheet, View, Text, Dimensions, Image } from 'react-native';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';
import iconWarning from '@assets/icons/icon_system_alert_exclamation.png';
import { CurrencyFormatter } from '../../../common/utils';
import { $verifyPaylater } from '../../profile/rx/action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 27;

class PaymentMethod extends PureComponent {
    toPaylater = () => {
        this.props.navigation.navigate('ProfilePembiayaan');
    };

    render() {
        let { data, isActive, onSelected } = this.props;
        //paylater
        if (data.id === 3) {
            this.props.$verifyPaylater();
            //paylater bisa digunakan
            if (data.is_useable) {
                return (
                    <View>
                        <ListItem
                            title={data.name}
                            subtitle={data.account_name}
                            rightTitle={data.data ? CurrencyFormatter(data.data.limit_murabahah) : null}
                            leftIcon={{
                                name: isActive ? 'radio-button-checked' : 'radio-button-off',
                                color: isActive ? COLOR.primary : COLOR.border,
                                size: 24,
                            }}
                            titleStyle={styles.listTitle}
                            subtitleStyle={styles.listSubTitle}
                            rightTitleStyle={styles.listRightTitle}
                            onPress={() => onSelected(data)}
                            underlayColor={'transparent'}
                            disabled={data.is_useable === false}
                        />
                        <ListItem
                            title={<Text style={styles.detailPaylaterTitle}>Lihat Detail Pembiayaan</Text>}
                            containerStyle={
                                data.is_useable === false ? styles.listDisableContainer : styles.listContainer
                            }
                            rightTitleStyle={styles.listRightTitle}
                            underlayColor={'transparent'}
                            onPress={() => this.toPaylater()}
                        />
                    </View>
                );
            } else {
                //paylater tidak bisa digunakan
                return (
                    <View>
                        <ListItem
                            title={data.name}
                            subtitle={data.account_name}
                            rightTitle={data.data ? CurrencyFormatter(data.data.limit_murabahah) : null}
                            leftIcon={{
                                name: isActive ? 'radio-button-checked' : 'radio-button-off',
                                color: isActive ? COLOR.primary : COLOR.border,
                                size: 24,
                            }}
                            containerStyle={
                                data.is_useable === false ? styles.listDisableContainer : styles.listContainer
                            }
                            titleStyle={styles.listTitle}
                            subtitleStyle={styles.listSubTitle}
                            rightTitleStyle={styles.listRightTitle}
                            onPress={() => onSelected(data)}
                            underlayColor={'transparent'}
                            disabled={data.is_useable === false}
                        />
                        <ListItem
                            title={<Text style={styles.detailPaylaterTitle}>Lihat Detail Pembiayaan</Text>}
                            rightTitleStyle={styles.listRightTitle}
                            underlayColor={'transparent'}
                            onPress={() => this.toPaylater()}
                        />
                        <ListItem
                            title={
                                <View style={styles.warningPaylaterContainer}>
                                    <Image source={iconWarning} style={styles.iconImageWarning} resizeMode="contain" />
                                    <Text style={styles.textWarning}>{data.reason}</Text>
                                </View>
                            }
                            containerStyle={styles.listContainer}
                            underlayColor={'transparent'}
                        />
                    </View>
                );
            }
        } else if (data.is_useable) {
            //method pembayaran bisa digunakan
            return (
                <ListItem
                    title={data.name}
                    subtitle={data.account_name}
                    leftIcon={{
                        name: isActive ? 'radio-button-checked' : 'radio-button-off',
                        color: isActive ? COLOR.primary : COLOR.border,
                        size: 24,
                    }}
                    containerStyle={styles.listContainer}
                    titleStyle={styles.listTitle}
                    subtitleStyle={styles.listSubTitle}
                    onPress={() => onSelected(data)}
                    disabled={data.is_useable === false}
                    underlayColor={'transparent'}
                />
            );
        } else {
            //method pembayaran tidak bisa digunakan
            return (
                <View>
                    <ListItem
                        title={data.name}
                        subtitle={data.account_name}
                        leftIcon={{
                            name: isActive ? 'radio-button-checked' : 'radio-button-off',
                            color: isActive ? COLOR.primary : COLOR.border,
                            size: 24,
                        }}
                        containerStyle={data.is_useable === false ? styles.listDisableContainer : styles.listContainer}
                        titleStyle={styles.listTitle}
                        subtitleStyle={styles.listSubTitle}
                        onPress={() => onSelected(data)}
                        disabled={data.is_useable === false}
                        underlayColor={'transparent'}
                    />
                    <ListItem
                        title={
                            <View style={styles.warningContainer}>
                                <Image source={iconWarning} style={styles.iconImageWarning} resizeMode="contain" />
                                <Text style={styles.textWarning}>{data.reason}</Text>
                            </View>
                        }
                        containerStyle={styles.listContainer}
                        underlayColor={'transparent'}
                    />
                </View>
            );
        }
    }
}

PaymentMethod.propTypes = {
    navigation: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    isActive: PropTypes.bool,
    onSelected: PropTypes.func,
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $verifyPaylater,
        },
        dispatch,
    );
};

export default connect(null, mapDispatchToProps)(PaymentMethod);

const styles = StyleSheet.create({
    listContainer: {
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingVertical: 10,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
        backgroundColor: 'transparent',
    },
    listTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        letterSpacing: 1.2,
    },
    listRightTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.black,
    },
    listSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    listDisableContainer: {
        opacity: 0.2,
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingVertical: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    emptyReasonContainer: {
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
    },
    detailPaylaterTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGreen,
        marginTop: -15,
        marginLeft: 40,
    },
    warningContainer: {
        width: DEVICE_WIDTH - MARGIN,
        height: 45,
        borderRadius: 10,
        backgroundColor: COLOR.warningRed,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        marginTop: -5,
        marginHorizontal: '0.2%',
    },
    warningPaylaterContainer: {
        width: DEVICE_WIDTH - MARGIN,
        height: 45,
        borderRadius: 10,
        backgroundColor: COLOR.warningRed,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8,
        marginTop: -20,
        marginHorizontal: '0.2%',
    },
    textWarning: {
        color: COLOR.lightRed,
        marginLeft: 10,
        marginRight: 14,
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
    iconImageWarning: {
        width: 24,
        height: 24,
        top: '-2%',
        marginLeft: -2,
    },
});
