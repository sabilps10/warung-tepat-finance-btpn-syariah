import React, { Component } from 'react';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { ListItem } from 'react-native-elements';
import PropTypes from 'prop-types';

import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';
import CartQuantity from './cart_quantity';
import { bindActionCreators } from 'redux';
import { $process, $counter, $show } from '../rx/action';
import { connect } from 'react-redux';

class Item extends Component {
    constructor(props) {
        super(props);

        this.state = {
            quantity: this.props.kk.quantity ? this.props.kk.quantity : 1,
            selected: this.props.selected,
            data: [],
        };
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.quantity !== this.state.quantity || nextProps.selected !== this.state.selected) {
            return true;
        }

        return false;
    }

    _onPress = () => {
        let selected = !this.state.selected;

        this.setState({ selected: selected });

        if (!selected) {
            this.onChanges(0);
        } else {
            this.onChanges(1);
        }
    };

    onChanges = (qty) => {
        let { $process, $counter, kk, catalogID } = this.props;

        let body = {
            catalog_id: catalogID,
            quantity: qty,
        };

        $process(kk.id, body).then((res) => {
            if (!res) {
                if (qty === 1) {
                    this.setState({ selected: false });
                } else {
                    this.setState({ quantity: qty - 1 });
                }
            } else {
                this.setState({ quantity: qty });
            }
            $counter();
        });
    };

    render() {
        const { kk, stock } = this.props;
        let { quantity, selected } = this.state;
        const addCart = true;
        return (
            <TouchableNativeFeedback onPress={this._onPress}>
                <ListItem
                    containerStyle={styles.itemContainer}
                    title={kk.name.toUpperCase()}
                    subtitle={kk.phone}
                    leftIcon={{
                        name: 'check',
                        color: selected ? COLOR.success : '#F4F4F4',
                        size: 20,
                    }}
                    rightElement={
                        selected ? (
                            <CartQuantity
                                addCart={addCart}
                                quantity={quantity}
                                stock={stock}
                                onChange={this.onChanges}
                            />
                        ) : null
                    }
                    titleStyle={styles.title}
                    subtitleStyle={styles.subtitle}
                />
            </TouchableNativeFeedback>
        );
    }
}

Item.propTypes = {
    kk: PropTypes.object.isRequired,
    catalogID: PropTypes.number,
    selected: PropTypes.bool,
    cart: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => {
    return {
        counter: state.Cart.counter,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $process, $counter, $show }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Item);

const styles = StyleSheet.create({
    itemContainer: {
        padding: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: COLOR.light,
    },
    title: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    subtitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
});
