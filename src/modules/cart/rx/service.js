import CartRest from '../../../services/rest/rest.cart';

export const service = class CartService {
    constructor() {
        this.rest = new CartRest();
    }

    get() {
        return this.rest.get();
    }

    getSummary() {
        return this.rest.getSummary();
    }

    checkCart(data) {
        return this.rest.checkCart(data);
    }

    getByCatalog(cid) {
        return this.rest.getByCatalog(cid);
    }

    getPaymentMethod() {
        return this.rest.getPaymentMethod();
    }

    show(id) {
        return this.rest.show(id);
    }

    delete(id) {
        return this.rest.delete(id);
    }

    process(kkID, data) {
        let body = {
            catalog_id: data.catalog_id,
            quantity: data.quantity,
        };

        return this.rest.process(kkID, body);
    }

    finish(id) {
        return this.rest.finish(id);
    }

    checkout(data) {
        return this.rest.checkout(data);
    }

    verifyOrder(data) {
        return this.rest.verifyOrder(data);
    }

    verify(data) {
        let body = {
            code: data.code,
        };

        return this.rest.verify(body);
    }

    getCounter() {
        return this.rest.getCounter();
    }
};

export const CartService = new service();
