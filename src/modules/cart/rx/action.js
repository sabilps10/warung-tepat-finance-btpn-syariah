import { NavigationActions, StackActions } from 'react-navigation';
import * as Activity from '../../../services/activity/action';
import { CartService } from './service';
import { Toast } from '../../../common/utils';
import { serverError } from '../../../assets/Utils';

export const TYPE = {
    FETCH_CART: 'cart::fetch.cart',
    FETCH_SUMMARY: 'cart::fetch.summary',
    PROCESS_FAILED: 'cart::failed.process',
    CHECK_CART_SUCCESS: 'cart::success.checkcart',
    CHECK_CART_FAILED: 'cart::failed.checkcart',
    CHECKOUT_FAILED: 'cart::failed.checkout',
    DELETE_SUCCESS: 'cart::success.delete',
    CHECKOUT_SUCCESS: 'cart::success.checkout',
    VERIFY_SUCCESS: 'cart::success.verify',
    FETCH_COUNTER: 'cart::fetch.counter',
    RESET_CART: 'cart::reset',
};

export function $load(spinner = true) {
    return (dispatch) => {
        if (spinner) {
            Activity.processing(dispatch, 'Cart', 'Load');
        }

        return CartService.get()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_CART,
                    payload: res.data,
                });

                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Cart', 'Load');

                return res;
            });
    };
}

export function $loadSummary() {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_summary', 'Show');

        return CartService.getSummary()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_SUMMARY,
                    payload: res.data,
                });

                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'cart_summary', 'Show');

                return res;
            });
    };
}

export function $show(id) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Cart', 'Load');

        return CartService.show(id)
            .then((res) => {
                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Cart', 'Load');

                return res;
            });
    };
}

export function $loadByCatalog(catalog_id) {
    return (dispatch) => {
        return CartService.getByCatalog(catalog_id)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                return res;
            });
    };
}

export function $loadPaymentMethod() {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'payment_method', 'Show');

        return CartService.getPaymentMethod()
            .then((res) => {
                return res;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'payment_method', 'Show');

                return res;
            });
    };
}

export function $delete(id) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_delete', 'delete');

        return CartService.delete(id)
            .then((res) => {
                dispatch({
                    type: TYPE.DELETE_SUCCESS,
                    payload: id,
                });

                Toast('Cart berhasil di hapus');
                return true;
            })
            .catch((error) => {
                Toast(error.errors.id);
                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'cart_delete', 'delete');

                return res;
            });
    };
}

export function $process(id, data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_add', 'Update');

        return CartService.process(id, data)
            .then((res) => {
                return true;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.PROCESS_FAILED,
                    payload: error.errors,
                });

                if (error.errors.quota) {
                    Toast(error.errors.quota);
                }

                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'cart_add', 'Update');

                return res;
            });
    };
}

export function $finish(id) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_finish', 'delete');

        return CartService.finish(id)
            .then((res) => {
                return true;
            })
            .catch((error) => {
                if (error.errors) {
                    Toast(error.errors.id);
                }

                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'cart_finish', 'delete');

                return res;
            });
    };
}

export function $checkout(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_checkout', 'Update');

        return CartService.checkout(data)
            .then((res) => {
                dispatch({
                    type: TYPE.CHECKOUT_SUCCESS,
                });

                return res.data;
            })
            .catch((error) => {
                if (data.bank_id === 2 || data.bank_id === 3) {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }

                if (error.errors.bank_id) {
                    Toast(error.errors.bank_id);
                }
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'cart_checkout', 'Update');

                return res;
            });
    };
}

export function $verifyOrder(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'order_verified', 'Update');

        return CartService.verifyOrder(data)
            .then((res) => {
                dispatch({
                    type: TYPE.VERIFY_SUCCESS,
                });

                return res;
            })
            .catch((error) => {
                if (error.message === 'minimum order') {
                    dispatch(
                        StackActions.push({
                            routeName: 'Cart',
                            params: {
                                error: error.data,
                            },
                        }),
                    );
                } else if (error.message === 'minimum order paylater') {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: {
                                    title: 'TRANSAKSI GAGAL',
                                    text: error.data,
                                    buttonText: 'KERANJANG',
                                    url: 'Cart',
                                },
                            },
                        }),
                    );
                }
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'order_verified', 'Update');

                return res;
            });
    };
}

export function $verify(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_verified', 'Update');

        return CartService.verify(data)
            .then((res) => {
                return res;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.PROCESS_FAILED,
                    payload: error.errors,
                });

                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'cart_verified', 'Update');

                return res;
            });
    };
}

export function $checkCart(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'check_cart', 'Update');

        return CartService.checkCart(data)
            .then((res) => {
                dispatch({
                    type: TYPE.CHECK_CART_SUCCESS,
                    payload: res.data,
                });
                return res.data;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.CHECK_CART_FAILED,
                    payload: error.errors,
                });

                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'check_cart', 'Update');

                return res;
            });
    };
}

export function $counter() {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'cart_counter', 'Update');

        CartService.getCounter().then((res) => {
            dispatch({
                type: TYPE.FETCH_COUNTER,
                payload: {
                    total: res.data,
                },
            });
        });
    };
}
