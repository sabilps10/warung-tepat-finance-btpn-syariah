import React, { PureComponent } from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import {
    Dialog,
    DialogButton,
    DialogContent,
    DialogFooter,
    DialogTitle,
    ScaleAnimation,
} from 'react-native-popup-dialog';

import { COLOR, FONT_SIZE, SSP } from '../../common/styles';
import { bindActionCreators } from 'redux';
import { $verify } from './rx/action';
import { connect } from 'react-redux';
import { Input } from 'react-native-elements';
import { Toast } from '../../common/utils';

class CartVerify extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            code: '',
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit() {
        let body = {
            code: this.state.code,
        };
        this.props.$verify(body).then((res) => {
            if (res) {
                Toast('Order telah diverifikasi');
                this.props.onSubmit();
            }
        });
    }

    _renderFooter() {
        return (
            <DialogFooter>
                <DialogButton
                    text="BATAL"
                    onPress={this.props.onCancel}
                    style={styles.button}
                    key="button-1"
                    textStyle={{
                        ...styles.textButton,
                        color: COLOR.danger,
                    }}
                />
                <DialogButton
                    text="VERIFIKASI"
                    disabled={this.state.code === ''}
                    onPress={this.onSubmit}
                    style={styles.button}
                    key="button-2"
                    textStyle={styles.textButton}
                />
            </DialogFooter>
        );
    }

    render() {
        return (
            <Dialog
                visible={this.props.visible}
                onTouchOutside={this.props.onCancel}
                footer={this._renderFooter()}
                dialogAnimation={new ScaleAnimation()}
                width={0.8}
                dialogTitle={
                    <DialogTitle
                        title="Verifikasi Order"
                        hasTitleBar={true}
                        style={styles.titleContainer}
                        textStyle={styles.textTitle}
                    />
                }
            >
                <DialogContent style={styles.dialogContainer}>
                    <Text style={styles.textContent}>Masukan Kode Verifikasi Order: </Text>
                    <Input
                        returnKeyType="done"
                        keyboardType="numeric"
                        inputStyle={styles.input}
                        errorStyle={styles.inputError}
                        onChangeText={(code) => this.setState({ code })}
                        autoFocus={true}
                        errorMessage={this.props.errors.code}
                    />
                </DialogContent>
            </Dialog>
        );
    }
}

CartVerify.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    visible: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        errors: state.Cart.errors,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $verify,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(CartVerify);

const styles = StyleSheet.create({
    titleContainer: {
        paddingVertical: 10,
    },
    input: {
        borderColor: COLOR.darkGray,
        borderBottomWidth: 0.8,
        textAlign: 'center',
    },
    inputError: {
        textAlign: 'center',
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: { paddingVertical: 15 },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
});
