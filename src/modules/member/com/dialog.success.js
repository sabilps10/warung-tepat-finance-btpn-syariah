import React, { PureComponent } from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import {
    Dialog,
    DialogButton,
    DialogContent,
    DialogFooter,
    DialogTitle,
    ScaleAnimation,
} from 'react-native-popup-dialog';

import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';
import { bindActionCreators } from 'redux';
import { $load } from '../rx/action';
import { connect } from 'react-redux';

class DialogSuccess extends PureComponent {
    _renderFooter() {
        return (
            <DialogFooter>
                <DialogButton
                    text="SELESAI"
                    onPress={this.finish}
                    style={styles.button}
                    key="button-1"
                    textStyle={{
                        ...styles.textButton,
                        color: COLOR.danger,
                    }}
                />
                <DialogButton
                    text="YA"
                    onPress={this.register}
                    style={styles.button}
                    key="button-2"
                    textStyle={styles.textButton}
                />
            </DialogFooter>
        );
    }

    finish = () => {
        this.props.$load(1, 15, '');
        this.props.navigation.navigate('Member');
    };

    register = () => {
        this.props.onPress();
    };

    render() {
        return (
            <Dialog visible={this.props.visible} onTouchOutside={this.finish}
                    footer={this._renderFooter()}
                    dialogAnimation={new ScaleAnimation()}
                    width={0.8}
                    dialogTitle={
                        <DialogTitle
                            title='REGISTRASI BERHASIL'
                            hasTitleBar={true}
                            style={styles.titleContainer}
                            textStyle={styles.textTitle}
                        />
                    }>
                <DialogContent style={styles.dialogContainer}>
                    <Text style={styles.textContent}>Member berhasil
                        didaftarkan! {'\n'} Apakah Anda
                        ingin mendaftarkan Member lain?</Text>
                </DialogContent>
            </Dialog>
        );
    }
}

DialogSuccess.propTypes = {
    visible: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
};


const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({
        $load,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(DialogSuccess);

const styles = StyleSheet.create({
    titleContainer: {
        paddingVertical: 10,
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: { paddingVertical: 15 },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
});
