import React, { PureComponent } from 'react';
import { StyleSheet, View, Share } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { Col, Grid } from 'react-native-easy-grid';
import { Button } from 'react-native-elements';
import PropTypes from 'prop-types';

import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';
import { Config } from '../../../common/config';

class Footer extends PureComponent {
    constructor(props) {
        super(props);
    }

    navigate = () => {
        const AddMember = NavigationActions.navigate({
            routeName: 'MemberCreate',
        });
        this.props.dispatch(AddMember);
    };

    getLink() {
        return this.props.session.poskora.id ? Config.orderUrl + '?source=share&id=' + this.props.session.poskora.id : null;
    }

    onShare = async() => {
        try {
            const result = await Share.share({
                message:'Belanja sembako dan kebutuhan rumah tangga (beras, minyak goreng, gula, sabun dll) yuk di sini.\n\nMudah dan Murah, Belanja kebutuhan rumah tangga gak ribet lagi.\n\nKlik link dibawah ini:\n' + this.getLink(),
            });
        } catch (error) {
            // console.log("share " + error)
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <Grid>
                    <Col>
                        <Button
                            title="SHARE LINK"
                            buttonStyle={styles.buttonShare}
                            titleStyle={styles.buttonText}
                            containerStyle={{ width: '100%', paddingRight: 5}}
                            onPress={this.onShare}
                            />
                    </Col>
                    <Col>
                        <Button
                            title="TAMBAH MEMBER"
                            buttonStyle={styles.buttonAdd}
                            titleStyle={styles.buttonText}
                            containerStyle={{ width: '100%', paddingLeft: 5}}
                            onPress={this.navigate}
                        />
                    </Col>
                </Grid>
            </View>
        );
    }
}

Footer.propTypes = {
    session: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        add: state.Member.add,
        session: state.Auth.session
    };
};

export default connect(mapStateToProps)(Footer);

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        height: 'auto',
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    buttonAdd: {
        backgroundColor: COLOR.success,
        width: '100%',
    },
    buttonShare: {
        backgroundColor: COLOR.primary,
        width: '100%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.SMALL,
        letterSpacing: 0.5,
    },
});
