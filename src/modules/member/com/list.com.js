import React, { Component } from 'react';
import {
    LayoutAnimation,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    UIManager,
    View,
} from 'react-native';
import { Col, Row } from 'react-native-easy-grid';
import PropTypes from 'prop-types';
import Swipeable from 'react-native-swipeable';
import Moment from 'react-moment';
import { Card, Icon } from 'react-native-elements';

import { CurrencyFormatter } from '../../../common/utils';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';
import PkaIcon from '../../../components/custom_icon';

class ListCom extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDialog: false,
        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    navigate = () => {
        this.props.navigation.navigate('MemberUpdate', { id: this.props.data.id }, true);
    };

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.props.data.name != nextProps.data.name || (this.props.deleted != nextProps.deleted && this.props.data.id == nextProps.deleted) || this.props.data.id != nextProps.data.id) {
            if (nextProps.deleted != 0 && this.props.data.id == nextProps.deleted) {
                LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                this.swipeable.recenter();
            }

            return true;
        }

        return false;
    }

    _renderButton() {
        return (
            <View style={styles.swipeContainer}>
                <TouchableOpacity activeOpacity={0.7} onPress={this.navigate} style={{
                    ...styles.buttonAction,
                    backgroundColor: COLOR.warning,
                }}>
                    <PkaIcon name="edit" color={COLOR.white} size={25}/>
                    <Text style={styles.buttonActionLabel}>Perbaharui</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} onPress={this.props.onDelete} style={{
                    ...styles.buttonAction,
                    backgroundColor: COLOR.danger,
                }}>
                    <PkaIcon name="trash" color={COLOR.white} size={25}/>
                    <Text style={styles.buttonActionLabel}>Hapus</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        return (
            <View style={{
                height: this.props.deleted != 0 && this.props.deleted == this.props.data.id ? 0 : null,
                overflow: 'hidden',
            }}>
                <Card containerStyle={styles.container}>
                    <Swipeable rightButtonWidth={160} onRef={(ref) => {
                        this.swipeable = ref;
                    }} rightButtons={[this._renderButton()]}>
                        <View style={styles.container}>
                            <Text style={styles.name}>
                                {this.props.data.name.toUpperCase()}
                            </Text>
                            <Row style={styles.rowContainer}>
                                <Col size={2}>
                                    <View style={styles.footerContainer}>
                                        <View style={styles.iconContainer}>
                                            <Icon name="old-phone" type='entypo' color={COLOR.white}
                                                  size={15}/>
                                        </View>
                                        <View>
                                            <Text style={styles.iconLabel}>
                                                No. Handphone
                                            </Text>
                                            <Text style={styles.iconValue}>
                                                {this.props.data.phone}
                                            </Text>
                                        </View>
                                    </View>
                                </Col>
                                <Col size={2}>
                                    <View style={styles.footerContainer}>
                                        <View style={styles.iconContainer}>
                                            <Icon name="shopping-basket" type='entypo'
                                                  color={COLOR.white} size={15}/>
                                        </View>
                                        <View>
                                            <Text style={styles.iconLabel}>
                                                Order Terakhir
                                            </Text>
                                            <Moment element={Text} style={styles.iconValue}
                                                    format="DD/MM/YYYY" utc>{this.props.data.last_order_at}</Moment>
                                        </View>
                                    </View>
                                </Col>
                            </Row>
                        </View>
                    </Swipeable>
                </Card>
            </View>
        );
    }
}

ListCom.propTypes = {
    data: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    deleted: PropTypes.number,
};

export default ListCom;

const styles = StyleSheet.create({
    container: {
        borderColor: COLOR.border,
        elevation: 0,
        borderWidth: 0.2,
        borderBottomWidth: 0.5,
        marginBottom: 5,
        marginTop: 5,
        paddingTop: PADDING.PADDING_CARD,
        padding: 0,
        margin: 0,
    },
    name: {
        color: COLOR.black,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 15,
    },
    phone: {
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.SMALL,
    },
    footerContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconContainer: {
        width: 25,
        height: 25,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLOR.purple,
        marginRight: 5,
    },
    iconLabel: {
        fontFamily: SSP.light,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        color: COLOR.black,
    },
    iconValue: {
        color: COLOR.black,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    },
    containerName: {
        paddingBottom: PADDING.PADDING_CARD,
    },
    label: {
        color: COLOR.black,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
        padding: PADDING.PADDING_CARD,
    },
    input: {
        width: '100%',
        borderColor: COLOR.border,
        borderWidth: 0.5,
        padding: PADDING.PADDING_CARD,
    },
    textInput: {
        color: COLOR.black,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    swipeContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: -5,
    },
    buttonAction: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
    },
    buttonActionLabel: {
        paddingTop: 3,
        color: COLOR.white,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        fontFamily: SSP.semi_bold,
    },
    rowContainer: {
        padding: PADDING.PADDING_CARD,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5F5F5',
        paddingHorizontal: 15,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.4,
        borderColor: '#dbdbdb',
    },
});
