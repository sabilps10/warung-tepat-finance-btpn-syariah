import React, { Component } from 'react';
import {
    ActivityIndicator,
    FlatList,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { $delete, $load } from './rx/action';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Footer from './com/footer';
import { Icon, SearchBar } from 'react-native-elements';
import EmptyState from '../../components/empty_state';
import ListCom from './com/list.com';
import PkaDialog from '../../components/dialog';


class MemberScreen extends Component {
    constructor(props) {
        super(props);

        this.typingTimer = false;

        this.state = {
            is_empty: false,
            is_show_top: false,
            search: '',
            showDialog: false,
            deleteData: {},
            slideHide: 0,
            reload: false,
        };

        this._onRefresh = this._onRefresh.bind(this);
        this._onLoad = this._onLoad.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.is_loading == this.props.is_loading && nextProps.is_loadmore == this.props.is_loadmore && nextProps.screen == this.props.screen && nextState.is_empty == this.state.is_empty && nextState.is_show_top == this.state.is_show_top && nextState.search == this.state.search
            && nextState.showDialog == this.state.showDialog && nextState.deleteData == this.state.deleteData && nextState.slideHide == this.state.slideHide) {
            return false;
        }

        return true;
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.navigation.getParam('reload') != this.props.navigation.getParam('reload')) {
            this._onRefresh();
        }
    }

    _onRefresh() {
        this.props.$load(1, 15, this.props.screen.search)
            .then(res => {
                if (!res && this.props.screen.data.length() == 0) {
                    this.setState({
                        is_empty: !res,
                        slideHide: 0,
                    });
                }
            });
    }

    _onLoad(search = '') {
        this.setState({ is_empty: false });
        this.props.$load(1, 15, search)
            .then(res => {
                if (!res && this.props.screen.total == 0) {
                    this.setState({ is_empty: !res });
                }
            });
    }

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.is_loading} onRefresh={this._onRefresh}
                               colors={[COLOR.primary]}/>;
    }

    componentWillMount() {
        this._onLoad();
    }

    _renderFooter() {
        return this.props.screen.total > this.props.screen.data.length ? (
            <ActivityIndicator color={COLOR.secondary} style={{
                paddingTop: 20,
                paddingBottom: 20,
            }} animating size="large"/>
        ) : <Text style={styles.totalText}>Total Member: {this.props.screen.total}</Text>;
    }

    _renderToTop() {
        return (
            this.state.showTop ?
                <TouchableOpacity onPress={this.onGoTop} style={styles.fab}>
                    <Icon name='chevron-up' type='feather' size={21}/>
                </TouchableOpacity>
                : null
        );
    }

    onLoadMore = () => {
        if (this.props.screen.total > this.props.screen.data.length) {
            this.props.$load(this.props.screen.page + 1);
        }
    };

    onScroll = (event) => {
        if (event.nativeEvent.contentOffset.y > 300) {
            this.setState({ showTop: true });
        } else {
            this.setState({ showTop: false });
        }
    };

    onGoTop = () => {
        this.listRef.scrollToOffset({
            offset: 0,
            animated: true,
        });
    };

    onSearch = search => {
        this.setState({ search });
        clearTimeout(this.typingTimer);
        this.typingTimer = setTimeout(() => {
            this._onLoad(search);
        }, 200);
    };

    onCancel = () => {
        this.setState({
            showDialog: false,
            deleteData: {},
        });
    };

    onSubmit = () => {
        this.props.$delete(this.state.deleteData.id)
            .then(res => {

                this.setState({
                    showDialog: false,
                    deleteData: {},
                    slideHide: res ? this.state.deleteData.id : 0,
                });
            })
            .finally(() => {
                this.setState({
                    slideHide: 0,
                });
            });
    };

    delete(item) {
        this.setState({
            showDialog: true,
            deleteData: item,
        });
    };

    _flatItem = ({ index, item }) => <ListCom key={index} data={item} deleted={this.state.slideHide}
                                              navigation={this.props.navigation}
                                              onDelete={() => this.delete(item)}/>;

    _flatKey = (item, index) => index.toString();

    _flatReff = (ref) => this.listRef = ref;

    render() {
        const { search } = this.state;
        return (
            <View style={styles.container}>
                {
                    this.state.is_empty && !this.props.is_loading && this.props.screen.search == '' ?
                        <EmptyState message="Anda belum mempunyai Member!"
                                    image="https://s3-ap-southeast-1.amazonaws.com/kora.id/v1/empty/member.png"/>
                        : <View style={styles.bodyContainer}>
                            <SearchBar lightTheme
                                       containerStyle={styles.searchContainer}
                                       inputContainerStyle={styles.inputContainer}
                                       inputStyle={styles.searchInput}
                                       placeholder="Cari Member"
                                       onChangeText={this.onSearch}
                                       value={search}
                            />
                            {
                                this.state.is_empty && this.props.screen.search != '' ?
                                    <EmptyState
                                        title={'Member Tidak Ditemukan'}
                                        subtitle="Tidak dapat menemukan Member dengan pencarian tersebut."
                                        image="https://s3.kora.id/app/assets/empty-search.png"/>
                                    : null
                            }
                            <FlatList
                                scrollEventThrottle={16}
                                onScroll={this.onScroll}
                                refreshControl={this._renderRefresh()}
                                ListFooterComponent={this._renderFooter()}
                                data={this.props.screen.data}
                                keyExtractor={this._flatKey}
                                renderItem={this._flatItem}
                                onEndReached={this.onLoadMore}
                                onEndReachedThreshold={0.1}
                                initialNumToRender={15}
                                showsVerticalScrollIndicator={true}
                                removeClippedSubviews={true}
                                ref={this._flatReff}
                            />

                            {this._renderToTop()}
                        </View>
                }
                <Footer session={this.props.session}/>

                <PkaDialog onCancel={this.onCancel} onSubmit={this.onSubmit}
                           title='Hapus Member'
                           text={'Anda yakin akan menghapus Member \n atas nama "' + this.state.deleteData.name + '" ?'}
                           visible={this.state.showDialog}/>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        is_loadmore: state.Activity.components.loadmore,
        is_loading: state.Activity.page,
        screen: state.Member.screen,
        session: state.Auth.session,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({
        $load,
        $delete,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MemberScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    bodyContainer: {
        flex: 1,
        paddingBottom: 70,
        backgroundColor: COLOR.light,
    },
    searchContainer: {
        backgroundColor: 'white',
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        margin: 0,
    },
    inputContainer: {
        backgroundColor: COLOR.light,
        borderColor: COLOR.border,
        borderWidth: 0.5,
        borderBottomWidth: 0.5,
    },
    searchInput: {
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.black,
        padding: 0,
    },
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 100,
        backgroundColor: '#fff',
        borderRadius: 30,
        elevation: 8,
        zIndex: 9999,
    },
    fabIcon: {
        fontSize: 30,
        color: 'black',
    },
    totalText: {
        textAlign: 'center',
        fontFamily: SSP.semi_bold,
        color: COLOR.darkGray,
        fontSize: FONT_SIZE.MEDIUM,
        margin: 10,
    },
});
