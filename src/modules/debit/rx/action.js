import { NavigationActions } from 'react-navigation';

import * as Activity from '@src/services/activity/action';
import { LmdToast } from '../../auth/com/lmdToast';
import { DebitService } from './service';
import { invalidAccount, insufficientFund, serverError, blockedAccount, notConnect } from '../../../assets/Utils';

export const TYPE = {
    BEGIN_VERIFY: 'debit::verify',
    VERIFY_SUCCESS: 'debit::verify.success',
    VERIFY_FAILED: 'debit::verify.failed',
    VERIFY_ERROR: 'debit::verify.error',
    RESET: 'reset',
};

// action verify order
export function $verifyPin(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Debit', 'Verify');
        dispatch({
            type: TYPE.BEGIN_VERIFY,
            payload: {
                isLoading: true,
            },
        });
        DebitService.verifyPin(data)
            .then((res) => {
                const { response_code } = res.data;

                if (response_code === '00') {
                    dispatch({
                        type: TYPE.VERIFY_SUCCESS,
                        payload: {
                            verified: true,
                            error: false,
                            isLoading: false,
                        },
                    });

                    if (data.bank_id) {
                        dispatch(
                            NavigationActions.navigate({
                                routeName: 'MurabahahLoading',
                                params: {
                                    data: res.data,
                                },
                            }),
                        );
                    } else {
                        dispatch(
                            NavigationActions.navigate({
                                routeName: 'DebitLoading',
                                params: {
                                    data: res.data,
                                },
                            }),
                        );
                    }
                } else if (response_code === '100') {
                    dispatch({
                        type: TYPE.VERIFY_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: notConnect,
                            },
                        }),
                    );
                }

                return res;
            })
            .catch((error) => {
                const { response_code } = error.data;

                if (response_code === '06') {
                    LmdToast.showToast('PIN yang anda masukkan salah', 'danger');

                    dispatch({
                        type: TYPE.VERIFY_ERROR,
                        payload: {
                            verified: false,
                            error: true,
                            isLoading: false,
                        },
                    });
                } else if (response_code === '01') {
                    dispatch({
                        type: TYPE.VERIFY_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: invalidAccount,
                            },
                        }),
                    );
                } else if (response_code === '51') {
                    dispatch({
                        type: TYPE.VERIFY_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: insufficientFund,
                            },
                        }),
                    );
                } else if (response_code === '111') {
                    dispatch({
                        type: TYPE.VERIFY_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: {
                                    title: 'TRANSAKSI GAGAL',
                                    text: error.message,
                                    buttonText: 'Kembali',
                                    url: 'Cart',
                                },
                            },
                        }),
                    );
                } else if (response_code === '12') {
                    dispatch({
                        type: TYPE.VERIFY_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'ConnectFailed',
                            params: {
                                data: blockedAccount,
                            },
                        }),
                    );
                } else {
                    dispatch({
                        type: TYPE.VERIFY_FAILED,
                        payload: {
                            verified: false,
                            isLoading: false,
                        },
                    });

                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Debit', 'Verify');

                return res;
            });
    };
}

// action reset error
export function $resetError() {
    return (dispatch) => {
        dispatch({ type: TYPE.RESET });
    };
}
