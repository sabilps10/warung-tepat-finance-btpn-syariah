import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { StyleSheet, ActivityIndicator } from 'react-native';
import { View, Text } from 'react-native-animatable';
import { connect } from 'react-redux';
import { NavigationActions, StackActions } from 'react-navigation';

import { COLOR } from '@common/styles';
import { orderSuccess } from '../../../assets/Utils';

import { $checkout } from '../../cart/rx/action';

class DebitLoadingPage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const { data } = this.props.navigation.state.params;
        const body = {
            bank_id: 2,
            token: data.token,
        };
        this.props.$checkout(body).then((res) => {
            if (res) {
                let navigationAction = StackActions.reset({
                    index: 2,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Main' }),
                        NavigationActions.navigate({ routeName: 'Cart' }),
                        NavigationActions.navigate({
                            routeName: 'OrderSuccess',
                            params: {
                                data: orderSuccess,
                            },
                        }),
                    ],
                });

                this.props.navigation.dispatch(navigationAction);
            }
        });
    }

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <ActivityIndicator size={85} color={COLOR.primary} />
                <View style={styles.textContainer}>
                    <Text style={styles.title}>Mohon Tunggu!</Text>
                    <Text style={styles.text}>Pesanan Anda sedang diproses..</Text>
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $checkout }, dispatch);
};

export default connect(null, mapDispatchToProps)(DebitLoadingPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
        justifyContent: 'center',
    },
    textContainer: {
        alignItems: 'center',
        marginTop: 16,
    },
    title: {
        fontFamily: 'Roboto-Bold',
        fontSize: 24,
        lineHeight: 36,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
    },
});
