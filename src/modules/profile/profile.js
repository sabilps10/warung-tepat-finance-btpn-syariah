import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, StyleSheet, View } from 'react-native';

import { COLOR, FONT_SIZE } from '../../common/styles';
import { TextField } from 'react-native-material-textfield';
import { bindActionCreators } from 'redux';
import { $reset } from '../../services/form/action';

class UpdateProfileScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            phone: '',
        };
    }

    componentWillMount() {
        const phone = this.props.session.user.phone;
        const mask = '*****';
        const sliceNo = phone.slice(0, -5);
        const phoneMask = sliceNo + mask;
        this.setState({
            name: this.props.session.user.name,
            email: this.props.session.user.email,
            phone: phoneMask,
        });
    }

    resetForm() {
        this.props.$reset();
    }

    render() {
        let { name, email, phone } = this.state;

        return (
            <View style={styles.container}>
                <ScrollView style={styles.containerScrollView} keyboardShouldPersistTaps="handled">
                    <View style={styles.section}>
                        <View style={styles.field}>
                            <TextField
                                label="Nama Lengkap"
                                value={name}
                                error={this.props.errors.name}
                                returnKeyType="next"
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                onFocus={() => this.resetForm()}
                                onChangeText={(fullName) => this.setState({ name: fullName })}
                                editable={false}
                            />
                        </View>
                        <View style={styles.field}>
                            <TextField
                                label="Alamat Email"
                                value={email}
                                error={this.props.errors.email}
                                returnKeyType="next"
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                onFocus={() => this.resetForm()}
                                onChangeText={(emails) => this.setState({ email: emails })}
                                editable={false}
                            />
                        </View>
                        <View style={{ ...styles.field, ...styles.containerHandphone }}>
                            <TextField
                                label="No. Handphone"
                                value={phone}
                                error={this.props.errors.phone}
                                lineWidth={0}
                                activeLineWidth={0}
                                tintColor={COLOR.primary}
                                labelHeight={16}
                                fontSize={14}
                                inputContainerPadding={0}
                                keyboardType="numeric"
                                onFocus={() => this.resetForm()}
                                onChangeText={(phones) => this.setState({ phone: phones })}
                                editable={false}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        session: state.Auth.session,
        isLoading: state.Activity.components.profile,
        errors: state.Profile.errors,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $reset,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfileScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.light,
        margin: 0,
        padding: 0,
        paddingBottom: 70,
    },
    section: {
        backgroundColor: COLOR.white,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: COLOR.border,
        paddingBottom: 5,
        marginBottom: 15,
    },
    field: {
        paddingHorizontal: 20,
        borderBottomWidth: 0.7,
        borderColor: COLOR.border,
        paddingTop: 5,
    },
    title: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
    containerScrollView: {
        paddingTop: 15,
    },
    containerHandphone: {
        borderBottomWidth: 0,
    },
});
