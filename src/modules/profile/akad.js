import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, ActivityIndicator } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import 'moment/locale/id';

import { $getAkadDetail } from './rx/action';
import { COLOR, PADDING, SSP, FONT_SIZE } from '../../common/styles';
import { CurrencyFormatter } from '../../common/utils';

class ViewAkadScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.onLoad();
    }

    onLoad() {
        const noakad = this.props.navigation.getParam('noakad');
        this.props.$getAkadDetail(noakad);
    }

    _paylaterType = () => {
        const { akadData } = this.props;
        if (akadData.data.paylater_type === 'REGULAR') {
            return <Text style={styles.textIndent}>Bayar Bulanan</Text>;
        } else if (akadData.data.paylater_type === 'IRREGULAR') {
            return <Text style={styles.textIndent}>Bayar di Akhir</Text>;
        } else {
            return <Text style={styles.textIndent}>-</Text>;
        }
    };

    _noRekening = () => {
        const { akadData } = this.props;
        if (akadData.data.raw_data !== null) {
            return <Text style={styles.textIndent}>{akadData.data.raw_data.data.accountWow}</Text>;
        } else {
            return <Text style={styles.textIndent}>-</Text>;
        }
    };

    render() {
        const { akadData } = this.props;

        if (akadData.length < 1) {
            return <ActivityIndicator style={styles.containerLoading} size={65} color={COLOR.primary} />;
        }

        return (
            <View style={styles.container}>
                <View style={styles.containerNumber}>
                    <Text style={styles.subTitle}>Nomor</Text>
                    <Text style={styles.title}>{akadData.data.kode_akad}</Text>
                </View>
                <ScrollView>
                    <View style={styles.textContent}>
                        <Text style={styles.textDescription}>
                            Akad Pembiayaan Murabahah ini (selanjutnya disebut “Akad Pembiayaan”) dibuat dan disetujui
                            pada hari ini, {moment(akadData.data.tgl_akad).format('dddd')} tanggal{' '}
                            {moment(akadData.data.tgl_akad).format('DD')} bulan{' '}
                            {moment(akadData.data.tgl_akad).format('MMMM')} tahun{' '}
                            {moment(akadData.data.tgl_akad).format('YYYY')} (
                            {moment(akadData.data.tgl_akad).format('DD/MM/YYYY')}), bertempat di {akadData.data.lokasi},
                            oleh dan di antara:
                        </Text>
                        <Text style={styles.textDescription}>
                            1. PT Bank BTPN Syariah Tbk, berkedudukan di Jakarta Selatan, dalam hal ini diwakili oleh{' '}
                            {akadData.data.pejabat[0].name} dan {akadData.data.pejabat[1].name}, dalam kedudukannya
                            berturut-turut selaku {akadData.data.pejabat[0].jabatan} dan{' '}
                            {akadData.data.pejabat[1].jabatan} (selanjutnya disebut “Bank”);
                        </Text>
                        <View style={styles.textDescription}>
                            <Text style={styles.textNormal}>2. Nama: {akadData.data.nama}</Text>
                            <Text style={styles.textIndent}>Nomor KTP: {akadData.data.ktp}</Text>
                            <Text style={styles.textIndent}>(selanjutnya disebut “Nasabah”)</Text>
                        </View>
                        <Text style={[styles.textDescription, styles.mb30]}>
                            Bank dan Nasabah selanjutnya secara bersama-sama disebut “Para Pihak”.
                        </Text>
                        <Text style={styles.textDescription}>
                            Sebelumnya Para Pihak menerangkan terlebih dahulu bahwa dengan tunduk pada Perjanjian
                            Penyediaan Fasilitas (<Text style={styles.italic}>Line Facility</Text>) Nomor{' '}
                            {akadData.data.limit_induk_id} tanggal{' '}
                            {moment(akadData.data.limit_induk_date).format('Do MMMM YYYY')} (selanjutnya disebut
                            “Perjanjian <Text style={styles.italic}>Line Facility</Text>”) yang telah disetujui oleh
                            Nasabah, Para Pihak setuju untuk membuat Akad Pembiayaan ini dengan syarat dan ketentuan
                            sebagai berikut:
                        </Text>
                        <Text style={styles.textDescription}>PASAL 1. PEMBERIAN FASILITAS PEMBIAYAAN</Text>
                        <View style={styles.mb30}>
                            <Text style={styles.textNormal}>1. Tujuan Pembiayaan:</Text>
                            <Text style={styles.textIndent}>Modal Usaha</Text>
                            <View>
                                <Text style={styles.textNormal}>2. Jumlah Pembiayaan:</Text>
                                <Text style={styles.textIndent}>
                                    {CurrencyFormatter(akadData.data.total_order)} dengan perincian sebagai berikut
                                </Text>
                                <View style={styles.textFlex}>
                                    <Text style={styles.textLeft}>Harga Beli</Text>
                                    <Text style={styles.textRight}>
                                        : {CurrencyFormatter(akadData.data.total_order)}
                                    </Text>
                                </View>
                                <View style={styles.textFlex}>
                                    <Text style={styles.textLeft}>Margin</Text>
                                    <Text style={styles.textRight}>
                                        : {CurrencyFormatter(akadData.data.biaya_margin)}
                                    </Text>
                                </View>
                                <View style={styles.textFlex}>
                                    <Text style={styles.textLeft}>Harga Jual</Text>
                                    <Text style={styles.textRight}>
                                        : {CurrencyFormatter(akadData.data.jml_fasilitas_pembiayaan)}
                                    </Text>
                                </View>
                            </View>
                            <Text style={styles.textNormal}>3. Tenor Pembiayaan:</Text>
                            <Text style={styles.textIndent}>{akadData.data.tenor} bulan</Text>
                            <Text style={styles.textNormal}>4. Jumlah Angsuran:</Text>
                            <Text style={styles.textIndent}>{CurrencyFormatter(akadData.data.cicilan)}</Text>
                            <Text style={styles.textNormal}>5. Jatuh Tempo Pembayaran:</Text>
                            <Text style={styles.textIndent}>
                                Setiap Tanggal {moment(akadData.data.jatuh_tempo).format('DD')}
                            </Text>
                            <Text style={styles.textNormal}>6. Tanggal Berakhir Pembiayaan:</Text>
                            <Text style={styles.textIndent}>
                                {moment(akadData.data.jatuh_tempo).format('DD MMMM YYYY')}
                            </Text>
                            <Text style={styles.textNormal}>7. Cara Pembayaran:</Text>
                            {this._paylaterType()}
                            <Text style={styles.textNormal}>8. Nomor Rekening Nasabah:</Text>
                            {this._noRekening()}
                        </View>
                        <Text style={styles.textDescription}>PASAL 2. KETENTUAN PENUTUP</Text>
                        <View style={styles.mb30}>
                            <Text style={styles.textDescription}>
                                1. Nasabah telah membaca dan memahami seluruh Akad Pembiayaan ini serta Nasabah telah
                                memperoleh informasi yang jelas, lengkap dan benar.
                            </Text>
                            <Text style={styles.textDescription}>
                                2. Akad Pembiayaan ini tunduk pada serta merupakan satu kesatuan dan bagian yang tidak
                                terpisahkan dari Perjanjian <Text style={styles.italic}>Line Facility</Text> dan
                                Ketentuan Pembiayaan.
                            </Text>
                            <Text style={styles.textDescription}>
                                3. Nasabah setuju dengan memasukan nomor PIN Nasabah, maka Nasabah dengan ini memberikan
                                persetujuan Akad Pembiayaan.
                            </Text>
                            <Text style={styles.textDescription}>
                                4. Nasabah setuju bahwa persetujuan yang diberikan berfungsi juga sebagai tanda terima
                                Akad Pembiayaan dan uang atas Fasilitas Pembiayaan yang diberikan Bank sebesar Jumlah
                                Pembiayaan sesuai yang tercantum pada Akad Pembiayaan.
                            </Text>
                        </View>
                        <View style={styles.mb30}>
                            <Text>Bank</Text>
                            <Text style={styles.title}>PT Bank BTPN Syariah Tbk</Text>
                            <Text style={styles.title}>{akadData.data.pejabat[0].name}</Text>
                        </View>
                        <View style={styles.mb30}>
                            <Text>Nasabah</Text>
                            <Text style={styles.title}>{akadData.data.nama}</Text>
                        </View>
                    </View>
                    <View style={styles.borderBottom} />
                    <View />
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        akadData: state.Profile.akadData,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $getAkadDetail,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewAkadScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerLoading: {
        flex: 1,
        justifyContent: 'center',
    },
    containerNumber: {
        padding: 16,
    },
    subTitle: {
        fontSize: 12,
        color: COLOR.off,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    textContent: {
        backgroundColor: COLOR.white,
        padding: 16,
    },
    textDescription: {
        marginBottom: 14,
        fontSize: 16,
        color: COLOR.textGray,
    },
    textIndent: {
        marginBottom: 6,
        fontSize: 16,
        color: COLOR.textGray,
        marginLeft: 19,
    },
    textFlex: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: 6,
        marginLeft: 19,
    },
    textLeft: {
        fontSize: 16,
        color: COLOR.textGray,
        width: 100,
    },
    textRight: {
        fontSize: 16,
        color: COLOR.textGray,
    },
    textNormal: {
        marginBottom: 6,
        fontSize: 16,
        color: COLOR.textGray,
        fontWeight: 'bold',
    },
    footerButton: {
        bottom: 0,
        height: 72,
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        padding: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    buttonOutlined: {
        borderColor: COLOR.secondary,
        width: '100%',
    },
    buttonText: {
        fontFamily: SSP.bold,
        letterSpacing: 1.2,
        fontSize: FONT_SIZE.MEDIUM,
    },
    colStyle: {
        width: '50%',
        paddingRight: 15,
    },
    button: {
        backgroundColor: COLOR.darkGreen,
        width: '100%',
    },
    width100: { width: '100%' },
    width85: { width: '85%' },
    mb30: {
        marginBottom: 30,
    },
    borderBottom: {
        height: 8,
        backgroundColor: COLOR.backgroundCart,
    },
    switchContainer: {
        width: 24,
        height: 24,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 16,
    },
    innerCheck: {
        height: 24,
        width: 24,
    },
    italic: { fontStyle: 'italic' },
});
