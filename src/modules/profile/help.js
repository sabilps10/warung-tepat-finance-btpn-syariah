import React, { PureComponent } from 'react';
import { Image, Linking, StyleSheet, Text, View } from 'react-native';
import { Icon, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';

import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';

const contacts = [
    {
        name: 'Messaging',
        phone: '+6287803650247',
        is_whatapp: true,
        is_call: false,
    },
    {
        name: 'Contact Center',
        phone: '+628001500300',
        is_whatapp: false,
        is_call: true,
    },
];

class HelpScreen extends PureComponent {
    _call(phone) {
        Linking.openURL(`tel:${phone}`);
    }

    _message(phone) {
        Linking.openURL(`whatsapp://send?phone=${phone}`);
    }

    _navigate(phone, is_call) {
        if (is_call === true) {
            this._call(phone);
        } else {
            this._message(phone);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image
                    source={{ uri: 'https://s3.kora.id/app/assets/help.png' }}
                    style={styles.containerCard}
                    resizeMode="contain"
                />
                <View>
                    <Text style={styles.title}>Hi, {this.props.session.user.name}</Text>
                    <Text style={styles.sub}>
                        Apabila Anda membutuhkan bantuan, pertanyaan, kritik dan saran, Silakan Hubungi Customer Service
                        kami.
                    </Text>

                    <View style={styles.body}>
                        {contacts.map((cs, i) => (
                            <ListItem
                                key={i}
                                containerStyle={styles.list}
                                titleStyle={styles.listTitle}
                                subtitleStyle={styles.listSubtitle}
                                title={cs.name}
                                subtitle={cs.phone}
                                activeOpacity={0.97}
                                leftAvatar={{ source: { uri: 'https://s3.kora.id/app/assets/cs.png' } }}
                                onPress={() => this._navigate(cs.phone, cs.is_call)}
                                rightElement={
                                    <View style={styles.listCall}>
                                        {cs.is_whatapp ? (
                                            <Icon
                                                name="logo-whatsapp"
                                                size={20}
                                                type="ionicon"
                                                style={styles.containerIconWA}
                                                solid
                                            />
                                        ) : (
                                            <Icon name="phone" size={20} solid />
                                        )}
                                    </View>
                                }
                            />
                        ))}
                    </View>

                    <Text style={styles.titleOperations}>Jam operasional Senin-Jumat ( 09.00 - 17.00 WIB )</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        session: state.Auth.session,
    };
};

export default connect(mapStateToProps)(HelpScreen);

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR.light,
        flex: 1,
        padding: 30,
    },
    body: {
        paddingBottom: 20,
        paddingTop: 20,
    },
    title: {
        textAlign: 'center',
        color: COLOR.black,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.LARGE,
        paddingTop: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingBottom: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    sub: {
        textAlign: 'center',
        color: COLOR.black,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.MEDIUM,
        paddingBottom: PADDING.PADDING_CARD,
    },
    list: {
        padding: 0,
        borderColor: COLOR.light,
        borderBottomWidth: 0.7,
        elevation: 0,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingVertical: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    listSubtitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
    },
    listTitle: {
        textAlign: 'left',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.black,
    },
    listCall: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    containerCard: {
        height: 120,
        width: '100%',
    },
    containerIconWA: {
        marginRight: 20,
    },
    titleOperations: {
        textAlign: 'center',
        color: COLOR.black,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
});
