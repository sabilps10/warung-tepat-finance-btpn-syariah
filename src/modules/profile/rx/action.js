import * as Activity from '../../../services/activity/action';
import { ProfileService } from './service';
import { Toast } from '../../../common/utils';
import { NavigationActions } from 'react-navigation';
import { serverError } from '../../../assets/Utils';
import { LmdToast } from '../../auth/com/lmdToast';

export const TYPE = {
    FETCH_LOC: 'account::fetch.location',
    SAVE_SUCCESS: 'account::save.success',
    SAVE_ERROR: 'account::save.error',
    FETCH_ME: 'account::fetch',
    AVATAR_UPDATED: 'account::avatar.updated',
    FETCH_PAYLATER_BALANCE: 'paylater::fetch.balance',
    FETCH_PAYLATER_HISTORY: 'paylater::fetch.history',
    VERIFY_SUCCESS: 'paylater::verify.success',
    VERIFY_ERROR: 'paylater::verify.error',
    VERIFY_SUCCESS_HISTORY: 'paylater::verify.success.history',
    VERIFY_ERROR_HISTORY: 'paylater::verify.error.history',
    FETCH_AKAD_CONTENT: 'paylater::fetch.content',
    VERIFY_PAYLATER_PIN_FAILED: 'paylater::verify.pin.error',
    VERIFY_PAYLATER_PIN_TIMEOUT: 'paylater::verify.pin.timeout',
    VERIFY_SALDO_SUCCESS: 'saldo::verify.success',
    VERIFY_SALDO_FAILED: 'saldo::verify.failed',
    VERIFY_SALDO_TIMEOUT: 'saldo::verify.timeout',
    RESET_VERIFY_SALDO: 'saldo::verify.reset',
    RESET: 'reset',
};

export function $loadLocation() {
    return (dispatch) => {
        Activity.processing(dispatch, 'Location', 'Load');

        return ProfileService.getLocation()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_LOC,
                    payload: res.data,
                });
                return res.data;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Location', 'Load');
                return res;
            });
    };
}

export function $updateLocation(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'Location', 'Update');

        return ProfileService.updateLocation(data)
            .then((res) => {
                return true;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.SAVE_ERROR,
                    payload: error.errors,
                });
                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'Location', 'Update');
                return res;
            });
    };
}

export function $updateAvatar(name, source) {
    return (dispatch) => {
        return ProfileService.putAvatar(name, source).then((res) => {
            dispatch({
                type: TYPE.AVATAR_UPDATED,
                payload: res.data,
            });
            return res.data;
        });
    };
}

export function $updatePassword(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'password', 'update');

        return ProfileService.updatePassword(data)
            .then((res) => {
                Toast('Kata Sandi berhasil di perbaharui');
                return true;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.SAVE_ERROR,
                    payload: error.errors,
                });
                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'password', 'update');
                return res;
            });
    };
}

export function $updateProfile(data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'profile', 'update');

        return ProfileService.putProfile(data)
            .then((res) => {
                Toast('Profile berhasil di perbaharui');
                dispatch({
                    type: 'account::profile.updated',
                    payload: data,
                });
                return true;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.SAVE_ERROR,
                    payload: error.errors,
                });
                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'profile', 'update');
                return res;
            });
    };
}

export function $verifyPaylater() {
    return (dispatch) => {
        return ProfileService.getPaylaterBalance()
            .then((res) => {
                dispatch({
                    type: TYPE.VERIFY_SUCCESS,
                    payload: res.data,
                });

                return res.data;
            })
            .catch((error) => {
                const { response_code } = error.data;

                if (response_code === '113' || response_code === '114') {
                    dispatch({
                        type: TYPE.VERIFY_PAYLATER_PIN_TIMEOUT,
                    });
                } else {
                    dispatch({
                        type: TYPE.VERIFY_ERROR,
                    });
                }

                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Pembiayaan', 'Load');

                return res;
            });
    };
}

export function $loadPaylaterBalance() {
    return (dispatch) => {
        return ProfileService.getPaylaterBalance()
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_PAYLATER_BALANCE,
                    payload: res.data,
                });

                return res.data;
            })
            .catch((error) => {
                dispatch(
                    NavigationActions.navigate({
                        routeName: 'OrderFailed',
                        params: {
                            data: serverError,
                        },
                    }),
                );

                return false;
            })
            .finally((res) => {
                return res;
            });
    };
}

export function $loadPaylaterHistory() {
    return (dispatch) => {
        return ProfileService.getPaylaterHistory()
            .then((res) => {
                dispatch({
                    type: TYPE.VERIFY_SUCCESS_HISTORY,
                    payload: res.data,
                });

                return res.data;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.VERIFY_ERROR_HISTORY,
                });

                return error.data;
            })
            .finally((res, error) => {
                Activity.done(dispatch, 'Pembiayaan', 'History');

                return res;
            });
    };
}

//verify session pin paylater
export function $getVerifyPinPaylater() {
    return (dispatch) => {
        return ProfileService.getPaylaterBalance()
            .then((res) => {
                const { response_code } = res.data;

                if (response_code === '100') {
                    dispatch({
                        type: TYPE.FETCH_PAYLATER_BALANCE,
                        payload: res.data,
                    });
                }

                return res.data;
            })
            .catch((error) => {
                const { response_code } = error.data;

                if (response_code === '113' || response_code === '114') {
                    dispatch(
                        NavigationActions.navigate({
                            type: TYPE.VERIFY_SALDO_TIMEOUT,
                            routeName: 'VerifyPinPaylaterCard',
                        }),
                    );
                } else {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }

                return false;
            })
            .finally((res) => {
                return res;
            });
    };
}

//verify pin esb paylater
export function $verifyPinPaylater(data) {
    return (dispatch) => {
        return ProfileService.verifyPinSaldo(data)
            .then((res) => {
                const { response_code } = res.data;

                if (response_code === '00') {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'ProfileScreen',
                        }),
                    );
                } else {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }

                return res;
            })
            .catch((error) => {
                const { response_code } = error.data;

                if (response_code === '06') {
                    LmdToast.showToast('PIN yang anda masukkan salah', 'danger');

                    dispatch({
                        type: TYPE.VERIFY_PAYLATER_PIN_FAILED,
                        payload: {
                            verified: false,
                            errorPinPaylater: true,
                            isLoadingPinPaylater: false,
                            isConnected: true,
                        },
                    });
                } else {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'ProfilePembiayaan', 'Load');

                return res;
            });
    };
}

export function $getAkadDetail(id) {
    return (dispatch) => {
        return ProfileService.getAkadDetail(id)
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_AKAD_CONTENT,
                    payload: res.data,
                });
                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                return res;
            });
    };
}

export function $getVerifySaldoPin() {
    return (dispatch) => {
        return ProfileService.getverifySaldoPin()
            .then((res) => {
                const { response_code } = res.data;

                if (response_code === '00') {
                    dispatch({
                        type: TYPE.VERIFY_SALDO_SUCCESS,
                        payload: {
                            isSaldo: true,
                        },
                        balance: res.data.current_balance,
                    });
                } else if (response_code === '113' || response_code === '114') {
                    dispatch(
                        NavigationActions.navigate({
                            type: TYPE.VERIFY_SALDO_TIMEOUT,
                            routeName: 'VerifyPinSaldoCard',
                            payload: {
                                isSaldo: false,
                            },
                        }),
                    );
                } else {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }

                return res;
            })
            .catch((error) => {
                dispatch({ type: TYPE.VERIFY_SALDO_FAILED });
            })
            .finally((res) => {
                Activity.done(dispatch, 'Saldo', 'Load');

                return res;
            });
    };
}

//verify pin esb
export function $verifyPinSaldo(data) {
    return (dispatch) => {
        return ProfileService.verifyPinSaldo(data)
            .then((res) => {
                const { response_code } = res.data;

                if (response_code === '00') {
                    dispatch(
                        NavigationActions.navigate({
                            type: TYPE.VERIFY_SALDO_SUCCESS,
                            routeName: 'Main',
                            payload: {
                                isConnectSaldo: true,
                            },
                        }),
                    );
                } else {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }

                return res;
            })
            .catch((error) => {
                const { response_code } = error.data;

                if (response_code === '06') {
                    LmdToast.showToast('PIN yang anda masukkan salah', 'danger');

                    dispatch({
                        type: TYPE.VERIFY_SALDO_FAILED,
                        payload: {
                            verified: false,
                            isConnectSaldo: false,
                            error: true,
                            isLoading: false,
                        },
                    });
                } else {
                    dispatch(
                        NavigationActions.navigate({
                            routeName: 'OrderFailed',
                            params: {
                                data: serverError,
                            },
                        }),
                    );
                }
            })
            .finally((res) => {
                Activity.done(dispatch, 'Saldo', 'Load');

                return res;
            });
    };
}

export function $resetSaldo() {
    return (dispatch) => {
        dispatch({ type: TYPE.RESET_VERIFY_SALDO, isConnectSaldo: false });
    };
}

export function $resetError() {
    return (dispatch) => {
        dispatch({ type: TYPE.RESET });
    };
}
