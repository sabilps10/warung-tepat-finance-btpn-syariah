import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { StyleSheet, ActivityIndicator } from 'react-native';
import { View, Text } from 'react-native-animatable';
import { connect } from 'react-redux';
import { COLOR } from '@common/styles';

import { $getVerifyPinPaylater } from '../rx/action';

class PaylaterLoadingPage extends Component {
    constructor(props) {
        super(props);
    }

    toPaylater = () => {
        this.props.navigation.navigate('ProfilePembiayaan');
    };

    componentWillMount() {
        this.props.$getVerifyPinPaylater().then((res) => {
            if (res) {
                this.setState({ showLoadingDialog: false });
                this.toPaylater();
            }
        });
    }

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <ActivityIndicator size={85} color={COLOR.primary} />
                <View style={styles.textContainer}>
                    <Text style={styles.title}>Mohon Tunggu!</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        session: state.Auth.session,
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $getVerifyPinPaylater }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PaylaterLoadingPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
        justifyContent: 'center',
    },
    textContainer: {
        alignItems: 'center',
        marginTop: 16,
    },
    title: {
        fontFamily: 'Roboto-Bold',
        fontSize: 24,
        lineHeight: 36,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
    },
});
