import React, { PureComponent } from 'react';
import { Header } from 'react-native-elements';
import { StyleSheet, Text } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';
import { HeaderBackButton } from 'react-navigation-stack';

export default class PaylaterHeader extends PureComponent {
    toProfile = () => {
        this.props.navigation.navigate('ProfileScreen');
    };

    render() {
        return (
            <Header
                containerStyle={styles.container}
                placement="center"
                leftComponent={<HeaderBackButton tintColor={COLOR.white} onPress={this.toProfile} />}
                centerComponent={<Text style={styles.title}>PEMBIAYAAN</Text>}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR.primary,
        elevation: 4,
        justifyContent: 'space-around',
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        paddingLeft: 0,
    },
    title: {
        color: COLOR.white,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.LARGE,
        letterSpacing: 1,
    },
});
