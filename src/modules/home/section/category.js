import React, { PureComponent } from 'react';
import { Image, FlatList, StyleSheet, Text, TouchableNativeFeedback, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Countly from 'countly-sdk-react-native-bridge';

import Section from './section';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 36;

class SectionCategory extends PureComponent {
    navigate = () => {
        this.props.navigation.navigate('Category');
    };

    navigateToCatalog(category) {
        const event = { eventName: 'Categories', eventCount: 1 };

        event.segments = {
            Name: category.name,
        };

        Countly.sendEvent(event);
        setTimeout(() => {
            this.props.navigation.navigate('Catalog', { category_id: category.id }, true);
        }, 0);
    }

    componentWillUnmount() {
        clearTimeout();
    }

    _flatKey = (item, index) => index;

    _renderItem = (item) => {
        const category = item.item;

        return (
            <TouchableNativeFeedback
                background={TouchableNativeFeedback.Ripple(COLOR.light)}
                key={item.index}
                onPress={() => this.navigateToCatalog(category)}
            >
                <View style={styles.container}>
                    <View style={styles.iconContainer}>
                        <Image source={{ uri: category.icon }} style={styles.iconImage} resizeMode="contain" />
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.titleText}>{category.name}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        );
    };

    render() {
        return this.props.categories.length > 0 ? (
            <Section title="Kategori" moreText="Lihat Semua" moreTextPress={this.navigate} cat>
                <FlatList
                    data={this.props.categories.slice(0, 8)}
                    keyExtractor={this._flatKey}
                    renderItem={this._renderItem}
                    horizontal={false}
                    numColumns={4}
                />
            </Section>
        ) : null;
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categories: state.Catalog.categories,
    };
};

export default connect(mapStateToProps)(SectionCategory);

const styles = StyleSheet.create({
    container: {
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: (DEVICE_WIDTH - MARGIN) / 4,
        height: 105,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingBottom: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    iconContainer: {
        borderRadius: 50,
        backgroundColor: 'transparent',
        borderColor: COLOR.primary,
        borderWidth: 0,
    },
    iconImage: {
        height: 50,
        width: 50,
    },
    titleContainer: {
        padding: PADDING.PADDING_TITLE,
        backgroundColor: 'transparent',
    },
    titleText: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
        textAlign: 'center',
        color: COLOR.black,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        fontFamily: SSP.light,
    },
});
