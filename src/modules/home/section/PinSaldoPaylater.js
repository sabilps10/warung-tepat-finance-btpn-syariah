import React, { Component } from 'react';
import { StyleSheet, Text, Image, BackHandler } from 'react-native';
import { View } from 'react-native-animatable';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { MARKOT, NUNITO, COLOR, FONT_SIZE } from '../../../common/styles';
import iconPin from '@assets/icons/pin_input.png';

import ButtonSubmit from '../../debit/com/button';
import PinInputs from '../../../components/PinInputs';
import { $resetError, $verifyPinPaylater } from '../../profile/rx/action';

class PinPaylater extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pin: '',
        };
        this.handleBackPress = this.handleBackPress.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress() {
        this.props.navigation.navigate('ProfileScreen');
        return true;
    }

    resetError = () => {
        this.props.$resetError();
    };

    getPin = (pin) => {
        this.setState({ pin });
    };

    toPaylater = () => {
        this.props.navigation.navigate('ProfilePembiayaan');
    };

    submit = () => {
        let data = {
            mobile_no: this.props.username,
            pin: this.state.pin,
        };

        this.props.$verifyPinPaylater(data);
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <View style={styles.header}>
                    <Image source={iconPin} style={styles.iconImagePin} resizeMode="contain" />
                    <Text style={styles.title}>Input PIN</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>Masukkan PIN rekening anda </Text>
                    <Text style={styles.textBottom}>untuk melihat Pembiayaan</Text>
                    <PinInputs getOtp={this.getPin} isError={this.props.error} resetError={this.resetError} />
                    <View style={styles.footerContainer}>
                        <ButtonSubmit
                            title="Verifikasi"
                            loading={this.props.isLoading}
                            onPress={this.submit}
                            disabled={this.state.pin.length < 6 ? true : false}
                            flexGrow
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        username: state.Auth.session.user.username,
        error: state.Profile.error,
        isLoading: state.Profile.isLoading,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $resetError, $verifyPinPaylater }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PinPaylater);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    header: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 24,
        marginTop: 16,
        marginBottom: 32,
    },
    title: {
        fontFamily: MARKOT.bold,
        fontSize: FONT_SIZE.XLARGE,
        lineHeight: 36,
        marginBottom: '-5%',
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: NUNITO.regular,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_XLARGE,
        color: COLOR.textGray,
    },
    textBottom: {
        fontFamily: NUNITO.regular,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_XLARGE,
        color: COLOR.textGray,
        marginBottom: 16,
    },
    iconImagePin: {
        width: 70,
        height: 70,
        marginTop: '10%',
        marginBottom: '5%',
    },
    footerContainer: {
        position: 'absolute',
        bottom: -30,
        width: '100%',
        backgroundColor: COLOR.white,
        borderColor: COLOR.border,
        padding: 15,
    },
});
