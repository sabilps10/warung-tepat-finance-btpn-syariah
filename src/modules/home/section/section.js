import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Card } from 'react-native-elements';
import { Col, Grid, Row } from 'react-native-easy-grid';

import { COLOR, FONT_SIZE, PADDING, MARKOT, NUNITO } from '../../../common/styles';

export default class SectionComponent extends PureComponent {
    render() {
        return (
            <Card containerStyle={styles.container}>
                <Grid>
                    <Row>
                        <Col style={styles.headerLeft}>
                            {this.props.title ? <Text style={styles.title}> {this.props.title} </Text> : null}
                        </Col>
                        <Col style={styles.headerRight}>
                            {this.props.moreText !== '' ? (
                                <TouchableOpacity onPress={this.props.moreTextPress}>
                                    <Text style={styles.buttonText}>{this.props.moreText}</Text>
                                </TouchableOpacity>
                            ) : null}
                        </Col>
                    </Row>
                </Grid>
                <View style={this.props.cat ? styles.bodyCategory : styles.body}>{this.props.children}</View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderColor: COLOR.border,
        elevation: 0,
        borderWidth: 0.2,
        borderBottomWidth: 0.5,
        margin: 0,
        marginBottom: 5,
        marginTop: 5,
        padding: 0,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    body: {
        paddingVertical: PADDING.PADDING_CARD,
        paddingLeft: 0,
        paddingRight: 0,
    },
    bodyCategory: {
        paddingVertical: PADDING.PADDING_CARD,
        paddingLeft: 18,
        paddingRight: 18,
    },
    headerLeft: {
        paddingLeft: 24,
    },
    headerRight: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: 24,
    },
    title: {
        padding: 0,
        margin: 0,
        marginLeft: 0,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: MARKOT.bold,
        textAlign: 'left',
    },
    buttonText: {
        color: COLOR.darkGreen,
        fontFamily: NUNITO.extraBold,
        fontSize: FONT_SIZE.SMALL,
        textAlign: 'right',
    },
});
