import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import { CurrencySaldoFormatter } from '../../../common/utils';
import { COLOR, FONT_SIZE, NUNITO } from '../../../common/styles';
import iconSaldo from '@assets/icons/icon_system_action_cash.png';
const DEVICE_WIDTH = Dimensions.get('window').width;
import { Icon } from 'react-native-elements';
import { $getVerifySaldoPin, $resetSaldo } from '../../profile/rx/action';

class SectionDebit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hint: false,
            current_balance: '',
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.hint === nextState.hint || this.state.hint !== nextState.hint) {
            return true;
        }

        return false;
    }

    seeSaldo = () => {
        this.props.$getVerifySaldoPin().then((res) => {
            if (res.data.response_code === '00') {
                this.setState({ hint: true, current_balance: res.data.current_balance });
            }
        });
    };

    hideSaldo = () => {
        this.props.$getVerifySaldoPin().then((res) => {
            if (res.data.response_code === '00') {
                this.setState({ hint: false, current_balance: res.data.current_balance });
                this.props.$resetSaldo();
            }
        });
    };

    renderNumberSaldo() {
        const hint = this.state.hint;
        const isConnectSaldo = this.props.isConnectSaldo;
        const saldo = this.state.current_balance.slice(0, -2);
        if (hint) {
            return (
                <View style={styles.hideRow}>
                    <Text style={styles.saldoText}>{CurrencySaldoFormatter(Number(saldo))}</Text>
                </View>
            );
        } else if (!hint) {
            if (isConnectSaldo) {
                return (
                    <View style={styles.hideRow}>
                        <Text style={styles.saldoText}>{CurrencySaldoFormatter(Number(saldo))}</Text>
                    </View>
                );
            } else {
                return (
                    <View style={styles.hideRow}>
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                    </View>
                );
            }
        }
    }

    renderNumberFirstSaldo() {
        const hint = this.state.hint;
        const isConnectSaldo = this.props.isConnectSaldo;
        const saldo = this.state.current_balance.slice(0, -2);
        if (hint) {
            return (
                <View style={styles.hideRow}>
                    <Text style={styles.saldoText}>{CurrencySaldoFormatter(Number(saldo))}</Text>
                </View>
            );
        } else if (!hint) {
            if (isConnectSaldo) {
                return (
                    <View style={styles.hideRow}>
                        <Text style={styles.saldoText}>
                            {CurrencySaldoFormatter(Number(this.props.balance.slice(0, -2)))}
                        </Text>
                    </View>
                );
            } else {
                return (
                    <View style={styles.hideRow}>
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                        <Icon name="ellipse-sharp" size={10} type="ionicon" color={COLOR.white} />
                    </View>
                );
            }
        }
    }

    renderSaldoTitle() {
        const hint = this.state.hint;
        if (hint) {
            return (
                <View style={styles.saldoContainer}>
                    <Icon name="eye-off" size={15} type="ionicon" color={COLOR.white} style={styles.eyeIcon} solid />
                    <TouchableOpacity onPress={() => this.hideSaldo()} activeOpacity={0.7}>
                        <Text style={[styles.textIconSaldo, styles.colorWhite]}>Tutup Saldo</Text>
                    </TouchableOpacity>
                </View>
            );
        } else if (!hint) {
            return (
                <View style={styles.saldoContainer}>
                    <Icon name="eye" size={15} type="ionicon" color={COLOR.white} style={styles.eyeIcon} solid />
                    <TouchableOpacity onPress={() => this.seeSaldo()} activeOpacity={0.7}>
                        <Text style={[styles.textIconSaldo, styles.colorWhite]}>Lihat Saldo</Text>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    renderSaldoFirstTitle() {
        const hint = this.state.hint;
        const isConnectSaldo = this.props.isConnectSaldo;
        if (hint) {
            return (
                <View style={styles.saldoContainer}>
                    <Icon name="eye-off" size={15} type="ionicon" color={COLOR.white} style={styles.eyeIcon} solid />
                    <TouchableOpacity onPress={() => this.hideSaldo()} activeOpacity={0.7}>
                        <Text style={[styles.textIconSaldo, styles.colorWhite]}>Tutup Saldo</Text>
                    </TouchableOpacity>
                </View>
            );
        } else if (!hint) {
            if (isConnectSaldo) {
                return (
                    <View style={styles.saldoContainer}>
                        <Icon
                            name="eye-off"
                            size={15}
                            type="ionicon"
                            color={COLOR.white}
                            style={styles.eyeIcon}
                            solid
                        />
                        <TouchableOpacity onPress={() => this.hideSaldo()} activeOpacity={0.7}>
                            <Text style={[styles.textIconSaldo, styles.colorWhite]}>Tutup Saldo</Text>
                        </TouchableOpacity>
                    </View>
                );
            } else {
                return (
                    <View style={styles.saldoContainer}>
                        <Icon name="eye" size={15} type="ionicon" color={COLOR.white} style={styles.eyeIcon} solid />
                        <TouchableOpacity onPress={() => this.seeSaldo()} activeOpacity={0.7}>
                            <Text style={[styles.textIconSaldo, styles.colorWhite]}>Lihat Saldo</Text>
                        </TouchableOpacity>
                    </View>
                );
            }
        }
    }

    render() {
        const isConnectSaldo = this.props.isConnectSaldo;
        return (
            <View style={styles.container}>
                <View style={styles.saldoColumn}>
                    <View style={styles.saldoMainRow}>
                        <View style={styles.iconSaldoContainer}>
                            <Image source={iconSaldo} style={styles.iconImageSaldo} resizeMode="contain" />
                            <Text style={[styles.textIconSaldo, styles.colorWhite]}>Saldo</Text>
                        </View>
                        {isConnectSaldo ? (
                            <View>{this.renderSaldoFirstTitle()}</View>
                        ) : (
                            <View>{this.renderSaldoTitle()}</View>
                        )}
                    </View>
                    <View style={styles.saldoNumberColumn}>
                        <Text style={[styles.textCurrency, styles.colorWhite]}>Rp</Text>
                        {isConnectSaldo ? (
                            <View>{this.renderNumberFirstSaldo()}</View>
                        ) : (
                            <View>{this.renderNumberSaldo()}</View>
                        )}
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        session: state.Auth.session,
        isConnectSaldo: state.Profile.isConnectSaldo,
        balance: state.Profile.balance,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $getVerifySaldoPin,
            $resetSaldo,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(SectionDebit);

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: COLOR.white,
        elevation: 1,
        borderRadius: 10,
        overflow: 'hidden',
    },
    saldoMainRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: DEVICE_WIDTH - 48,
        marginTop: '1.5%',
    },
    iconSaldoContainer: {
        marginLeft: 16,
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconImageSaldo: {
        width: 24,
        height: 24,
    },
    textIconSaldo: {
        fontFamily: NUNITO.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        marginLeft: 8,
    },
    titleContainerCurrency: {
        marginRight: 16,
    },
    saldoText: {
        fontFamily: NUNITO.extraBold,
        color: COLOR.white,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
    },
    colorWhite: {
        color: COLOR.white,
    },
    colorDarkGreen: {
        color: COLOR.darkGreen,
    },
    hideRow: {
        flexDirection: 'row',
        marginLeft: '2%',
    },
    rowSaldo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    saldoTitle: {
        fontFamily: NUNITO.extraBold,
        color: COLOR.white,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
    },
    saldoColumn: {
        flexDirection: 'column',
        backgroundColor: COLOR.darkGreen,
        height: 59,
        width: DEVICE_WIDTH - 48,
    },
    currencyTitle: {
        marginTop: '1%',
        marginLeft: '10%',
        fontFamily: NUNITO.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    saldoContainer: {
        marginRight: '5%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    saldoNumberColumn: {
        flexDirection: 'row',
        alignItems: 'center',
        width: DEVICE_WIDTH - 48,
        marginLeft: 50,
    },
    iconBulletContainer: {
        marginLeft: 50,
    },
    textCurrency: {
        fontFamily: NUNITO.bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
});
