import { TYPE } from './action';

const INITIAL_STATE = {
    token: null,
    messages: [],
    countMessage: 0,
    read: false,
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.SAVE_TOKEN:
            return {
                ...state,
                token: action.payload,
            };
        case TYPE.GET_MESSAGE:
            return {
                ...state,
                messages: action.payload,
            };
        case TYPE.COUNT_MESSAGE:
            return {
                ...state,
                countMessage: action.payload,
            };
        case TYPE.READ_POST:
            return {
                ...state,
                messages: action.payload,
                read: true,
            };
        case TYPE.READ_ALL_POST:
            return {
                ...state,
                read: true,
            };
        default:
            return state;
    }
}

export function persister({ read, messages }) {
    return {
        read,
        messages,
    };
}
