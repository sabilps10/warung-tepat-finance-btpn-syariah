import { AsyncStorage } from 'react-native';
import NotifRest from '@src/services/rest/rest.notification';

const rest = new NotifRest();
export const NotificatonServiceImplementation = class NotificationService {
    constructor() {
        this._deviceToken = null;
        this._hasRegisteredDevice = null;
    }

    async initialize() {
        await this._loadStorage();
    }

    async _loadStorage() {
        this._deviceToken = (await AsyncStorage.getItem('notif.deviceToken')) || null;
        this._hasRegisteredDevice = (await AsyncStorage.getItem('notif.register')) || null;
        this._alreadyLogoutDevice = (await AsyncStorage.getItem('notif.alreadyLogout')) || null;
    }

    getDeviceToken() {
        return this._deviceToken;
    }

    async registerDevice() {
        try {
            // Register device id
            AsyncStorage.setItem('notif.register', 'REGISTERED_DEVICE_ID');
        } catch (error) {}
    }

    async logoutDevice() {
        this._alreadyLogoutDevice = true;
        await AsyncStorage.setItem('notif.alreadyLogout', true);
    }

    async isRegisteredDevice() {
        return !!this._hasRegisteredDevice;
    }

    isTokenAvailable() {
        return !!this._deviceToken;
    }

    registerDevicePost(body) {
        return rest.postRegister(body);
    }

    getMessages() {
        return rest.getAllMessage();
    }

    getCountMessage() {
        return rest.getCountMessage();
    }

    readPost(id) {
        return rest.postRead(id);
    }

    readAllPost() {
        return rest.postAllRead();
    }
};

export const NotificationService = new NotificatonServiceImplementation();
