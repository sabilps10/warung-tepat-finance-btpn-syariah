import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/check.png';

import Header from '../com/header';
import FormNewPassword from '../com/formNewPassword';

export default class NewPasswordScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Kode Yang Anda" title2="Masukkan Benar !" />
                <FormNewPassword navigation={this.props.navigation} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
});
