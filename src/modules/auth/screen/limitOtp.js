import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/check.png';

import Header from '../com/header';
import ButtonBack from '../com/button';

export default class LimitOtpScreen extends Component {
    constructor(props) {
        super(props);
    }

    goBack = () => {
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Nomor Handphone" title2="Anda Terdaftar" />
                <View style={styles.textContainer}>
                    <Text style={styles.text}>
                        Anda telah 3 kali salah memasukkan kode. Silahkan coba lagi setelah 5 Menit.
                    </Text>
                </View>
                <ButtonBack title="Kembali" onPress={this.goBack} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 0,
        paddingTop: 0,
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.lightRed,
    },
});
