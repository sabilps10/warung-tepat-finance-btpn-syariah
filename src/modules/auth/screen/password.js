import React, { Component } from 'react';
import { StyleSheet, Keyboard } from 'react-native';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import source from '@assets/img/warung_tepat.png';

import Header from '../com/header';
import FormPassword from '../com/formPassword';
import Logo from '../com/logo';

export default class PasswordScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            keyboard: false,
        };
    }

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }

    componentWillUnmount() {
        Keyboard.removeListener('keyboardDidShow', this.keyboardDidShow);
        Keyboard.removeListener('keyboardDidHide', this.keyboardDidHide);
    }

    keyboardDidShow = () => {
        this.setState({ keyboard: true });
    };

    keyboardDidHide = () => {
        this.setState({ keyboard: false });
    };

    render() {
        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Selamat Datang" subtitle="Di Warung Tepat" isLeft />
                <FormPassword navigation={this.props.navigation} />
                {this.state.keyboard ? null : <Logo />}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
});
