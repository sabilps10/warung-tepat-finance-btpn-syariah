import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native-animatable';
import { connect } from 'react-redux';

import { COLOR } from '@common/styles';
import source from '@assets/img/check.png';

import Header from '../com/header';
import ButtonLogin from '../com/button';

class SuccessScreen extends Component {
    constructor(props) {
        super(props);
    }

    login = () => {
        this.props.navigation.navigate('Login');
    };

    render() {
        if (this.props.newUser) {
            return (
                <View animation="fadeIn" style={styles.container} useNativeDriver>
                    <Header source={source} title="Selamat Akun Anda" title2="Berhasil Dibuat." />
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>
                            Anda bisa berbelanja di aplikasi Warung Tepat menggunakan akun Anda sendiri.
                        </Text>
                    </View>
                    <ButtonLogin title="Masuk" onPress={this.login} />
                </View>
            );
        }

        return (
            <View animation="fadeIn" style={styles.container} useNativeDriver>
                <Header source={source} title="Password Anda" title2="Berhasil Diubah." />
                <View style={styles.textContainer}>
                    <Text style={styles.text}>&nbsp;</Text>
                </View>
                <ButtonLogin title="Masuk" onPress={this.login} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    textContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 0,
        paddingTop: 0,
        marginLeft: 24,
        marginRight: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        lineHeight: 22,
        color: COLOR.textGray,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        newUser: state.Auth.newUser,
    };
};

export default connect(mapStateToProps, null)(SuccessScreen);
