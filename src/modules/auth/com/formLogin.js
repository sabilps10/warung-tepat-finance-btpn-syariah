import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text } from 'react-native-animatable';
import Countly from 'countly-sdk-react-native-bridge';
import JailMonkey from 'jail-monkey';

import { COLOR } from '@common/styles';
import Input from './input';
import ButtonSubmit from './button';
import { $check } from '../rx/action';

import { NotificationService } from '../../notification/rx/service';

class FormLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            error: null,
        };
    }

    onChangeText = (username) => {
        this.setState({ username, error: null });
    };

    onSubmit = () => {
        const { username } = this.state;
        let body = {
            username,
            android_id: NotificationService.getDeviceToken(),
        };

        const eventRoot = { eventName: 'Root Detection', eventCount: 1 };
        eventRoot.segments = {
            Phone: username,
        };

        if (JailMonkey.isJailBroken()) {
            Countly.sendEvent(eventRoot);
        }

        if (!this.state.username) {
            this.setState({ error: 'Nomor handphone tidak boleh kosong' });
        } else {
            this.props.$check(body);
        }
    };

    onForgot = () => {
        this.props.navigation.navigate({ routeName: 'ForgotPassword' });
    };

    render() {
        return (
            <View animation="bounceIn" delay={30} style={styles.container} useNativeDriver>
                <Text style={styles.text}>Masukkan nomor handphone anda</Text>
                <Input
                    placeholder="081234567890"
                    autoCapitalize={'none'}
                    returnKeyType={'next'}
                    autoCorrect={false}
                    onChangeText={this.onChangeText}
                    errorText={this.state.error}
                    keyboardType="numeric"
                />
                <ButtonSubmit title="Masuk" loading={this.props.isLoading} onPress={this.onSubmit} />
                <View style={styles.urlContainer}>
                    <Text style={styles.url} onPress={this.onForgot}>
                        Lupa Password
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        justifyContent: 'flex-start',
        marginTop: 16,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        marginLeft: 24,
        marginBottom: 8,
    },
    urlContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: 'NunitoSans-ExtraBold',
        fontSize: 14,
    },
    Messagecontainer: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: COLOR.white,
        height: 440,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        errors: state.Auth.errors,
        isLoading: state.Activity.loading,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $check }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(FormLogin);
