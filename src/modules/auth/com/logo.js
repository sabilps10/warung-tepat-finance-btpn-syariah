import React, { Component } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Text } from 'react-native';

import logo from '@assets/img/btpns_logo.png';
import { Toast } from '../../../common/utils';

export default class Logo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            click: 0,
        };
    }

    devMode = () => {
        let { click } = this.state;
        this.setState({ click: this.state.click + 1 });

        if (click === 15) {
            global.API_URL = global.API_CONFIG.dev;
            Toast('RUNNING ON DEV MODE');
        } else if (click === 20) {
            Toast('RUNNING ON PRODUCTION MODE');
            global.API_URL = global.API_CONFIG.prod;
            this.setState({ click: 0 });
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity activeOpacity={1} onPress={this.devMode}>
                    <Image style={styles.image} source={logo} resizeMode="contain" />
                    <Text style={styles.text}>{this.props.text}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        width: 98,
        height: 50,
    },
    text: {
        textAlign: 'center',
        fontFamily: 'NunitoSans-Regular',
        marginTop: 16,
    },
});
