import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, Image, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import { View } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import iconE from '@assets/icons/eye_black.png';

export default class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            secure: true,
        };

        this._showPass = this._showPass.bind(this);
    }

    _showPass() {
        this.state.secure === true ? this.setState({ secure: false }) : this.setState({ secure: true });
    }

    _isSecureText() {
        return this.props.secureTextEntry && this.state.secure;
    }

    render() {
        return (
            <View animation={this.props.errorText ? 'wobble' : ''} style={styles.inputWrapper} useNativeDriver>
                <Image source={this.props.source} style={styles.inlineImg} />
                <TextInput
                    style={styles.input}
                    placeholder={this.props.placeholder}
                    secureTextEntry={this._isSecureText()}
                    autoCorrect={this.props.autoCorrect}
                    autoCapitalize={this.props.autoCapitalize}
                    returnKeyType={this.props.returnKeyType}
                    placeholderTextColor={COLOR.border}
                    underlineColorAndroid="transparent"
                    onChangeText={this.props.onChangeText}
                    keyboardType={this.props.keyboardType}
                />
                {this.props.showEye ? (
                    <TouchableOpacity activeOpacity={0.5} style={styles.btnEye} onPress={this._showPass}>
                        <Image source={iconE} style={styles.iconEye} />
                    </TouchableOpacity>
                ) : null}
                <Text style={styles.hint}>{this.props.hint}</Text>
                <Text style={styles.errorMessage}>{this.props.errorText}</Text>
            </View>
        );
    }
}

Input.propTypes = {
    source: PropTypes.number.isRequired,
    placeholder: PropTypes.string.isRequired,
    secureTextEntry: PropTypes.bool,
    autoCorrect: PropTypes.bool,
    autoCapitalize: PropTypes.string,
    returnKeyType: PropTypes.string,
    errorText: PropTypes.string,
    showEye: PropTypes.bool,
};

const DEVICE_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
    inputWrapper: {
        marginBottom: 12,
    },
    input: {
        borderColor: COLOR.textSecondaryInverse,
        borderWidth: 1,
        borderRadius: 8,
        width: DEVICE_WIDTH - 40,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 24,
        color: COLOR.darkBlack,
        fontSize: 16,
        fontFamily: 'NunitoSans-Regular',
    },
    inlineImg: {
        position: 'absolute',
        zIndex: 99,
        width: 22,
        height: 22,
        left: 35,
        top: 9,
    },
    btnEye: {
        position: 'absolute',
        zIndex: 99,
        width: 30,
        height: 30,
        right: 35,
        top: 9,
    },
    iconEye: {
        width: 25,
        height: 25,
        tintColor: 'rgba(0,0,0,0.5)',
    },
    hint: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 12,
        marginLeft: 24,
        color: COLOR.lightGray,
    },
    errorMessage: {
        color: COLOR.danger,
        textAlign: 'center',
        height: 25,
    },
});
