import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text } from 'react-native-animatable';

import { COLOR } from '@common/styles';
import Input from './input';
import ButtonSubmit from './button';
import { LmdToast } from './lmdToast';
import { $password } from '../rx/action';

class FormNewPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            re_password: '',
        };
    }

    onSubmit = () => {
        const { password } = this.state;
        let body = {
            username: this.props.username,
            password,
            sign_password: this.props.sign_password,
        };

        if (this.state.password.length < 6) {
            LmdToast.showToast('Minimal password 6 karakter', 'danger');
        } else if (this.state.password !== this.state.re_password) {
            LmdToast.showToast('Password tidak sesuai', 'danger');
        } else {
            this.props.$password(body);
        }
    };

    render() {
        return (
            <View animation="bounceIn" delay={20} style={styles.container} useNativeDriver>
                <Text style={styles.text}>Silahkan buat password Anda</Text>
                <Input
                    secureTextEntry={true}
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    showEye={true}
                    onChangeText={(password) => this.setState({ password })}
                    hint="Terdiri dari huruf dan angka minimal 6 karakter"
                />
                <Text style={styles.text}>Konfirmasi ulang password Anda</Text>
                <Input
                    secureTextEntry={true}
                    returnKeyType={'done'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    showEye={true}
                    onChangeText={(re_password) => this.setState({ re_password })}
                />
                <ButtonSubmit title="Submit" loading={this.props.isLoading} onPress={this.onSubmit} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 3,
        justifyContent: 'flex-start',
        marginTop: 24,
    },
    text: {
        fontFamily: 'NunitoSans-Regular',
        fontSize: 14,
        marginLeft: 24,
        marginBottom: 8,
    },
    urlContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    url: {
        color: COLOR.darkGreen,
        fontFamily: 'NunitoSans-ExtraBold',
        fontSize: 14,
    },
});

const mapStateToProps = (state, ownProps) => {
    return {
        username: state.Auth.username,
        sign_password: state.Auth.sign_password,
        isLoading: state.Activity.loading,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $password }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(FormNewPassword);
