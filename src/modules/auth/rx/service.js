import { AsyncStorage } from 'react-native';
import AuthRest from '@src/services/rest/rest.auth';

const rest = new AuthRest();

export const AuthServiceImplementation = class AuthService {
    constructor() {
        this.access_token = null;
    }

    async initialize() {
        await this._loadSession();
    }

    async _loadSession() {
        this.access_token = (await AsyncStorage.getItem('auth.access_token')) || null;
    }

    async _clearSession() {
        this.access_token = null;
        await AsyncStorage.removeItem('auth.access_token');
    }

    async saveSession(session) {
        this.access_token = session.token || null;
        await AsyncStorage.setItem('auth.access_token', this.access_token);
    }

    async logout() {
        await this._clearSession();
    }

    getAccessToken() {
        return this.access_token;
    }

    isAuthenticated() {
        return !!this.access_token;
    }

    login(data) {
        return rest.postLogin(data);
    }

    async check(data) {
        let body = {
            username: data.username,
            is_check: true,
        };
        return await rest.postCheck(body);
    }

    otp(data) {
        return rest.postRequestOTP(data);
    }

    verifyOTP(data) {
        return rest.postVerifyOTP(data);
    }

    registerDevicePost(body) {
        return rest.postRegister(body);
    }

    password(data) {
        return rest.postNewPassword(data);
    }
};

export const AuthService = new AuthServiceImplementation();
