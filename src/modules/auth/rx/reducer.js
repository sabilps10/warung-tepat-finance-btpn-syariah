import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    session: {},
    authenticated: false,
    history: {
        catalogs: [],
        search_keywords: [],
    },
    username: '',
    sign_password: '',
    newUser: false,
};

/**
 * Auth Reducer
 *
 * @param state
 * @param action
 * @returns {*}
 */
export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.LOGIN_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.LOGIN_SUCCESS:
            return {
                ...INITIAL_STATE,
                session: action.payload,
                authenticated: true,
            };
        case TYPE.LOGIN_EFORM:
            return {
                ...INITIAL_STATE,
                session: action.payload,
            };
        case TYPE.LOGOUT:
            return {
                ...INITIAL_STATE,
            };
        case TYPE.NOTIFICATION_DEVICE:
            return {
                ...INITIAL_STATE,
            };
        case TYPE.SAVE_USER:
            return {
                ...INITIAL_STATE,
                username: action.payload.username,
            };
        case TYPE.BEGIN_CHECK:
            return {
                ...INITIAL_STATE,
                username: action.payload.username,
            };
        case TYPE.CHECK_SUCCESS:
            return {
                ...state,
                newUser: action.payload,
                errors: {},
            };
        case TYPE.CHECK_FAILED:
            return {
                ...INITIAL_STATE,
                errors: action.payload,
            };
        case TYPE.OTP_SUCCESS:
            return {
                ...state,
                errors: {},
            };
        case TYPE.OTP_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.VERIFY_SUCCESS:
            return {
                ...state,
                sign_password: action.payload,
                errors: {},
            };
        case TYPE.VERIFY_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.PASSWORD_SUCCESS:
            return {
                ...INITIAL_STATE,
            };
        case TYPE.PASSWORD_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.RESET:
            return {
                ...state,
                errors: {},
            };
        case 'account::avatar.updated':
            state.session.user.avatar = action.payload;

            return {
                ...state,
            };
        case 'account::profile.updated':
            state.session.user.name = action.payload.name;
            state.session.user.email = action.payload.email;
            state.session.user.phone = action.payload.phone;

            return {
                ...state,
            };
        case 'catalog::search':
            if (state.history.search_keywords.indexOf(action.query) === -1) {
                state.history.search_keywords.unshift(action.query);
            }

            if (state.history.search_keywords.length >= 10) {
                state.history.search_keywords.splice(-1, 1);
            }

            return {
                ...state,
            };
        case 'catalog::show.catalog':
            let exist = false;
            for (const item of state.history.catalogs) {
                if (item.id === action.payload.id) {
                    exist = true;
                    break;
                }
            }

            if (!exist) {
                state.history.catalogs.unshift(action.payload);
            }

            if (state.history.catalogs.length >= 10) {
                state.history.catalogs.splice(-1, 1);
            }

            return {
                ...state,
            };
        default:
            return state;
    }
}

/**
 * Auth Presister State
 *
 * @param authenticated
 * @param user
 * @returns {{authenticated: *, user: *}}
 */
export function persister({ authenticated, session, history }) {
    return {
        authenticated,
        session,
        history,
    };
}
