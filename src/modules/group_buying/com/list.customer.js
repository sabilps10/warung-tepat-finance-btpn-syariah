import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, ListItem } from 'react-native-elements';
import PropTypes from 'prop-types';
import { Col, Grid } from 'react-native-easy-grid';

import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';

class ListCustomer extends PureComponent {
    render() {
        let { data, finished } = this.props;

        return (
            <ListItem
                containerStyle={styles.container}
                titleStyle={styles.name}
                subtitle={
                    <Text
                        style={styles.subtitle}>{data.phone} / {data.village_name} - {data.district_name}</Text>
                }
                title={data.name.toUpperCase()}
                rightElement={
                    finished ?
                        <View style={styles.buttonContainer}>
                            <Grid>
                                <Col>
                                    <Button
                                        buttonStyle={styles.buttonPrimaryStyle}
                                        type='solid'
                                        onPress={() => this.props.onChat(data)}
                                        icon={{
                                            name: 'chat-bubble',
                                            size: 15,
                                            color: 'white',
                                        }}
                                    />
                                </Col>
                                <Col>
                                    <Button
                                        buttonStyle={styles.buttonSuccessStyle}
                                        onPress={() => this.props.onApprove(data)}
                                        type='solid'
                                        icon={{
                                            name: 'check',
                                            size: 15,
                                            color: 'white',
                                        }}
                                    />
                                </Col>
                                <Col>
                                    <Button
                                        buttonStyle={styles.buttonDangerStyle}
                                        onPress={() => this.props.onDelete(data)}
                                        type='solid'
                                        icon={{
                                            name: 'close',
                                            size: 15,
                                            color: 'white',
                                        }}
                                    />
                                </Col>
                            </Grid>
                        </View>
                        :
                        <View>
                            <Button
                                buttonStyle={styles.buttonPrimaryStyle}
                                type='solid'
                                onPress={() => this.props.onChat(data)}
                                icon={{
                                    name: 'chat-bubble',
                                    size: 15,
                                    color: 'white',
                                }}
                            />
                        </View>
                }
            />
        );
    }
}

ListCustomer.propTypes = {
    data: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    onApprove: PropTypes.func.isRequired,
    onChat: PropTypes.func.isRequired,
    finished: PropTypes.bool,
};

export default ListCustomer;

const styles = StyleSheet.create({
    container: {
        padding: 0,
        borderBottomWidth: 0.5,
        borderColor: COLOR.border,
        elevation: 0,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
    },
    buttonContainer: {
        width: '30%',
    },
    subtitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        color: COLOR.darkGray,
    },
    name: {
        textAlign: 'left',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    buttonDangerStyle: {
        paddingVertical: 15,
        borderWidth: 0,
        borderRadius: 0,
        backgroundColor: COLOR.danger,
    },
    buttonSuccessStyle: {
        paddingVertical: 15,
        borderWidth: 0,
        borderRadius: 0,
        backgroundColor: COLOR.success,
        marginRight: 1,
        marginLeft: 1,
    },
    buttonPrimaryStyle: {
        paddingVertical: 15,
        borderWidth: 0,
        borderRadius: 0,
        backgroundColor: COLOR.secondary,
    },
});
