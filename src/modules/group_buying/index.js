import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Clipboard, FlatList, Linking, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import Moment from 'react-moment';
import { ListItem } from 'react-native-elements';

import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { CurrencyFormatter, Toast } from '../../common/utils';

import PkaDialog from '../../components/dialog';
import EmptyState from '../../components/empty_state';
import PkaButton from '../../components/button';

import { $approve, $delete, $load } from './rx/action';
import ListCustomer from './com/list.customer';

class GroupBuyingScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            is_empty: false,
            showDialogDelete: false,
            showDialogApprove: false,
            dialogData: {},
        };

        this._onLoad = this._onLoad.bind(this);
        this._onDelete = this._onDelete.bind(this);
        this._onApprove = this._onApprove.bind(this);
        this._onChat = this._onChat.bind(this);
        this._onCancelDialog = this._onCancelDialog.bind(this);
    }

    componentDidMount() {
        this._onLoad();
    }

    getLinkReff() {
        return 'https://store.kora.id/group-buying';
    }

    _onLoad() {
        this.props.$load().then((res) => {
            this.setState({ is_empty: !res });
        });
    }

    _onDelete(item) {
        this.setState({
            showDialogDelete: true,
            showDialogApprove: false,
            dialogData: item,
        });
    }

    _onApprove(item) {
        this.setState({
            showDialogDelete: false,
            showDialogApprove: true,
            dialogData: item,
        });
    }

    _onChat(item) {
        let phone = item.phone;
        if (phone[0] == 0) {
            phone = phone.replace('0', '+62');
        }

        Linking.openURL(`whatsapp://send?text=&phone=${phone}`);
    }

    _onCancelDialog() {
        this.setState({
            showDialogDelete: false,
            showDialogApprove: false,
            deleteData: {},
        });
    }

    _onSubmitApprove = () => {
        this.props.$approve(this.state.dialogData.id).then((res) => {
            if (res) {
                this._onCancelDialog();
                this._onLoad();

                Toast('Keranjang Belanja Telah Terbuat');
            }
        });
    };

    _onSubmitDelete = () => {
        this.props.$delete(this.state.dialogData.id).then((res) => {
            if (res) {
                this._onCancelDialog();
                this._onLoad();

                Toast('Pesanan Telah Dibatalkan');
            }
        });
    };

    onClick = () => {
        Clipboard.setString(this.getLinkReff()), Toast('Tautan Telah Disalin');
    };

    _flatItem = ({ index, item }) => (
        <ListCustomer
            key={index}
            data={item}
            onDelete={this._onDelete}
            onApprove={this._onApprove}
            onChat={this._onChat}
            finished={this.props.screen.status == 'finish'}
        />
    );

    _flatKey = (item, index) => index.toString();

    _flatReff = (ref) => (this.listRef = ref);

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this._onLoad} colors={[COLOR.primary]} />;
    }

    _renderHeader() {
        let { screen } = this.props;

        return (
            <View style={styles.headerContainer}>
                <ListItem
                    title={screen.catalog_name + ' x ' + screen.quantity}
                    subtitle={
                        <View>
                            <Text style={styles.headerSubTitle}>
                                Periode: &nbsp;&nbsp;&nbsp;&nbsp;
                                <Moment element={Text} format="DD/MM/YYYY">
                                    {screen.started_at}
                                </Moment>{' '}
                                -
                                <Moment element={Text} format="DD/MM/YYYY">
                                    {screen.finished_at}
                                </Moment>
                            </Text>
                            <Text style={styles.headerSubTitle}>
                                Total Order: &nbsp;&nbsp; {`${CurrencyFormatter(screen.total_order)}`}
                            </Text>
                        </View>
                    }
                    leftIcon={{
                        name: 'loyalty',
                        color: COLOR.success,
                        containerStyle: {
                            borderWidth: 1,
                            padding: 5,
                            borderRadius: 5,
                            borderColor: COLOR.success,
                        },
                    }}
                    rightTitle={`${CurrencyFormatter(screen.current_price)}`}
                    rightSubtitle={
                        <Text style={styles.headerRightSubTitle}>{`${CurrencyFormatter(screen.normal_price)}`}</Text>
                    }
                    titleStyle={styles.headerTitle}
                    rightTitleStyle={styles.headerTitle}
                />
            </View>
        );
    }

    render() {
        let { screen } = this.props;

        if (this.state.is_empty) {
            return (
                <EmptyState
                    title="Belum Ada Event Pembelian Kolektif"
                    subtitle="Pembelian Kolektif akan tampil disini"
                    image="https://s3.kora.id/app/assets/empty-referral.png"
                />
            );
        }

        return (
            <View style={styles.container}>
                <View style={styles.bodyContainer}>
                    <ScrollView
                        refreshControl={this._renderRefresh()}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                    >
                        {this._renderHeader()}

                        {screen.customers && screen.customers.length > 0 ? (
                            <FlatList
                                scrollEventThrottle={16}
                                data={screen.customers}
                                keyExtractor={this._flatKey}
                                renderItem={this._flatItem}
                                onEndReachedThreshold={0.1}
                                initialNumToRender={100}
                                showsVerticalScrollIndicator={true}
                                removeClippedSubviews={true}
                                ref={this._flatReff}
                            />
                        ) : (
                            <EmptyState
                                title="Belum Ada Pembelian Kolektif"
                                subtitle="Semua pembelian kolektif akan tampil disini"
                                image="https://s3.kora.id/app/assets/empty-transaction.png"
                            />
                        )}
                    </ScrollView>
                </View>

                <View style={styles.footContainer}>
                    <View>
                        <Text style={styles.titleFoot}>
                            Salin (Klik) link dibawah dan share kepada kerabat Anda, {'\n'}
                            semakin banyak orang yang membeli, harga barang akan semakin menurun.
                        </Text>
                        <PkaButton color={COLOR.success} onPress={this.onClick} title={this.getLinkReff()} />
                    </View>
                </View>

                <PkaDialog
                    onCancel={this._onCancelDialog}
                    onSubmit={this._onSubmitDelete}
                    title="Hapus Membera"
                    text={'Anda yakin akan menghapus Member \n atas nama ?'}
                    visible={this.state.showDialogDelete}
                />

                <PkaDialog
                    onCancel={this._onCancelDialog}
                    onSubmit={this._onSubmitApprove}
                    title="Setujui Pesanan"
                    text={
                        'Anda yakin akan membuat order dengan Member \n atas nama "' +
                        this.state.dialogData.name +
                        '" ?'
                    }
                    visible={this.state.showDialogApprove}
                />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        screen: state.GroupBuying.data,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $load,
            $approve,
            $delete,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupBuyingScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainer: {
        borderBottomWidth: 0.5,
        borderBottomColor: COLOR.border,
    },
    headerTitle: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        letterSpacing: 1,
    },
    headerSubTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    headerRightSubTitle: {
        textDecorationLine: 'line-through',
        fontFamily: SSP.semi_bold,
        color: COLOR.danger,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    bodyContainer: {
        flex: 1,
        backgroundColor: COLOR.light,
        paddingBottom: 100,
    },
    footContainer: {
        position: 'absolute',
        bottom: 0,
        height: 'auto',
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        paddingVertical: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    titleFoot: {
        textAlign: 'center',
        fontFamily: SSP.semi_bold,
        color: COLOR.darkGray,
        fontSize: FONT_SIZE.SMALL,
    },
    totalText: {
        textAlign: 'center',
        fontFamily: SSP.semi_bold,
        color: COLOR.darkGray,
        fontSize: FONT_SIZE.SMALL,
        margin: 10,
    },
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 120,
        backgroundColor: '#fff',
        borderRadius: 30,
        elevation: 8,
        zIndex: 9999,
    },
    fabIcon: {
        fontSize: 30,
        color: 'black',
    },
    footContainer: {
        position: 'absolute',
        bottom: 0,
        height: 'auto',
        width: '100%',
        backgroundColor: COLOR.white,
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        paddingVertical: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    titleFoot: {
        textAlign: 'center',
        fontFamily: SSP.semi_bold,
        color: COLOR.darkGray,
        fontSize: FONT_SIZE.SMALL,
        marginBottom: 10,
    },
});
