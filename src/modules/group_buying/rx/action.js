import * as Activity from '../../../services/activity/action';
import { GroupBuyingService } from './service';

export const TYPE = {
    FETCH_GROUPBUYING: 'group_buying::fetch.data',
};


export function $load() {
    return (dispatch) => {
        Activity.processing(dispatch, 'GroupBuying', 'Load');

        return GroupBuyingService.get()
            .then(res => {
                dispatch(
                    {
                        type: TYPE.FETCH_GROUPBUYING,
                        payload: res.data,
                    },
                );

                return true;
            })
            .catch(err => {
                return false;
            })
            .finally(res => {
                Activity.done(dispatch, 'GroupBuying', 'Load');

                return res;
            });
    };
}

export function $approve(id) {
    return (dispatch) => {
        return GroupBuyingService.approve(id)
            .then(res => {
                return true;
            })
            .catch(err => {
                return false;
            })
            .finally(res => {
                return res;
            });
    };
}

export function $delete(id) {
    return (dispatch) => {
        return GroupBuyingService.delete(id)
            .then(res => {
                return true;
            })
            .catch(err => {
                return false;
            })
            .finally(res => {
                return res;
            });
    };
}
