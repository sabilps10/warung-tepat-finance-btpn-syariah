import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    data: {},
};


export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_GROUPBUYING:
            return {
                ...state,
                data: action.payload,
            };
        default:
            return state;
    }
}

export function persister({ data }) {
    return {
        data,
    };
}
