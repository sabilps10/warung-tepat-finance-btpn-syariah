import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    ScrollView,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableNativeFeedback,
    View,
    Image,
    Dimensions,
} from 'react-native';
import Countly from 'countly-sdk-react-native-bridge';
import { Button } from 'react-native-elements';
import { $search } from './rx/action';
import { COLOR, FONT_SIZE, PADDING, SSP, MARKOT, NUNITO } from '../../common/styles';
import { NavigationActions } from 'react-navigation';
import { HeaderBackButton } from 'react-navigation-stack';
import { Header, ListItem } from 'react-native-elements';
import { CurrencyFormatter } from '../../common/utils';
import SearchBar from '../../components/search_bar';
import empty from '@assets/img/empty_search2.png';
import Tab from './com/Tab';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 230;

class SearchScreen extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            results: [],
            catalog: [],
            supplier: [],
            isEmpty: false,
        };
    }

    clearState = () => {
        this.setState({
            query: '',
            results: [],
            catalog: [],
            supplier: [],
            isEmpty: false,
        });
    };

    load(query) {
        const { session } = this.props;
        const { user } = session;

        if (query !== '') {
            const eventSearch = { eventName: 'Search', eventCount: 1 };
            eventSearch.segments = {
                Username: user.username,
                Name: user.name,
                Keyword: query,
            };
            Countly.sendEvent(eventSearch);

            this.props.$search(query).then((res) => {
                if (res) {
                    this.setState({
                        results: res,
                        isEmpty: false,
                        catalog: res.catalog.data,
                        supplier: res.supplier.data,
                    });
                } else {
                    this.setState({
                        isEmpty: true,
                    });
                }
            });
        } else {
            this.setState({
                results: [],
                isEmpty: false,
            });
        }
    }

    onSearchKeyword(query) {
        if (query !== this.state.query) {
            this.setState({ query });
            this.load(query);
        } else {
            this.setState({
                results: [],
                query: '',
                isEmpty: false,
            });
        }
    }

    _renderHeader() {
        const containerStyle = { marginLeft: -15 };
        const inputStyle = {
            lineHeight: 20,
            fontSize: 12,
            padding: 0,
        };
        let { query, isEmpty } = this.state;

        return (
            <Header
                containerStyle={styles.headerContainer}
                centerContainerStyle={containerStyle}
                placement="left"
                leftComponent={
                    <HeaderBackButton
                        tintColor={COLOR.secondary}
                        onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                    />
                }
                centerComponent={
                    <SearchBar
                        lightTheme
                        placeholder="Ketik di sini untuk cari Produk"
                        onChange={(value) => this.setState({ query: value })}
                        onSubmitEditing={() => this.load(query)}
                        onClear={this.clearState}
                        isEmpty={isEmpty}
                        value={query}
                        inputStyle={inputStyle}
                        autoFocus={true}
                    />
                }
            />
        );
    }

    _renderHistoryQuery() {
        let { history_keywords } = this.props;
        let { query } = this.state;

        if (history_keywords.length === 0) {
            return null;
        }

        return (
            <View style={styles.sectionHistoryKeyword}>
                <Text style={styles.titleSection}>RIWAYAT PENCARIAN</Text>
                <View style={styles.historyKeywordContainer}>
                    {history_keywords.map((keyword, i) => {
                        return (
                            <TouchableNativeFeedback
                                key={i}
                                onPress={() => this.onSearchKeyword(keyword)}
                                background={TouchableNativeFeedback.Ripple('#f4f4f4')}
                            >
                                <View
                                    style={
                                        query === keyword
                                            ? { ...styles.keywordBadge, ...styles.keywordBadgeActive }
                                            : { ...styles.keywordBadge }
                                    }
                                    key={i}
                                >
                                    <Text style={styles.keywordBadgeText}>{keyword}</Text>
                                </View>
                            </TouchableNativeFeedback>
                        );
                    })}
                </View>
            </View>
        );
    }

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} colors={[COLOR.primary]} />;
    }

    _renderHistoryProduct() {
        let { history_catalogs } = this.props;

        if (history_catalogs.length === 0) {
            return null;
        }
        return (
            <View style={styles.sectionHistoryProduct}>
                <Text style={styles.titleSection}>RIWAYAT PRODUK</Text>
                <View style={styles.historyProductContainer}>
                    {history_catalogs.map((catalog, i) => {
                        return (
                            <ListItem
                                key={i}
                                leftAvatar={{ source: { uri: catalog.image_default } }}
                                title={catalog.name}
                                subtitle={
                                    <Text style={styles.historyProductSubTitle}>
                                        {catalog.size ? (catalog.size !== '' ? `Isi: ${catalog.size}` : null) : null}

                                        {catalog.item
                                            ? catalog.item.unit !== ''
                                                ? ' / ' + catalog.item.unit
                                                : null
                                            : null}

                                        {catalog.max_monthly !== 0 ? (
                                            <Text>&nbsp;(Max: {catalog.max_monthly})</Text>
                                        ) : null}
                                    </Text>
                                }
                                rightTitle={`${CurrencyFormatter(catalog.selling_price)}`}
                                rightSubtitle={
                                    catalog.discount !== 0 ? ` ${CurrencyFormatter(catalog.regular_price)}` : null
                                }
                                containerStyle={styles.productList}
                                titleStyle={styles.historyProductTitle}
                                rightTitleStyle={styles.historyProductRightTitle}
                                rightSubtitleStyle={styles.historyProductRightSubTitle}
                                onPress={() => this.navigateCatalog(catalog)}
                                activeOpacity={0.97}
                            />
                        );
                    })}
                </View>
            </View>
        );
    }

    _renderRecommeded() {
        let { recomens } = this.props;

        if ((Object.keys(recomens).length === 0 && recomens.constructor === Object) || recomens.length === 0) {
            return null;
        }

        return (
            <View style={styles.sectionRecommendProduct}>
                <Text style={styles.titleSection}>REKOMENDASI PRODUK</Text>
                <View style={styles.recommendProductContainer}>
                    {recomens.map((catalog, i) => {
                        return (
                            <ListItem
                                key={i}
                                leftAvatar={{ source: { uri: catalog.image_default } }}
                                title={catalog.name}
                                subtitle={
                                    <Text style={styles.historyProductSubTitle}>
                                        {catalog.size ? (catalog.size !== '' ? `Isi: ${catalog.size}` : null) : null}

                                        {catalog.item
                                            ? catalog.item.unit !== ''
                                                ? ' / ' + catalog.item.unit
                                                : null
                                            : null}

                                        {catalog.max_monthly !== 0 ? (
                                            <Text>&nbsp;(Max: {catalog.max_monthly})</Text>
                                        ) : null}
                                    </Text>
                                }
                                rightTitle={`${CurrencyFormatter(catalog.selling_price)}`}
                                rightSubtitle={
                                    catalog.discount !== 0 ? ` ${CurrencyFormatter(catalog.regular_price)}` : null
                                }
                                containerStyle={styles.productList}
                                titleStyle={styles.recommendProductTitle}
                                rightTitleStyle={styles.recommendProductRightTitle}
                                rightSubtitleStyle={styles.recommendProductRightSubTitle}
                                onPress={() => this.navigateCatalog(catalog)}
                                activeOpacity={0.97}
                            />
                        );
                    })}
                </View>
            </View>
        );
    }

    _renderEmpty() {
        let { isEmpty } = this.state;

        if (!isEmpty) {
            return null;
        }
        return (
            <View style={styles.emptyTabContainer}>
                <Image source={empty} style={styles.iconEmptyImage} resizeMode="contain" />
                <View style={styles.titleTextContainer}>
                    <Text style={styles.emptyText}>Ups, Pencarian Tidak Ditemukan</Text>
                </View>
                <View>
                    <Text style={styles.emptySubitleText}>Kami tidak menemukan kata kunci yang</Text>
                    <Text style={styles.emptySubitleText}>kamu cari, coba cari kembali.</Text>
                </View>
            </View>
        );
    }

    _renderClear() {
        return (
            <Button
                title="Cari Lagi"
                buttonStyle={styles.searchButton}
                titleStyle={styles.searchButtonText}
                onPress={this.clearState}
            />
        );
    }

    navigateCatalog(catalog) {
        const event = { eventName: 'Products', eventCount: 1 };

        event.segments = {
            Name: catalog.name,
        };

        Countly.sendEvent(event);
        setTimeout(() => {
            this.props.navigation.navigate('CatalogDetail', {
                catalog_id: catalog.id,
                product: catalog,
            });
        }, 0);
    }

    render() {
        let { results, catalog, supplier, isEmpty } = this.state;

        if (isEmpty) {
            return (
                <View style={styles.isEmptyContainer}>
                    {this._renderHeader()}
                    <ScrollView style={styles.scrollViewEmptyContainer}>
                        <View>{this._renderEmpty()}</View>
                        {this._renderClear()}
                        {this._renderHistoryQuery()}
                        {this._renderHistoryProduct()}
                        {this._renderRecommeded()}
                    </ScrollView>
                </View>
            );
        }

        if (results.length !== 0) {
            return (
                <View style={styles.container}>
                    {this._renderHeader()}
                    <View style={styles.tabContainer}>
                        <Tab navigation={this.props.navigation} catalog={catalog} supplier={supplier} />
                    </View>
                    <View style={styles.container}>
                        <ScrollView refreshControl={this._renderRefresh()}>
                            {this._renderHistoryQuery()}
                            {this._renderHistoryProduct()}
                            {this._renderRecommeded()}
                        </ScrollView>
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    {this._renderHeader()}
                    <ScrollView style={styles.scrollViewContiner} refreshControl={this._renderRefresh()}>
                        {this._renderHistoryQuery()}
                        {this._renderHistoryProduct()}
                        {this._renderRecommeded()}
                    </ScrollView>
                </View>
            );
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
        recomens: state.Home.recomens,
        history_catalogs: state.Auth.history.catalogs,
        history_keywords: state.Auth.history.search_keywords,
        session: state.Auth.session,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $search }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

const styles = StyleSheet.create({
    container: {
        flex: 3.3,
        backgroundColor: COLOR.white,
    },
    headerContainer: {
        backgroundColor: COLOR.white,
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 0,
        paddingLeft: 0,
        paddingRight: 10,
        zIndex: 100,
        borderBottomWidth: 0,
        elevation: 1,
    },
    scrollViewContiner: {
        marginTop: '5%',
    },
    titleSection: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        marginHorizontal: '3%',
    },
    sectionHistoryKeyword: {
        marginTop: '2%',
        backgroundColor: COLOR.white,
    },
    historyKeywordContainer: {
        flex: 2,
        marginHorizontal: '3%',
        alignItems: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    keywordBadge: {
        backgroundColor: COLOR.white,
        borderWidth: 1,
        borderColor: COLOR.off,
        padding: 5,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        position: 'relative',
        marginRight: PADDING.PADDING_CARD,
        width: 'auto',
        borderRadius: 6,
        marginTop: 7,
    },
    keywordBadgeActive: {
        borderColor: COLOR.success,
    },
    keywordBadgeText: {
        color: COLOR.textGray,
        fontFamily: NUNITO.regular,
        fontSize: FONT_SIZE.MEDIUM,
    },
    sectionRecommendProduct: {
        marginTop: '2%',
        backgroundColor: COLOR.white,
    },
    recommendProductContainer: {
        paddingTop: 5,
    },
    recommendProductTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    recommendProductRightTitle: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    recommendProductSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    recommendProductRightSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        textDecorationLine: 'line-through',
    },
    productList: {
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderColor: COLOR.shadow,
    },
    iconEmptyImage: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: 260,
        width: 260,
        marginHorizontal: '20%',
        marginVertical: '-7%',
    },
    titleTextContainer: {
        marginTop: 8,
        marginBottom: 16,
    },
    emptyText: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    emptySubitleText: {
        fontFamily: NUNITO.regular,
        color: COLOR.textGray,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    searchButton: {
        backgroundColor: COLOR.darkGreen,
        width: DEVICE_WIDTH - MARGIN,
        height: 40,
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 16,
        borderRadius: 10,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingBottom: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    searchButtonText: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        alignItems: 'center',
        justifyContent: 'center',
    },
    isEmptyContainer: {
        flex: 1,
        backgroundColor: COLOR.white,
    },
    scrollViewEmptyContainer: {
        backgroundColor: COLOR.white,
    },
    emptyContainer: {
        marginBottom: 20,
    },
    grid: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 1,
    },
    gridItem: {
        margin: 5,
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    gridItemImage: {
        width: 100,
        height: 100,
        borderWidth: 1.5,
        borderColor: 'white',
        borderRadius: 50,
    },
    gridItemText: {
        marginTop: 5,
        textAlign: 'center',
    },
    titleRecommendation: {
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: MARKOT.bold,
        color: COLOR.darkBlack,
        marginLeft: '3%',
    },
    sectionHistoryProduct: {
        marginTop: '2%',
        backgroundColor: COLOR.white,
    },
    historyProductContainer: {
        paddingTop: 5,
    },
    historyProductTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    historyProductRightTitle: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    historyProductSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    historyProductRightSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        textDecorationLine: 'line-through',
    },
    tabContainer: {
        minHeight: 340,
    },
    listContainer: {
        paddingTop: 5,
    },
    emptyTabContainer: {
        marginBottom: 20,
    },
});
