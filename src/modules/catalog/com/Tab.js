import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import ListTab from './ListTab';
import { COLOR, FONT_SIZE, NUNITO } from '../../../common/styles';
import React, { PureComponent } from 'react';

class Tab extends PureComponent {
    render() {
        let Tabs = createAppContainer(
            createMaterialTopTabNavigator(
                {
                    Produk: {
                        screen: () => (
                            <ListTab title="Produk" navigation={this.props.navigation} catalogs={this.props.catalog} />
                        ),
                        navigationOptions: {
                            title: 'Produk',
                        },
                    },
                    Toko: {
                        screen: () => (
                            <ListTab
                                title="Toko Resmi"
                                navigation={this.props.navigation}
                                suppliers={this.props.supplier}
                            />
                        ),
                        navigationOptions: {
                            title: 'Toko Resmi',
                        },
                    },
                },
                {
                    tabBarPosition: 'top',
                    initialRouteName: 'Produk',
                    animationEnabled: true,
                    lazy: true,
                    optimizationsEnabled: true,
                    backBehavior: 'none',
                    tabBarOptions: {
                        upperCaseLabel: false,
                        activeTintColor: COLOR.primary,
                        inactiveTintColor: COLOR.textGray,
                        allowFontScaling: false,
                        style: {
                            backgroundColor: COLOR.white,
                            elevation: 4,
                            height: 40,
                        },
                        tabStyle: {
                            paddingVertical: 12,
                        },
                        indicatorStyle: {
                            backgroundColor: COLOR.primary,
                            height: 3,
                        },
                        labelStyle: {
                            fontFamily: NUNITO.extraBold,
                            fontSize: FONT_SIZE.LARGE,
                            padding: 0,
                            margin: 0,
                        },
                    },
                },
            ),
        );
        return <Tabs />;
    }
}
export default Tab;
