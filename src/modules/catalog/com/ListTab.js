import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, Image, ScrollView, Dimensions } from 'react-native';
import Countly from 'countly-sdk-react-native-bridge';
import { ListItem } from 'react-native-elements';
import { COLOR, FONT_SIZE, SSP, NUNITO, MARKOT, PADDING } from '../../../common/styles';
import { CurrencyFormatter } from '../../../common/utils';
import iconSupplier from '@assets/icons/supplier.png';
import empty from '@assets/img/empty_search2.png';
const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 230;

class ListTab extends PureComponent {
    navigateCatalog(catalog) {
        const event = { eventName: 'Products', eventCount: 1 };

        event.segments = {
            Name: catalog.name,
        };

        Countly.sendEvent(event);
        setTimeout(() => {
            this.props.navigation.navigate('CatalogDetail', {
                catalog_id: catalog.id,
                product: catalog,
            });
        }, 0);
    }

    toSupplierPage = (supplier) => {
        const supplierName = supplier.name;
        const supplierCode = supplier.id;
        const type = false;
        this.props.navigation.navigate(
            'Supplier',
            {
                supplierCode,
                supplierName,
                type,
            },
            true,
        );
    };

    _renderEmpty() {
        return (
            <View style={styles.emptyTabContainer}>
                <Image source={empty} style={styles.iconEmptyImage} resizeMode="contain" />
                <View style={styles.titleTextContainer}>
                    <Text style={styles.emptyText}>Ups, Pencarian Tidak Ditemukan</Text>
                </View>
                <View>
                    <Text style={styles.emptySubitleText}>Kami tidak menemukan kata kunci yang</Text>
                    <Text style={styles.emptySubitleText}>kamu cari, coba cari kembali.</Text>
                </View>
            </View>
        );
    }

    _product() {
        let { title, catalogs, suppliers } = this.props;

        if (title === 'Produk') {
            if (catalogs !== null) {
                return (
                    <View>
                        <View style={styles.listContainer}>
                            {catalogs.map((catalog, i) => {
                                return (
                                    <ListItem
                                        key={i}
                                        leftAvatar={{ source: { uri: catalog.image_default } }}
                                        title={catalog.name}
                                        subtitle={
                                            <Text style={styles.listSubTitle}>
                                                {catalog.size ? `Isi: ${catalog.size}` : null}
                                            </Text>
                                        }
                                        rightTitle={`${CurrencyFormatter(catalog.selling_price)}`}
                                        rightSubtitle={
                                            catalog.discount !== 0
                                                ? ` ${CurrencyFormatter(catalog.regular_price)}`
                                                : null
                                        }
                                        containerStyle={styles.listItem}
                                        titleStyle={styles.listTitle}
                                        subtitleStyle={styles.listRightSubTitle}
                                        rightTitleStyle={styles.listRightTitle}
                                        rightSubtitleStyle={styles.listRightSubTitle}
                                        onPress={() => this.navigateCatalog(catalog)}
                                        activeOpacity={0.97}
                                    />
                                );
                            })}
                        </View>
                    </View>
                );
            } else {
                return <View>{this._renderEmpty()}</View>;
            }
        } else {
            if (suppliers !== null) {
                return (
                    <View>
                        <View style={styles.listContainer}>
                            {suppliers.map((supplier, i) => {
                                return (
                                    <ListItem
                                        key={i}
                                        leftAvatar={
                                            <Image
                                                source={iconSupplier}
                                                style={styles.iconSupplierContainer}
                                                resizeMode="contain"
                                            />
                                        }
                                        title={supplier.name}
                                        containerStyle={styles.listItem}
                                        titleStyle={styles.listTitle}
                                        onPress={() => this.toSupplierPage(supplier)}
                                        activeOpacity={0.97}
                                    />
                                );
                            })}
                        </View>
                    </View>
                );
            } else {
                return <View>{this._renderEmpty()}</View>;
            }
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>{this._product()}</ScrollView>
            </View>
        );
    }
}
export default ListTab;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 5,
    },
    listContainer: {
        paddingTop: 5,
    },
    listSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
    },
    listItem: {
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderColor: COLOR.shadow,
    },
    listTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    listRightTitle: {
        color: COLOR.danger,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    listRightSubTitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
        textDecorationLine: 'line-through',
    },
    iconSupplierContainer: {
        width: 30,
        height: 30,
    },
    emptyContainer: {
        marginBottom: 20,
    },
    emptyText: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    searchButton: {
        backgroundColor: COLOR.darkGreen,
        width: DEVICE_WIDTH - MARGIN,
        height: 40,
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 16,
        borderRadius: 10,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        paddingBottom: PADDING.PADDING_VERTICAL_CONTAINER,
    },
    searchButtonText: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.MEDIUM,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleTextContainer: {
        marginTop: 8,
        marginBottom: 16,
    },
    emptyTabContainer: {
        marginBottom: 20,
    },
    iconEmptyImage: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: 260,
        width: 260,
        marginHorizontal: '20%',
        marginVertical: '-7%',
    },
    emptySubitleText: {
        fontFamily: NUNITO.regular,
        color: COLOR.textGray,
        fontSize: FONT_SIZE.LARGE,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
});
