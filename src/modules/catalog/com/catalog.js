import React, { Component } from 'react';
import { ActivityIndicator, RefreshControl, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';
import { View } from 'react-native-animatable';
import { bindActionCreators } from 'redux';
import { FlatList, NavigationEvents } from 'react-navigation';

import { $load, $loadmore } from '../rx/action';
import { COLOR } from '../../../common/styles';
import EmptyState from '../../../components/empty_state';
import { ProductLoading, ProductThumb } from '../../../components/product';

class Catalog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showTop: false,
            data: [],
            page: 1,
            total: 0,
            is_empty: false,
        };

        this._onRefresh = this._onRefresh.bind(this);
        this._onLoad = this._onLoad.bind(this);
        this._onLoadMore = this._onLoadMore.bind(this);
        this._onScroll = this._onScroll.bind(this);
        this._onGoTop = this._onGoTop.bind(this);
        this._onNavigationFocus = this._onNavigationFocus.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        // rerender hanya saat data berubah
        if (
            nextState.data === this.state.data &&
            nextState.showTop === this.state.showTop &&
            nextState.is_empty === this.state.is_empty
        ) {
            return false;
        }

        return true;
    }

    _onRefresh() {
        this.props.$load(this.props.category.id).then((res) => {
            if (typeof this.props.catalogs[this.props.category.id] !== 'undefined') {
                this.setState(this.props.catalogs[this.props.category.id]);
            } else {
                this.setState({ is_empty: true });
            }
        });
    }

    _onLoad() {
        if (this.state.data.length === 0) {
            this.props.$load(this.props.category.id).then((res) => {
                if (typeof this.props.catalogs[this.props.category.id] !== 'undefined') {
                    this.setState(this.props.catalogs[this.props.category.id]);
                } else {
                    this.setState({ is_empty: true });
                }
            });
        }
    }

    _onScroll(event) {
        if (event.nativeEvent.contentOffset.y > 500) {
            this.setState({ showTop: true });
        } else {
            this.setState({ showTop: false });
        }
    }

    _onGoTop() {
        this.listRef.scrollToOffset({
            offset: 0,
            animated: true,
        });
    }

    _onLoadMore() {
        if (!this.props.loadmore && this.state.total === 10) {
            this.props.$loadmore(this.props.category.id, this.state.page + 1).then((res) => {
                if (typeof this.props.catalogs[this.props.category.id] !== 'undefined') {
                    this.setState(this.props.catalogs[this.props.category.id]);
                }
            });
        }
    }

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this._onRefresh} colors={[COLOR.primary]} />;
    }

    _renderFooter() {
        const activityStyle = {
            paddingTop: 20,
            paddingBottom: 20,
        };

        return this.props.loading ? (
            <ActivityIndicator color={COLOR.secondary} style={activityStyle} animating size="large" />
        ) : null;
    }

    _renderToTop() {
        return this.state.showTop ? (
            <TouchableOpacity onPress={this._onGoTop} style={styles.fab}>
                <Icon name="chevron-up" type="feather" size={21} />
            </TouchableOpacity>
        ) : null;
    }

    _flatItem = ({ index, item }) => <ProductThumb key={index} product={item} navigation={this.props.navigation} />;

    _flatKey = (item, index) => index;

    _flatReff = (ref) => (this.listRef = ref);

    _onNavigationFocus() {
        this._onLoad();
    }

    render() {
        return (
            <View>
                <NavigationEvents onWillFocus={this._onNavigationFocus} />
                {this.state.is_empty ? (
                    <View>
                        <EmptyState
                            title="Maaf, Belum Ada Produk"
                            subtitle1="Belum ada produk di kategori. Tapi"
                            subtitle2="jangan sedih, sebentar lagi akan segera hadir"
                            subtitle3="hadir produk untuk kategori ini."
                            refreshControl={this._renderRefresh()}
                        />
                    </View>
                ) : (
                    <ProductLoading onReady={this.state.data.length > 0} bgColor="#efefef" animate="fade">
                        <View animation="fadeIn" useNativeDriver>
                            <FlatList
                                scrollEventThrottle={16}
                                onScroll={this._onScroll}
                                refreshControl={this._renderRefresh()}
                                ListFooterComponent={this._renderFooter()}
                                data={this.state.data}
                                keyExtractor={this._flatKey}
                                horizontal={false}
                                numColumns={2}
                                renderItem={this._flatItem}
                                onEndReached={this._onLoadMore}
                                onEndReachedThreshold={1}
                                initialNumToRender={10}
                                showsVerticalScrollIndicator={false}
                                removeClippedSubviews={true}
                                ref={this._flatReff}
                            />

                            {this._renderToTop()}
                        </View>
                    </ProductLoading>
                )}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        catalogs: state.Catalog.catalogs,
        loadmore: state.Catalog.loadmore,
        loading: state.Activity.page,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $load, $loadmore }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);

const styles = StyleSheet.create({
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 50,
        backgroundColor: '#fff',
        borderRadius: 30,
        elevation: 8,
        zIndex: 9999,
    },
    fabIcon: {
        fontSize: 30,
        color: 'black',
    },
});
