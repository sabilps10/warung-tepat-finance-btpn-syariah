import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { View } from 'react-native-animatable';
import { connect } from 'react-redux';

import { COLOR, FONT_SIZE, SSP } from '../../common/styles';
import Headers from '../../components/header';
import Catalog from './com/catalog';

class CatalogTabs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            routeConfig: this.routeConfig(this.props.categories),
        };
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.navigation.getParam('category_id') !== this.props.navigation.getParam('category_id')) {
            return true;
        }

        return false;
    }

    routeConfig(categories) {
        return categories.reduce((routes, category) => {
            let key = 'catalog' + category.id;
            routes[key] = {
                screen: () => <Catalog category={category} navigation={this.props.navigation} />,
                path: 'catalog/' + category.id,
                navigationOptions: {
                    title: category.name,
                },
            };

            return routes;
        }, {});
    }

    render() {
        const displayStyle = { flex: 1 };
        let { routeConfig } = this.state;
        let { navigation } = this.props;

        if (!routeConfig) {
            return null;
        }

        let Tabs = createAppContainer(
            createMaterialTopTabNavigator(routeConfig, {
                tabBarPosition: 'top',
                initialRouteName: 'catalog' + navigation.getParam('category_id', 1),
                animationEnabled: true,
                optimizationsEnabled: true,
                lazy: true,
                backBehavior: 'none',
                tabBarOptions: {
                    activeTintColor: COLOR.primaryDarkest,
                    inactiveTintColor: COLOR.primaryDarkest,
                    allowFontScaling: false,
                    scrollEnabled: true,
                    style: {
                        backgroundColor: COLOR.primary,
                        elevation: 0.5,
                        height: 40,
                    },
                    tabStyle: {
                        paddingVertical: 12,
                    },
                    indicatorStyle: {
                        backgroundColor: COLOR.primaryDarkest,
                        height: 3,
                    },
                    labelStyle: {
                        fontFamily: SSP.semi_bold,
                        fontSize: FONT_SIZE.SMALL,
                        padding: 0,
                        margin: 0,
                    },
                },
            }),
        );

        return (
            <View style={displayStyle}>
                <Headers navigation={this.props.navigation} />
                <Tabs />
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categories: state.Catalog.categories,
    };
};

export default connect(mapStateToProps)(CatalogTabs);
