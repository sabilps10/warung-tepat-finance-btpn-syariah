import { TYPE } from './action';

const INITIAL_STATE = {
    loadmore: false,
    categories: {},
    catalogs: {},
    catalog: {},
    search_results: {
        data: [],
        total: 0,
        page: 1,
        query: '',
    },
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_CATEGORY:
            return {
                ...state,
                categories: action.payload,
            };
        case TYPE.FETCH_CATALOG:
            state.catalogs[action.category_id] = {
                data: action.payload,
                total: action.total,
                page: 1,
            };
            return {
                ...state,
                category_id: action.category_id,
            };
        case TYPE.START_LOADMORE:
            return {
                ...state,
                loadmore: true,
            };
        case TYPE.FINISH_LOADMORE:
            state.catalogs[action.category_id] = {
                data:
                    action.payload !== null
                        ? state.catalogs[action.category_id].data.concat(action.payload)
                        : state.catalogs[action.category_id].data,
                total: action.total,
                page: action.page !== -1 ? action.page : state.page,
            };
            return {
                ...state,
                loadmore: false,
            };
        case TYPE.SHOW_CATALOG:
            return {
                ...state,
                catalog: action.payload,
            };
        case TYPE.RESET_CATALOG:
            return {
                ...INITIAL_STATE,
                catalogs: {},
            };
        default:
            return state;
    }
}

export function persister({ categories, catalogs }) {
    return {
        categories,
        catalogs,
    };
}
