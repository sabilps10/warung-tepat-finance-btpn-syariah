import React, { Component } from 'react';
import { Image, ScrollView, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { COLOR, FONT_SIZE, PADDING, NUNITO, MARKOT } from '../../common/styles';
import Countly from 'countly-sdk-react-native-bridge';
import { FlatList } from 'react-navigation';

import CategoryHeader from './com/category_header';
class CategoryScreen extends Component {
    navigateToCatalog(category) {
        const event = { eventName: 'Categories', eventCount: 1 };

        event.segments = {
            Name: category.name,
        };

        Countly.sendEvent(event);

        setTimeout(() => {
            this.props.navigation.navigate('Catalog', {
                category_id: category.id,
            });
        }, 0);
    }

    _flatKey = (item, index) => index;

    _renderItem = (item) => {
        let temp = item.item.name;
        let result = temp.replace(/\s/g, '\n');

        return (
            <View style={styles.gridItem}>
                <TouchableOpacity onPress={() => this.navigateToCatalog(item.item)}>
                    <View style={styles.iconContainer}>
                        <Image source={{ uri: item.item.icon }} style={styles.iconImage} resizeMode="contain" />
                    </View>
                    <View style={styles.titleContainer}>
                        <Text style={styles.titleText}>{result}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <CategoryHeader navigation={this.props.navigation} />
                <ScrollView>
                    <Text style={styles.itemTitle}>Peluang Usaha</Text>
                    <View>
                        <FlatList
                            data={this.props.categories}
                            keyExtractor={this._flatKey}
                            renderItem={this._renderItem}
                            horizontal={false}
                            numColumns={4}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categories: state.Catalog.categories,
    };
};

export default connect(mapStateToProps)(CategoryScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLOR.white,
    },
    gridItem: {
        width: '25%',
        height: '50%',
    },
    gridItemImage: {
        borderWidth: 1.5,
        borderColor: 'white',
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemTitle: {
        marginTop: 24,
        marginBottom: 16,
        fontFamily: MARKOT.bold,
        fontSize: FONT_SIZE.EXTRA_LARGE,
        color: COLOR.darkBlack,
        marginLeft: 18,
    },
    iconContainer: {
        borderRadius: 50,
        backgroundColor: 'transparent',
        borderColor: COLOR.primary,
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconImage: {
        height: 50,
        width: 50,
    },
    titleContainer: {
        padding: PADDING.PADDING_TITLE,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_EXTRA_SMALL,
        textAlign: 'center',
        color: COLOR.textGray,
        paddingTop: PADDING.PADDING_VERTICAL_CONTAINER,
        fontFamily: NUNITO.regular,
    },
});
