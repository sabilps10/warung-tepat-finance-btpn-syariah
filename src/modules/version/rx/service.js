import VersionRest from '@src/services/rest/rest.version';

const rest = new VersionRest();

export const VersionServiceImplementation = class VersionService {
    checkVersion(data) {
        return rest.postCheckVersion(data);
    }
};

export const VersionService = new VersionServiceImplementation();
