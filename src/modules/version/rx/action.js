import { NavigationActions } from 'react-navigation';

import * as Activity from '@src/services/activity/action';
import { VersionService } from './service';

export const TYPE = {
    CHECK_SUCCESS: 'version::check.success',
    CHECK_FAILED: 'version::check.failed',
    SHOW_MODAL: 'version::modal.toggle',
};

// action check version
export function $checkVersion(data) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Version', 'Check');
        VersionService.checkVersion(data)
            .then((res) => {
                const { update_status, apk_link } = res.data;

                if (update_status === 500003) {
                    dispatch({
                        type: TYPE.CHECK_SUCCESS,
                        payload: apk_link,
                    });
                    dispatch(NavigationActions.navigate({ routeName: 'ForceUpdate' }));
                } else if (update_status === 500002) {
                    dispatch({
                        type: TYPE.CHECK_SUCCESS,
                        payload: apk_link,
                    });
                    this.$toggleModal(true);
                } else {
                    console.log('Latest Version');
                }
            })
            .catch((error) => {
                const { errors } = error;

                dispatch({
                    type: TYPE.CHECK_FAILED,
                    payload: errors,
                });
            })
            .finally((res) => {
                Activity.done(dispatch, 'Version', 'Check');
            });
    };
}

// action toggle modal
export function $toggleModal(data) {
    return (dispatch) => {
        dispatch({ type: TYPE.SHOW_MODAL, payload: data });
    };
}
