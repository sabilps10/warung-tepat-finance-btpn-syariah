import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Easing, Text } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../common/styles';

import Home from './router.tab';
import CartScreen from './cart';
import CheckoutScreen from './cart/checkout';

import VerifyPinScreen from './debit';
import OrderSuccessPage from './debit/screen/orderSuccessPage';
import OrderFailedPage from './debit/screen/orderFailedPage';
import DebitLoadingPage from './debit/screen/debitLoadingPage';

import MurabahahScreen from './murabahah';
import MurabahahLoadingPage from './murabahah/screen/murabahahLoadingPage';

import HelpScreen from './profile/help';
import TermScreen from './profile/term';
import LocationScreen from './profile/location';
import PasswordScreen from './profile/password';
import ProfileUpdateScreen from './profile/profile';
import ProfilePembiayaanScreen from './profile/paylater';
import ViewAkadScreen from './profile/akad';

import VerifyPinSaldoScreen from './link_saldo/index.js';
import ConnectSuccessScreen from './link_saldo/screen/connectSuccessPage';
import ConnectFailedScreen from './link_saldo/screen/connectFailedPage';

import MemberScreen from './member';
import MemberCreateScreen from './member/create';
import MemberUpdateScreen from './member/update';
import CatalogDetailScreen from './catalog/detail';
import CategoryScreen from './catalog/category';
import SearchScreen from './catalog/search';
import GroupBuyingScreen from './group_buying';
import TransactionDetailScreen from './transaction/detail';

import SupplierScreen from './supplier';
import SearchScreenSupplier from './supplier/search';

import NotificationScreen from './notification';
import VerifyPinSaldoCardScreen from './home/section/PinSaldo';
import PaylaterLoadingPage from './profile/com/PaylaterLoadingPage';
import VerifyPinPaylaterCardScreen from './home/section/PinSaldoPaylater';

function navigationOpt(name) {
    if (name === '') {
        return {
            header: null,
        };
    }

    return {
        headerTitle: () => (
            <Text
                style={{
                    color: COLOR.white,
                    fontFamily: SSP.semi_bold,
                    fontSize: FONT_SIZE.LARGE,
                    letterSpacing: 1,
                }}
            >
                {' '}
                {name.toUpperCase()}{' '}
            </Text>
        ),
        headerStyle: {
            backgroundColor: COLOR.primary,
            elevation: 0,
        },
        headerTintColor: COLOR.white,
        headerTitleStyle: {
            color: COLOR.white,
            fontFamily: SSP.semi_bold,
            fontSize: FONT_SIZE.EXTRA_LARGE,
        },
    };
}

const config = {
    animation: 'timing',
    config: {
        duration: 200,
        easing: Easing.out(Easing.poly(2)),
    },
};

const MainStack = createStackNavigator(
    {
        Main: {
            screen: Home,
            navigationOptions: navigationOpt(''),
        },
        CatalogSearch: {
            screen: SearchScreen,
            navigationOptions: navigationOpt(''),
        },
        CatalogDetail: {
            screen: CatalogDetailScreen,
            navigationOptions: navigationOpt(''),
        },
        Category: {
            screen: CategoryScreen,
            navigationOptions: navigationOpt(''),
        },
        Cart: {
            screen: CartScreen,
            navigationOptions: navigationOpt(''),
        },
        Checkout: {
            screen: CheckoutScreen,
            navigationOptions: navigationOpt(''),
        },
        VerifyPin: {
            screen: VerifyPinScreen,
            navigationOptions: navigationOpt('Pembayaran'),
        },
        OrderSuccess: {
            screen: OrderSuccessPage,
            navigationOptions: navigationOpt(''),
        },
        OrderFailed: {
            screen: OrderFailedPage,
            navigationOptions: navigationOpt(''),
        },
        DebitLoading: {
            screen: DebitLoadingPage,
            navigationOptions: navigationOpt(''),
        },
        AkadScreen: {
            screen: MurabahahScreen,
            navigationOptions: navigationOpt('Akad Pembiayaan'),
        },
        ViewAkad: {
            screen: ViewAkadScreen,
            navigationOptions: navigationOpt('View Akad Pembiayaan'),
        },
        MurabahahLoading: {
            screen: MurabahahLoadingPage,
            navigationOptions: navigationOpt(''),
        },
        ProfileLocation: {
            screen: LocationScreen,
            navigationOptions: navigationOpt('Alamat Pengiriman'),
        },
        ProfileHelp: {
            screen: HelpScreen,
            navigationOptions: navigationOpt('Bantuan'),
        },
        ProfileTerm: {
            screen: TermScreen,
            navigationOptions: navigationOpt('Syarat & Ketentuan'),
        },
        ProfileResetPassword: {
            screen: PasswordScreen,
            navigationOptions: navigationOpt('Perbaharui Kata Sandi'),
        },
        ProfileUpdate: {
            screen: ProfileUpdateScreen,
            navigationOptions: navigationOpt('Profile'),
        },
        ProfilePembiayaan: {
            screen: ProfilePembiayaanScreen,
            navigationOptions: navigationOpt(''),
        },
        Member: {
            screen: MemberScreen,
            navigationOptions: navigationOpt('Member'),
        },
        MemberCreate: {
            screen: MemberCreateScreen,
            navigationOptions: navigationOpt('Registrasi Member'),
        },
        MemberUpdate: {
            screen: MemberUpdateScreen,
            navigationOptions: navigationOpt('Perbaharui Member'),
        },
        TransactionDetail: {
            screen: TransactionDetailScreen,
            navigationOptions: navigationOpt('Detail Transaksi'),
        },
        GroupBuying: {
            screen: GroupBuyingScreen,
            navigationOptions: navigationOpt('Pembelian Kolektif'),
        },
        Supplier: {
            screen: SupplierScreen,
            navigationOptions: navigationOpt(''),
        },
        SupplierSearch: {
            screen: SearchScreenSupplier,
            navigationOptions: navigationOpt(''),
        },
        VerifyPinSaldo: {
            screen: VerifyPinSaldoScreen,
            navigationOptions: navigationOpt('Saldo'),
        },
        ConnectSuccess: {
            screen: ConnectSuccessScreen,
            navigationOptions: navigationOpt(''),
        },
        ConnectFailed: {
            screen: ConnectFailedScreen,
            navigationOptions: navigationOpt(''),
        },
        Notification: {
            screen: NotificationScreen,
            navigationOptions: navigationOpt(''),
        },
        VerifyPinSaldoCard: {
            screen: VerifyPinSaldoCardScreen,
            navigationOptions: navigationOpt(''),
        },
        PaylaterLoading: {
            screen: PaylaterLoadingPage,
            navigationOptions: navigationOpt(''),
        },
        VerifyPinPaylaterCard: {
            screen: VerifyPinPaylaterCardScreen,
            navigationOptions: navigationOpt(''),
        },
    },
    {
        lazy: true,
        optimizationsEnabled: false,
        animationEnabled: true,
        mode: 'modal',
        navigationOptions: {
            headerTitleAlign: 'center',
            gestureEnabled: true,
            transitionSpec: {
                open: config,
                close: config,
            },
        },
    },
);

const MainRoute = createAppContainer(MainStack);
export default MainRoute;
