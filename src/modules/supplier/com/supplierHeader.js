import React, { PureComponent } from 'react';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { StyleSheet, TouchableOpacity, View, Text, Dimensions } from 'react-native';
import { HeaderBackButton } from 'react-navigation-stack';
import PropTypes from 'prop-types';
import Icons from '../../../components/custom_icon';

const deviceWidth = Dimensions.get('screen').width;

import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';

class SupplierHeader extends PureComponent {
    toSearch = () => {
        this.props.navigation.push('SupplierSearch');
    };

    toCart = () => {
        this.props.navigation.push('Cart');
    };

    back = () => {
        if (this.props.navigation.state.params.type) {
            this.props.navigation.push('Cart');
        } else {
            this.props.navigation.goBack(null);
        }
    };

    toNotif = () => {
        this.props.navigation.push('Notification');
    };

    render() {
        let { counter, counterMessage } = this.props;

        return (
            <View style={styles.container}>
                {
                    <View style={styles.backBar}>
                        {<HeaderBackButton tintColor={COLOR.secondary} onPress={this.back} />}
                    </View>
                }
                {
                    <View style={styles.searchbar}>
                        <TouchableOpacity activeOpacity={0.5} style={styles.search} onPress={this.toSearch}>
                            <Icon name="search" size={19} color={COLOR.darkGray} />
                            <Text style={styles.textSearch}>&nbsp;Cari ditoko ini ... </Text>
                        </TouchableOpacity>
                    </View>
                }
                <View style={styles.containerIcon}>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.toCart}>
                        <View style={styles.containerCounter}>
                            <Icons name="bag" type="Feather" color={COLOR.secondary} size={20} />
                            <View style={styles.counter}>
                                <Text style={styles.counterText}>{counter !== undefined ? counter.total : 0}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.toNotif}>
                        <View style={styles.containerCounter}>
                            <Icons name="bell-o" color={COLOR.secondary} size={20} />
                            {counterMessage !== 0 ? (
                                <View style={styles.counter}>
                                    <Text style={styles.counterText}>{counterMessage}</Text>
                                </View>
                            ) : null}
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

SupplierHeader.propTypes = {
    navigation: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => {
    return {
        counter: state.Cart.counter,
        counterMessage: state.Notification.countMessage,
    };
};

export default connect(mapStateToProps)(SupplierHeader);

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR.white,
        elevation: 4,
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        width: deviceWidth,
        flexDirection: 'row',
        alignItems: 'center',
    },
    backBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 30,
        marginRight: 10,
    },
    searchbar: {
        justifyContent: 'center',
        width: deviceWidth - 125,
        height: 30,
        marginLeft: 5,
        marginRight: 8,
    },
    search: {
        backgroundColor: COLOR.light,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 4,
        borderRadius: 3,
    },
    textSearch: {
        color: COLOR.darkGray,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    },
    title: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        letterSpacing: 1,
        color: COLOR.white,
    },
    containerIcon: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    containerCounter: { flexDirection: 'row' },
    counter: {
        position: 'relative',
        width: 18,
        height: 15,
        right: 10,
        borderRadius: 50,
        bottom: 10,
        backgroundColor: COLOR.red,
        zIndex: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
    counterText: {
        color: COLOR.white,
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
});
