import React, { Component } from 'react';
import { ActivityIndicator, RefreshControl, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { ListItem, Icon } from 'react-native-elements';
import { View } from 'react-native-animatable';
import { bindActionCreators } from 'redux';
import { FlatList } from 'react-navigation';

import { $load, $loadmore } from './rx/action';
import SupplierHeader from './com/supplierHeader';
import { FLEX, FONT_SIZE, ROBOTO, SSP, COLOR } from '../../common/styles';
import EmptyState from '../../components/empty_state';
import { ProductLoading, ProductThumb } from '../../components/product';

class SupplierIndex extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showTop: false,
            data: [],
            page: 1,
            total: 0,
            is_empty: false,
        };

        this._onRefresh = this._onRefresh.bind(this);
        this._onLoad = this._onLoad.bind(this);
        this._onLoadMore = this._onLoadMore.bind(this);
        this._onScroll = this._onScroll.bind(this);
        this._onGoTop = this._onGoTop.bind(this);
        this._onNavigationFocus = this._onNavigationFocus.bind(this);
    }

    componentDidMount() {
        this.onRender();
    }

    onRender = () => {
        this._onLoad();
    };

    shouldComponentUpdate(nextProps, nextState) {
        // rerender hanya saat data berubah
        if (
            nextState.data === this.state.data &&
            nextState.showTop === this.state.showTop &&
            nextState.is_empty === this.state.is_empty
        ) {
            return false;
        }

        return true;
    }

    _onRefresh() {
        const { supplierCode } = this.props.navigation.state.params;
        this.props.$load(supplierCode).then((res) => {
            if (typeof this.props.suppliers[supplierCode] !== 'undefined') {
                this.setState(this.props.suppliers[supplierCode]);
            } else {
                this.setState({ is_empty: true });
            }
        });
    }

    _onLoad() {
        const { supplierCode } = this.props.navigation.state.params;
        if (this.state.data.length === 0) {
            this.props.$load(supplierCode).then((res) => {
                if (typeof this.props.suppliers[supplierCode] !== 'undefined') {
                    this.setState(this.props.suppliers[supplierCode]);
                } else {
                    this.setState({ is_empty: true });
                }
            });
        }
    }

    _onScroll(event) {
        if (event.nativeEvent.contentOffset.y > 500) {
            this.setState({ showTop: true });
        } else {
            this.setState({ showTop: false });
        }
    }

    _onGoTop() {
        this.listRef.scrollToOffset({
            offset: 0,
            animated: true,
        });
    }

    _onLoadMore() {
        const { supplierCode } = this.props.navigation.state.params;
        if (!this.props.loadmore && this.state.total === 10) {
            this.props.$loadmore(supplierCode, this.state.page + 1).then((res) => {
                if (typeof this.props.suppliers[supplierCode] !== 'undefined') {
                    this.setState(this.props.suppliers[supplierCode]);
                }
            });
        }
    }

    _renderRefresh() {
        return <RefreshControl refreshing={this.props.loading} onRefresh={this._onRefresh} colors={[COLOR.primary]} />;
    }

    _renderFooter() {
        const activityStyle = {
            paddingTop: 20,
            paddingBottom: 20,
        };

        return this.props.loading ? (
            <ActivityIndicator color={COLOR.secondary} style={activityStyle} animating size="large" />
        ) : null;
    }

    _renderToTop() {
        return this.state.showTop ? (
            <TouchableOpacity onPress={this._onGoTop} style={styles.fab}>
                <Icon name="chevron-up" type="feather" size={21} />
            </TouchableOpacity>
        ) : null;
    }

    _flatItem = ({ index, item }) => <ProductThumb key={index} product={item} navigation={this.props.navigation} />;

    _flatKey = (item, index) => index;

    _flatReff = (ref) => (this.listRef = ref);

    _onNavigationFocus() {
        this._onLoad();
    }

    renderSupplierInfo() {
        const { supplierName } = this.props.navigation.state.params;
        return (
            <View style={styles.supplierInfo}>
                <ListItem
                    title={supplierName}
                    // subtitle={<Text style={styles.listSubTitle}></Text>}
                    leftIcon={{
                        type: 'Ionicons',
                        name: 'store',
                        color: COLOR.success,
                        containerStyle: {
                            borderWidth: 1,
                            width: 32,
                            height: 30,
                            borderRadius: 5,
                            borderColor: COLOR.success,
                        },
                    }}
                    titleStyle={styles.listTitle}
                    subtitleStyle={styles.listSubTitle}
                    containerStyle={styles.listContainer}
                />
            </View>
        );
    }

    renderProduct() {
        return (
            <View style={styles.renderProduct}>
                {this.state.is_empty ? (
                    <View>
                        <EmptyState
                            title="Belum ada barang di supplier ini!"
                            image="https://s3.kora.id/app/assets/empty-catalog.png"
                            refreshControl={this._renderRefresh()}
                        />
                    </View>
                ) : (
                    <ProductLoading onReady={this.state.data.length > 0} bgColor="#efefef" animate="fade">
                        <View animation="fadeIn" useNativeDriver>
                            <FlatList
                                scrollEventThrottle={16}
                                onScroll={this._onScroll}
                                refreshControl={this._renderRefresh()}
                                ListFooterComponent={this._renderFooter()}
                                data={this.state.data}
                                keyExtractor={this._flatKey}
                                horizontal={false}
                                numColumns={2}
                                renderItem={this._flatItem}
                                onEndReached={this._onLoadMore}
                                onEndReachedThreshold={1}
                                initialNumToRender={10}
                                showsVerticalScrollIndicator={false}
                                removeClippedSubviews={true}
                                ref={this._flatReff}
                            />
                            {this._renderToTop()}
                        </View>
                    </ProductLoading>
                )}
            </View>
        );
    }

    render() {
        const { supplierName } = this.props.navigation.state.params;
        return (
            <View style={styles.view}>
                <SupplierHeader
                    style={styles.supplierInfo}
                    navigation={this.props.navigation}
                    supplierName={supplierName.toUpperCase()}
                />
                {this.renderSupplierInfo()}
                <View style={styles.product}>
                    <Text style={styles.productText}>PRODUK</Text>
                </View>
                {this.renderProduct()}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        suppliers: state.Supplier.suppliers,
        loadmore: state.Supplier.loadmore,
        loading: state.Activity.page,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $load, $loadmore }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SupplierIndex);

const styles = StyleSheet.create({
    view: {
        flex: FLEX.full,
    },
    header: {
        flex: 1,
    },
    supplierInfo: {
        flex: 2,
        marginBottom: 8,
    },
    listTitle: {
        fontFamily: SSP.bold,
        color: COLOR.black,
        fontSize: FONT_SIZE.MEDIUM,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    listSubTitle: {
        fontFamily: ROBOTO.light,
        color: COLOR.lightGray,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    renderProduct: {
        flex: 20,
    },
    product: {
        backgroundColor: COLOR.background,
        padding: 16,
        marginBottom: 16,
    },
    productText: {
        fontFamily: ROBOTO.light,
        color: COLOR.darkBlack,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
        letterSpacing: 1,
    },
    fab: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 50,
        backgroundColor: COLOR.white,
        borderRadius: 30,
        elevation: 8,
        zIndex: 9999,
    },
    fabIcon: {
        fontSize: 30,
        color: 'black',
    },
});
