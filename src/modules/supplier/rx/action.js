import { SupplierService } from './service';
import * as Activity from '@src/services/activity/action';

export const TYPE = {
    FETCH_CATALOG: 'supplier::fetch.catalog',
    START_LOADMORE: 'supplier::loadmore.start',
    FINISH_LOADMORE: 'supplier::loadmore.finish',
    SHOW_CATALOG: 'supplier::show.catalog',
    CATALOG_SEARCH: 'supplier::search',
    RESET_CATALOG: 'supplier::reset',
};

export function $load(cid) {
    return (dispatch) => {
        return SupplierService.getCatalog(cid)
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_CATALOG,
                    supplier_id: cid,
                    payload: res.data,
                    total: res.total,
                });
                return true;
            })
            .catch((error) => {
                return false;
            });
    };
}

export function $search(query,supplier_id, page) {
    return (dispatch) => {
        Activity.processing(dispatch, 'Search', 'Supplier');
        return SupplierService.search(query,supplier_id, page,)
            .then((res) => {
                dispatch({
                    type: TYPE.CATALOG_SEARCH,
                    payload: res.data,
                    page: page,
                    total: res.total,
                    supplier_id: supplier_id,
                    query: query,
                });

                return res.data;
            })
            .catch((error) => {
                return false;
            })
            .finally((res) => {
                Activity.done(dispatch, 'Search', 'Supplier');

                return res;
            });
    };
}

export function $loadmore(cid, page) {
    return (dispatch) => {
        dispatch({ type: TYPE.START_LOADMORE });

        return SupplierService.getCatalog(cid, page)
            .then((res) => {
                dispatch({
                    type: TYPE.FINISH_LOADMORE,
                    payload: res.data,
                    supplier_id: cid,
                    page: page,
                    total: res.total,
                });
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.FINISH_LOADMORE,
                    payload: null,
                    page: -1,
                });
            });
    };
}
