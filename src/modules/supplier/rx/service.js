import SupplierRest from '../../../services/rest/rest.supplier';

export const service = class SupplierService {
    constructor() {
        this.rest = new SupplierRest();
    }

    getCatalog(cid = 0, page = 1) {
        return this.rest.getCatalog(cid, 0, page, 10);
    }

    showCatalog(id) {
        return this.rest.showCatalog(id);
    }

    search(query,supplier_id, page = 1) {
        return this.rest.search(query,supplier_id, page);
    }
};
export const SupplierService = new service();
