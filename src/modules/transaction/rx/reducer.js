import { TYPE } from './action';

const INITIAL_STATE = {
    errors: {},
    transactions: [],
    detail: {},
    item: {},
    loadmore: false,
    guides: [],
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_TRANS:
            state.transactions[action.status] = {
                data: action.payload,
                total: action.total,
                page: 1,
            };

            return {
                ...state,
            };
        case TYPE.START_LOADMORE:
            return {
                ...state,
                loadmore: true,
            };
        case TYPE.FINISH_LOADMORE:
            state.transactions[action.status] = {
                data:
                    action.payload !== null
                        ? state.transactions[action.status].data.concat(action.payload)
                        : state.transactions[action.status].data,
                total: action.total,
                page: action.page !== -1 ? action.page : state.page,
            };

            return {
                ...state,
                loadmore: false,
            };
        case TYPE.SHOW_TRANS:
            return {
                ...state,
                detail: action.payload,
            };
        case TYPE.SHOW_ITEMS:
            return {
                ...state,
                item: action.payload,
            };
        case TYPE.PROCESS_FAILED:
            return {
                ...state,
                errors: action.payload,
            };
        case TYPE.PROCESS_SUCCESS:
            return INITIAL_STATE;
        case 'logout':
            return INITIAL_STATE;
        case TYPE.PAYMENT_GUIDE:
            state.guides[action.status] = {
                data: action.payload,
            };
            return {
                ...state,
            };
        default:
            return state;
    }
}

export function persister({ transactions }) {
    return {
        transactions,
    };
}
