import TransactionRest from '../../../services/rest/res.transaction';

export const service = class TransService {
    constructor() {
        this.rest = new TransactionRest();
    }

    get(type, page, limit, search) {
        if (type === 'finished') {
            return this.getFinished(page, limit, search);
        } else if (type === 'proceed') {
            return this.getProceed(page, limit, search);
        } else {
            return this.getPending(page, limit, search);
        }
    }

    getPending(page, limit, search) {
        return this.rest.getPending(page, limit, search);
    }

    getProceed(page, limit, search) {
        return this.rest.getProceed(page, limit, search);
    }

    getFinished(page, limit, search) {
        return this.rest.getFinished(page, limit, search);
    }

    getOrders(id) {
        return this.rest.getOrders(id);
    }

    getItems(id) {
        return this.rest.getItems(id);
    }

    showDetail(id) {
        return this.rest.showDetail(id);
    }

    cancel(id, req) {
        let body = {
            void_note: req.note,
        };

        return this.rest.cancel(id, body);
    }

    getGuide(id) {
        return this.rest.getGuide(id);
    }
};

export const TransService = new service();
