import { TransService } from './service';
import * as Activity from '../../../services/activity/action';

export const TYPE = {
    FETCH_TRANS: 'transaction::fetch',
    SHOW_TRANS: 'transaction::show.transaction',
    SHOW_ITEMS: 'transaction::show.items',
    START_LOADMORE: 'transaction::loadmore.start',
    FINISH_LOADMORE: 'transaction::loadmore.finish',
    PROCESS_FAILED: 'transaction::failed.process',
    PROCESS_SUCCESS: 'transaction::success.process',
    PAYMENT_GUIDE: 'transaction::payment.guide',
};

export function $load(status, page = 1, limit = 15, search) {
    return (dispatch) => {
        return TransService.get(status, page, limit, search)
            .then((res) => {
                dispatch({
                    type: TYPE.FETCH_TRANS,
                    status: status,
                    payload: res.data,
                    total: res.total,
                });

                return true;
            })
            .catch((err) => {
                console.log(err);
                return false;
            });
    };
}

export function $loadmore(status, page, limit = 15, search) {
    return (dispatch) => {
        dispatch({ type: TYPE.START_LOADMORE });

        return TransService.get(status, page, limit, search)
            .then((res) => {
                dispatch({
                    type: TYPE.FINISH_LOADMORE,
                    payload: res.data,
                    status: status,
                    page: page,
                    total: res.total,
                });
            })
            .catch((err) => {
                dispatch({
                    type: TYPE.FINISH_LOADMORE,
                    payload: null,
                    page: -1,
                });
                return console.log(err);
            });
    };
}

export function $loadDetail(id) {
    return (dispatch) => {
        return TransService.showDetail(id)
            .then((res) => {
                return res.data;
            })
            .catch((err) => {
                console.log(err);
                return false;
            });
    };
}

export function $loadOrders(id) {
    return (dispatch) => {
        return TransService.getOrders(id)
            .then((res) => {
                return res.data;
            })
            .catch((err) => {
                console.log(err);
                return false;
            });
    };
}

export function $loadItems(id) {
    return (dispatch) => {
        return TransService.getItems(id)
            .then((res) => {
                return res.data;
            })
            .catch((err) => {
                console.log(err);
                return false;
            });
    };
}

export function $cancel(id, data) {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'transaction_cancelled', 'cancelled');

        return TransService.cancel(id, data)
            .then((res) => {
                dispatch({
                    type: TYPE.PROCESS_SUCCESS,
                });

                return true;
            })
            .catch((error) => {
                dispatch({
                    type: TYPE.PROCESS_FAILED,
                    payload: error.errors,
                });

                return false;
            })
            .finally((res) => {
                Activity.componentDone(dispatch, 'transaction_cancelled', 'cancelled');

                return res;
            });
    };
}

export function $loadGuide(id) {
    return (dispatch) => {
        return TransService.getGuide(id)
            .then((res) => {
                return res.data;
            })
            .catch((err) => {
                console.log(err);
                return false;
            });
    };
}
