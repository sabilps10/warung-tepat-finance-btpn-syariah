import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { $loadDetail, $loadOrders } from './rx/action';
import { Clipboard, FlatList, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import { Button, Icon, ListItem } from 'react-native-elements';
import { CurrencyFormatter, Toast } from '../../common/utils';
import Moment from 'react-moment';
import ListOrder from './com/list.order';
import { Grid, Row } from 'react-native-easy-grid';
import TransactionCancelation from './cancel';

class DetailTransactionScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            transaction: this.props.navigation.getParam('transaction'),
            orders: [],
            showDialogCancelation: false,
        };

        this.toggleDialogCancelation = this.toggleDialogCancelation.bind(this);
        this.onCancelled = this.onCancelled.bind(this);
    }

    componentWillMount() {
        this._onLoad();
        this._loadOrder();
    }

    toggleDialogCancelation() {
        this.setState({ showDialogCancelation: !this.state.showDialogCancelation });
    }

    onCancelled() {
        this.props.navigation.navigate('Transaction', {
            reload: true,
        });
    }

    _onLoad() {
        let id = this.props.navigation.getParam('id');

        this.props.$loadDetail(id).then((data) => {
            if (data) {
                this.setState({ transaction: data });
            }
        });
    }

    _loadOrder() {
        let id = this.props.navigation.getParam('id');

        this.props.$loadOrders(id).then((data) => {
            if (data) {
                this.setState({ orders: data });
            }
        });
    }

    copyText(name, value) {
        Clipboard.setString(value.toString(), Toast(name + ' Telah Disalin'));
    }

    _renderTransaction() {
        let { transaction } = this.state;

        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.titleSection}>INFORMASI TRANSAKSI</Text>
                <View style={styles.sectionBodyContainer}>
                    <ListItem
                        title="Tanggal Buat"
                        rightTitle={
                            <Moment element={Text} style={styles.listTitleValue} format="DD/MM/YYYY" utc>
                                {transaction.ordered_at}
                            </Moment>
                        }
                        containerStyle={{ ...styles.listContainer, ...styles.listContainerFirst }}
                        titleStyle={styles.listTitle}
                        rightTitleStyle={styles.listTitleValue}
                    />
                    <ListItem
                        title="Kode Transaksi"
                        rightTitle={transaction.code}
                        rightElement={
                            <Text
                                style={{
                                    ...styles.listTitle,
                                    fontSize: FONT_SIZE.SMALL,
                                    color: COLOR.danger,
                                }}
                            >
                                SALIN
                            </Text>
                        }
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTitle}
                        rightTitleStyle={styles.listTitleValue}
                        onPress={() => this.copyText('Kode Transaksi', transaction.code)}
                        activeOpacity={0.5}
                        underlayColor={'transparent'}
                    />
                    {transaction.payment_status !== 'void' ? (
                        <ListItem
                            title="Total Transaksi"
                            rightTitle={`${CurrencyFormatter(transaction.total_payment)}`}
                            containerStyle={styles.listContainer}
                            titleStyle={styles.listTitle}
                            rightTitleStyle={styles.listTitleValue}
                        />
                    ) : null}

                    <ListItem
                        title="Status"
                        rightElement={
                            <Text
                                style={
                                    transaction.payment_status === 'void'
                                        ? { ...styles.listTitleValue, ...styles.listTitleVoid }
                                        : { ...styles.listTitleValue, ...styles.listTitleFinish }
                                }
                            >
                                {transaction.transaction_status.description}
                            </Text>
                        }
                        containerStyle={styles.listContainer}
                        titleStyle={styles.listTitle}
                    />
                </View>
            </View>
        );
    }

    _renderPayment() {
        let { transaction } = this.state;
        let { bank } = this.state.transaction;

        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.titleSection}>INFORMASI PEMBAYARAN</Text>
                <View style={styles.sectionBodyContainer}>
                    {bank !== undefined && bank !== 'undefined' ? (
                        <View>
                            <ListItem
                                title="Jenis Pembayaran"
                                rightTitle={bank.id === 1 || 2 ? bank.name : 'Transfer'}
                                containerStyle={{ ...styles.listContainer, ...styles.listContainerFirst }}
                                titleStyle={styles.listTitle}
                                rightTitleStyle={{ ...styles.listTitleValue, width: 140, textAlign: 'right' }}
                            />
                        </View>
                    ) : null}

                    {transaction.transaction_status.code === 700001 && bank !== undefined && bank !== 'undefined' ? (
                        bank.id !== 1 ? (
                            <View>
                                <ListItem
                                    title="Bank"
                                    rightTitle={bank.name}
                                    containerStyle={{ ...styles.listContainer, ...styles.listContainerFirst }}
                                    titleStyle={styles.listTitle}
                                    rightTitleStyle={styles.listTitleValue}
                                />
                                <ListItem
                                    title="No. Rekening"
                                    rightTitle={bank.account_number}
                                    rightElement={
                                        <Text
                                            style={{
                                                ...styles.listTitle,
                                                fontSize: FONT_SIZE.SMALL,
                                                color: COLOR.danger,
                                            }}
                                        >
                                            SALIN
                                        </Text>
                                    }
                                    containerStyle={styles.listContainer}
                                    titleStyle={styles.listTitle}
                                    rightTitleStyle={styles.listTitleValue}
                                    onPress={() => this.copyText('Nomor Rekening', bank.account_number)}
                                    activeOpacity={0.5}
                                    underlayColor={'transparent'}
                                />
                                <ListItem
                                    title="Atas Nama"
                                    rightElement={<Text style={styles.listTitleValue}>{bank.account_name}</Text>}
                                    containerStyle={styles.listContainer}
                                    titleStyle={styles.listTitle}
                                />
                                <ListItem
                                    title="Jumlah Tagihan"
                                    rightTitle={`${CurrencyFormatter(transaction.total_payment)}`}
                                    rightElement={
                                        <Text
                                            style={{
                                                ...styles.listTitle,
                                                fontSize: FONT_SIZE.SMALL,
                                                color: COLOR.danger,
                                            }}
                                        >
                                            SALIN
                                        </Text>
                                    }
                                    containerStyle={styles.listContainer}
                                    titleStyle={styles.listTitle}
                                    rightTitleStyle={styles.listTitleValue}
                                    onPress={() => this.copyText('Nominal', transaction.total_payment)}
                                    activeOpacity={0.5}
                                    underlayColor={'transparent'}
                                />
                                <ListItem
                                    title="Batas Pembayaran"
                                    rightTitle={
                                        <Moment
                                            element={Text}
                                            style={styles.listTitleValue}
                                            format="DD/MM/YYYY HH:mm"
                                            utc
                                        >
                                            {transaction.payment_expired_at}
                                        </Moment>
                                    }
                                    containerStyle={styles.listContainer}
                                    titleStyle={styles.listTitle}
                                    rightTitleStyle={styles.listTitleValue}
                                />
                            </View>
                        ) : (
                            <ListItem
                                title="Jumlah Tagihan"
                                rightTitle={`${CurrencyFormatter(transaction.total_payment)}`}
                                containerStyle={styles.listContainer}
                                titleStyle={styles.listTitle}
                                rightTitleStyle={styles.listTitleValue}
                                activeOpacity={0.5}
                                underlayColor={'transparent'}
                            />
                        )
                    ) : (
                        <ListItem
                            title="Tanggal Bayar"
                            rightTitle={
                                <Moment element={Text} style={styles.listTitleValue} format="DD/MM/YYYY" utc>
                                    {transaction.paid_at}
                                </Moment>
                            }
                            containerStyle={{ ...styles.listContainer, ...styles.listContainerFirst }}
                            titleStyle={styles.listTitle}
                        />
                    )}
                </View>
            </View>
        );
    }

    _flatKey = (item, index) => 'id_' + index;

    _flatItem = ({ index, item }) => <ListOrder data={item} navigation={this.props.navigation} />;

    _renderOrders() {
        let { orders } = this.state;

        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.titleSection}>INFORMASI ORDER</Text>
                <View style={styles.sectionBodyContainer}>
                    <FlatList
                        scrollEventThrottle={16}
                        refreshing={true}
                        data={orders}
                        keyExtractor={this._flatKey}
                        renderItem={this._flatItem}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </View>
        );
    }

    _renderRefresh() {
        return (
            <RefreshControl refreshing={this.props.loading} onRefresh={() => this._onLoad()} colors={[COLOR.primary]} />
        );
    }

    _renderShipment() {
        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.titleSection}>INFORMASI PENGIRIMAN</Text>
                <View style={styles.sectionBodyContainer} />
            </View>
        );
    }

    _renderFooter() {
        const newWidth = { width: '100%' };
        let { transaction } = this.state;

        return (
            <Row style={styles.footerContainer}>
                {transaction.transaction_status.code === 700001 ? (
                    <Grid style={styles.grid}>
                        <Button
                            title="BATALKAN TRANSAKSI"
                            buttonStyle={styles.buttonDanger}
                            titleStyle={styles.buttonText}
                            containerStyle={newWidth}
                            onPress={this.toggleDialogCancelation}
                        />
                    </Grid>
                ) : null}
            </Row>
        );
    }

    _renderDialogCancellation() {
        let { showDialogCancelation } = this.state;

        if (!showDialogCancelation) {
            return null;
        }

        return (
            <TransactionCancelation
                onCancel={this.toggleDialogCancelation}
                onSubmit={this.onCancelled}
                transaction={this.state.transaction}
                visible={true}
            />
        );
    }

    _renderCheckoutCompleted() {
        const { bank } = this.state.transaction;
        const subtitle =
            bank.id === 2
                ? 'Proses Debit akan dilakukan sesuai dengan syarat dan ketentuan yang berlaku.'
                : 'Silakan lakukan pembayaran sesuai dengan informasi pembayaran dibawah.';

        if (typeof this.props.navigation.getParam('checkout') === 'undefined') {
            return null;
        }

        return (
            <View style={styles.completedContainer}>
                <Icon name="check" size={58} reverse color={COLOR.success} />
                <Text style={styles.completedTitle}>Transaksi Telah Terbuat</Text>
                <Text style={styles.completedSubTitle}>{subtitle}</Text>
            </View>
        );
    }

    render() {
        const padding = { paddingBottom: 70 };

        return (
            <View style={styles.wrapper}>
                <ScrollView
                    contentContainerStyle={padding}
                    scrollEventThrottle={1}
                    refreshControl={this._renderRefresh()}
                >
                    {this._renderCheckoutCompleted()}
                    {this._renderTransaction()}
                    {this._renderPayment()}
                    {this._renderOrders()}
                    {this._renderFooter()}
                </ScrollView>
                {this._renderDialogCancellation()}
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.page,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            $loadDetail,
            $loadOrders,
        },
        dispatch,
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailTransactionScreen);

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#f4f4f4',
    },
    completedContainer: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        paddingTop: 50,
        paddingBottom: 30,
        paddingHorizontal: 15,
    },
    completedTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: 28,
    },
    completedSubTitle: {
        textAlign: 'center',
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
    sectionContainer: {
        paddingVertical: PADDING.PADDING_HORIZONTAL_CONTAINER,
        paddingBottom: 0,
    },
    titleSection: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        marginBottom: 5,
    },
    listContainer: {
        borderColor: '#dbdbdb',
        elevation: 0,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        marginTop: 0.5,
        marginBottom: 0.5,
        borderBottomWidth: 0.5,
        paddingVertical: 10,
    },
    listContainerFirst: {
        borderTopWidth: 0.5,
    },
    listTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    listTitleVoid: {
        color: COLOR.danger,
    },
    listTitleFinish: {
        color: COLOR.success,
    },
    listTitleValue: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.primaryDark,
    },
    footerContainer: {
        width: '100%',
        borderTopWidth: 0.7,
        borderColor: COLOR.border,
        paddingHorizontal: 10,
        paddingVertical: 30,
    },
    grid: {
        alignContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 5,
    },
    button: {
        backgroundColor: COLOR.success,
        width: '100%',
    },
    buttonDanger: {
        backgroundColor: COLOR.danger,
        width: '100%',
    },
    buttonGuide: {
        backgroundColor: COLOR.primary,
        flex: 1,
        marginTop: 10,
    },
    buttonText: {
        fontFamily: SSP.bold,
        fontSize: FONT_SIZE.SMALL,
        letterSpacing: 0.5,
    },
});
