import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import {
    Dialog,
    DialogButton,
    DialogContent,
    DialogFooter,
    DialogTitle,
    ScaleAnimation,
} from 'react-native-popup-dialog';

import { COLOR, SSP, FONT_SIZE } from '../../common/styles';
import { bindActionCreators } from 'redux';
import { $cancel } from './rx/action';
import { connect } from 'react-redux';
import { Input } from 'react-native-elements';
import { Toast } from '../../common/utils';

class TransactionCancelation extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            note: '',
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit() {
        let body = {
            note: this.state.note,
        };
        this.props.$cancel(this.props.transaction.id, body)
            .then(res => {
                if (res) {
                    Toast('Transaksi Telah Dibatalkan');
                    this.props.onSubmit();
                }
            });
    }

    _renderFooter() {
        return (
            <DialogFooter>
                <DialogButton
                    text="TUTUP"
                    onPress={this.props.onCancel}
                    style={styles.button}
                    key="button-1"
                    textStyle={{
                        ...styles.textButton,
                        color: COLOR.danger,
                    }}
                />
                <DialogButton
                    text="BATALKAN TRANSAKSI"
                    disabled={this.state.note == '' || this.props.loading}
                    onPress={this.onSubmit}
                    style={styles.button}
                    key="button-2"
                    textStyle={styles.textButton}
                />
            </DialogFooter>
        );
    }

    render() {
        return (
            <Dialog visible={this.props.visible} onTouchOutside={this.props.onCancel}
                    footer={this._renderFooter()}
                    dialogAnimation={new ScaleAnimation()}
                    width={0.8}
                    dialogTitle={
                        <DialogTitle
                            title='Verifikasi Order'
                            hasTitleBar={true}
                            style={styles.titleContainer}
                            textStyle={styles.textTitle}
                        />
                    }>
                <DialogContent style={styles.dialogContainer}>
                    <View style={styles.warningSection}>
                        <Text style={styles.textContent}>Anda yakin akan membatalkan transaksi
                            ini?</Text>
                        <Text style={styles.textSubtitle}>Semua order akan terhapus apabila Anda
                            membatalkan transaksi.</Text>
                    </View>


                    <Text style={styles.textContent}>Alasan Pembatalan Transaksi: </Text>
                    <Input returnKeyType='done'
                           inputStyle={styles.input}
                           errorStyle={styles.inputError}
                           onChangeText={(note) => this.setState({ note })}
                           errorMessage={this.props.errors.void_note}/>

                </DialogContent>
            </Dialog>
        );
    }
}

TransactionCancelation.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    transaction: PropTypes.object.isRequired,
    visible: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Activity.components.transaction_cancelled,
        errors: state.Transaction.errors,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({
        $cancel,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionCancelation);


const styles = StyleSheet.create({
    titleContainer: {
        paddingVertical: 10,
    },
    warningSection: {
        marginBottom: 20,
        backgroundColor: '#f4f4f4',
        padding: 10,
    },
    textSubtitle: {
        textAlign: 'center',
        fontSize: FONT_SIZE.TINY,
    },
    input: {
        borderColor: COLOR.darkGray,
        borderBottomWidth: 0.8,
        textAlign: 'center',
    },
    inputError: {
        textAlign: 'center',
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: { paddingVertical: 15 },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.EXTRA_SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
});
