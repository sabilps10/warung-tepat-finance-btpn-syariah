import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CurrencyFormatter } from '../../../common/utils';
import { ListItem } from 'react-native-elements';
import { StyleSheet, Text } from 'react-native';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../../common/styles';
import Moment from 'react-moment';

export default class ListCom extends Component {
    navigate = () => {
        let { navigation, data } = this.props;

        setTimeout(() => {
            navigation.navigate('TransactionDetail', {
                id: data.id,
                transaction: data,
            });
        }, 0);
    };

    render() {
        let { data } = this.props;

        return (
            <ListItem
                title={data.code}
                subtitle={
                    <Moment element={Text} style={styles.subtitle} format="DD/MM/YYYY" utc>
                        {data.ordered_at}
                    </Moment>
                }
                rightTitle={`${CurrencyFormatter(data.total_payment)}`}
                rightSubtitle={data.total_kk + ' ORDER MEMBER'}
                activeOpacity={0.5}
                underlayColor={'transparent'}
                onPress={this.navigate}
                leftIcon={{
                    type: 'Ionicons',
                    name: 'check-circle',
                    color: data.payment_status === 'void' ? '#dbdbdb' : COLOR.success,
                }}
                containerStyle={styles.container}
                titleStyle={styles.title}
                subtitleStyle={styles.subtitle}
                rightTitleStyle={styles.title}
                rightSubtitleStyle={styles.subtitle}
                chevron
            />
        );
    }
}

ListCom.propTypes = {
    data: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 15,
        borderColor: '#dbdbdb',
        elevation: 0,
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        marginTop: 2,
        marginBottom: 2,
        borderBottomWidth: 0.5,
    },
    subtitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        color: COLOR.darkGray,
    },
    title: {
        textAlign: 'left',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.primaryDark,
    },
});
