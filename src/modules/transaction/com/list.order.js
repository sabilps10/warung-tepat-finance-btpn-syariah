import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CurrencyFormatter } from '../../../common/utils';
import { ListItem } from 'react-native-elements';
import { LayoutAnimation, Platform, StyleSheet, Text, UIManager, View } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';

export default class ListOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: true,
        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    toggle = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ collapsed: !this.state.collapsed });
    };

    navigate = () => {
        let { navigation, data } = this.props;

        setTimeout(() => {
            navigation.navigate('TransactionDetail', {
                id: data.id,
                transaction: data,
            });
        }, 0);
    };

    navigateCatalog(catalog) {
        setTimeout(() => {
            this.props.navigation.navigate('CatalogDetail', {
                catalog_id: catalog.id,
                product: catalog,
            });
        }, 0);
    };

    _renderItem() {
        if (this.state.collapsed) return null;

        let { data } = this.props;

        return (
            <View>
                {
                    data.items.map((item, i) => {
                        return <ListItem
                            key={i}
                            title={item.catalog.name}
                            subtitle={
                                <Text style={styles.productSubtitle}>
                                    {item.catalog.item ? item.catalog.item.size != '' ? item.catalog.item.size : null : null}
                                    {item.catalog.item ? item.catalog.item.unit != '' ? ' / ' + item.catalog.item.unit : null : null}
                                    {item.catalog.max_monthly != 0 ?
                                        <Text>
                                            &nbsp;(Max: {item.catalog.max_monthly})
                                        </Text>
                                        : null
                                    }
                                    {' @ ' + `${CurrencyFormatter(item.unit_price)}`}
                                </Text>
                            }
                            badge={{
                                value: item.quantity,
                                textStyle: {
                                    color: 'white',
                                    fontWeight: 'bold',
                                },
                                badgeStyle: {
                                    backgroundColor: '#888',
                                },
                            }}
                            rightElement={
                                <Text
                                    style={styles.productRightTitle}>{`${CurrencyFormatter(item.subtotal)}`}</Text>
                            }
                            containerStyle={styles.productList}
                            titleStyle={styles.productTitle}
                            onPress={() => this.navigateCatalog(item.catalog)}
                            activeOpacity={0.7}
                            underlayColor={'transparent'}
                        />;
                    })
                }
            </View>
        );
    }

    _renderDiscount() {
        let { data } = this.props;

        if (data.discount == 0) return null;

        return (
            <ListItem
                title="Discount"
                rightTitle={`${CurrencyFormatter(data.discount)}`}
                containerStyle={styles.productList}
                titleStyle={styles.productTitle}
                rightTitleStyle={{ ...styles.productRightTitle, color: COLOR.danger }}
            />
        );
    }

    render() {
        let { data } = this.props;

        return (
            <View style={{ marginBottom: 1 }}>
                <ListItem
                    title={data.kk.name.toUpperCase()}
                    subtitle={data.code}
                    rightTitle={`${CurrencyFormatter(data.total)}`}
                    activeOpacity={0.7}
                    underlayColor={'transparent'}
                    containerStyle={styles.container}
                    titleStyle={styles.title}
                    subtitleStyle={styles.subtitle}
                    rightTitleStyle={styles.rightTitle}
                    onPress={this.toggle}
                    leftIcon={
                        {
                            type: 'Ionicons',
                            name: 'add',
                            color: data.is_verified == 1 ? COLOR.success : COLOR.border,
                        }
                    }
                />

                <View style={{
                    height: !this.state.collapsed ? null : 0,
                    overflow: 'hidden',
                    backgroundColor: '#fff',
                }}>
                    {this._renderItem()}
                    {this._renderDiscount()}
                </View>
            </View>
        );
    }
}

ListOrder.propTypes = {
    data: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 7,
        borderColor: '#dbdbdb',
        elevation: 0,
        marginBottom: 0,
        borderBottomWidth: 0,
        borderTopWidth: 0.5,
    },
    subtitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        color: COLOR.darkGray,
    },
    title: {
        textAlign: 'left',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    rightTitle: {
        textAlign: 'left',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.primaryDark,
    },
    productList: {
        paddingVertical: 5,
        borderTopWidth: 0.5,
        borderColor: '#dbdbdb',
        backgroundColor: 'transparent',
    },
    productTitle: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_SMALL,
    },
    productRightTitle: {
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
        fontSize: FONT_SIZE.MEDIUM,
        fontFamily: SSP.bold,
    },
    productSubtitle: {
        fontFamily: SSP.light,
        color: COLOR.black,
        fontSize: FONT_SIZE.EXTRA_SMALL,
        lineHeight: FONT_SIZE.LINE_HEIGHT_MEDIUM,
    },
});
