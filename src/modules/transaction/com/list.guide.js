import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListItem, Icon} from 'react-native-elements';
import { LayoutAnimation, Platform, StyleSheet, Text, UIManager, View } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../../../common/styles';

export default class ListGuide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: true,
        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    toggle = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ collapsed: !this.state.collapsed });
    };

    _renderItem() {
        if (this.state.collapsed) return null;

        let {data, pg} = this.props;

        return (
            <View style={styles.containerSteps}>
                {
                    data.steps.map((item, i) => {

                        if (pg == 1){
                            return <ListItem
                                key={i}
                                title={item.desc}
                                containerStyle={styles.stepContainer}
                                titleStyle={styles.stepTitle}
                                leftElement={
                                    <View style={styles.badge}>
                                        <Text style={styles.badgeTitle}>{i+1}</Text>
                                    </View>
                                }
                            />;
                        } else {
                            return <ListItem
                                key={i}
                                title={item.desc}
                                containerStyle={styles.stepContainerNote}
                                titleStyle={styles.stepTitle}
                            />;
                        }
                        
                    })
                }
            </View>
        );
    }

    render() {
        let {data} = this.props;

        let chevron

        if (this.state.collapsed){
            chevron = <Icon name='chevron-down' size={16} color="#000" type='feather'/>
        }else {
            chevron = <Icon name='chevron-up' size={16} color="#000" type='feather'/>
        }
        
        return (
            <View style={{ marginBottom: 1 }}>
                <ListItem
                    title={data.title}
                    activeOpacity={0.7}
                    underlayColor={'transparent'}
                    containerStyle={styles.container}
                    titleStyle={styles.title}
                    onPress={this.toggle}
                    rightElement={chevron}
                />
                <View style={{
                    height: !this.state.collapsed ? null : 0,
                    overflow: 'hidden',
                    backgroundColor: '#fff',
                }}>
                    {this._renderItem()}
                </View>
            </View>
        );
    }
}

ListGuide.propTypes = {
    data: PropTypes.object.isRequired,
    pg: PropTypes.number.isRequired
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 15,
        borderTopColor: '#dbdbdb',
        elevation: 0,
        marginBottom: 0,
        borderTopWidth: 0.5,
    },
    containerSteps:{
        paddingVertical: 10,
        borderTopColor: '#dbdbdb',
        elevation: 0,
        marginBottom: 0,
        borderTopWidth: 0.5,
    },
    title: {
        textAlign: 'left',
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
    },
    stepTitle: {
        textAlign: 'left',
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.MEDIUM,
    },
    stepContainerFirst:{
        paddingVertical: 5,
        borderTopColor: '#dbdbdb',
        elevation: 0,
        marginBottom: 0,
        borderTopWidth: 0.5,
    },
    stepContainer: {
        paddingVertical: 5
    },
    stepContainerLast: {
        paddingTop: 5,
        paddingBottom: 15
    },
    stepContainerNote:{
        paddingTop: 15,
        paddingBottom: 5
    },
    badge:{
        backgroundColor: '#dbdbdb',
        padding: 5,
        height: 20,
        width: 20,
        alignContent: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    badgeTitle:{
        textAlign: 'center',
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    }
});
