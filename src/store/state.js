/**
 * State
 *
 * This module is for state definition
 */

import * as Activity from '../services/activity/reducer';
import * as Router from '../services/navigation/reducer';
import * as Region from '../services/region/reducer';

import * as Auth from '../modules/auth/rx/reducer';
import * as Version from '../modules/version/rx/reducer';
import * as Home from '../modules/home/rx/reducer';
import * as Catalog from '../modules/catalog/rx/reducer';
import * as Profile from '../modules/profile/rx/reducer';
import * as Member from '../modules/member/rx/reducer';
import * as Transaction from '../modules/transaction/rx/reducer';
import * as Cart from '../modules/cart/rx/reducer';
import * as GroupBuying from '../modules/group_buying/rx/reducer';
import * as Supplier from '../modules/supplier/rx/reducer';
import * as Debit from '../modules/debit/rx/reducer';
import * as LinkSaldo from '../modules/link_saldo/rx/reducer';
import * as Notification from '../modules/notification/rx/reducer';
import * as Murabahah from '../modules/murabahah/rx/reducer';

const $state = {
    Activity,
    Router,
    Region,
    Auth,
    Version,
    Home,
    Catalog,
    Profile,
    Member,
    Transaction,
    Cart,
    GroupBuying,
    Supplier,
    Debit,
    LinkSaldo,
    Notification,
    Murabahah,
};

export default $state;
