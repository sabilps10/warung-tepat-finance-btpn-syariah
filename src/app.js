import React, { PureComponent } from 'react';
import { connect, Provider } from 'react-redux';
import { StatusBar } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { createReduxContainer } from 'react-navigation-redux-helpers';
import { Root } from 'native-base';
import Countly from 'countly-sdk-react-native-bridge';
import { NotifierWrapper } from 'react-native-notifier';

import { COLOR } from './common/styles';
import { StackNavigator } from './router.main';
import { setupStore } from './store';
import { Config } from './common/config';

export const store = setupStore();

const navigator = createReduxContainer(StackNavigator);
const mapStateToProps = (state) => ({
    state: state.Router,
});

const AppNavigator = connect(mapStateToProps)(navigator);

export default class App extends PureComponent {
    componentDidMount() {
        SplashScreen.hide();
        StatusBar.setHidden(false, true);

        const envi = global.ENVI;
        // modify serverURL to your own.
        const serverURL = 'https://countly-ce.apps.btpnsyariah.com';
        const deviceId = null; // or use some string that identifies current app user

        // Enable logging, disable if apps in production env.
        // Countly.enableLogging();

        // Using countly crash reports
        Countly.enableCrashReporting();

        // initialize Countly
        Countly.init(serverURL, Config.appKey[envi], deviceId);

        // start session tracking
        Countly.start();
    }

    render() {
        return (
            <Provider store={store}>
                <StatusBar backgroundColor={COLOR.primary} barStyle="light-content" />
                <Root>
                    <NotifierWrapper>
                        <AppNavigator />
                    </NotifierWrapper>
                </Root>
            </Provider>
        );
    }
}
