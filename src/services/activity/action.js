import { createLogger } from '@common/logger';
import { EventEmitter } from '@common/events';

const events = new EventEmitter();
const Logger = createLogger('ACTIVITY');

export const TYPE = {
    PAGE_PROCESSING: 'activity::page.processing',
    PAGE_DONE: 'activity::page.done',
    COMPONENT_PROCESSING: 'activity::component.processing',
    COMPONENT_DONE: 'activity::component.done',
};

/**
 * Action indicate that page is processing
 *
 * @param dispatcher
 * @param screen
 * @param operation
 */
export function processing(dispatcher, screen = 'App', operation = 'default') {
    events.emit('started', `${screen}.${operation}`);
    dispatcher({ type: TYPE.PAGE_PROCESSING });
}

/**
 * Action that indicate page is done processing
 *
 * @param dispatcher
 * @param screen
 * @param operation
 */
export function done(dispatcher, screen = 'App', operation = 'default') {
    events.emit('finished', `${screen}.${operation}`);
    dispatcher({ type: TYPE.PAGE_DONE });
}

/**
 * Action indicate that some component is processing
 *
 * @param dispatcher
 * @param component
 */
export function componentProcessing(dispatcher, component = 'Default') {
    dispatcher({
        type: TYPE.COMPONENT_PROCESSING,
        name: component,
    });
}

/**
 * Action indicate that component is done processing
 *
 * @param dispatcher
 * @param component
 */
export function componentDone(dispatcher, component = 'Default') {
    dispatcher({
        type: TYPE.COMPONENT_DONE,
        name: component,
    });
}

if (process.env.NODE_ENV === 'development') {
    events.on('started', (screen) => {
        Logger.info('@processing', screen);
    });
    events.on('finished', (screen) => {
        Logger.info('@done', screen);
    });
}
