import * as Activity from '../activity/action';
import { RegionService } from './service';

export const TYPE = {
    FETCH_REGENCY: 'region::fetch.regency',
    FETCH_DISTRICT: 'region::fetch.district',
    FETCH_VILLAGE: 'region::fetch.village',
};

export function $loadRegency(page = 1, limit = 15, search = '') {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'regency');

        return RegionService.getRegency(page, limit, search)
            .then(res => {
                dispatch(
                    {
                        type: TYPE.FETCH_REGENCY,
                        payload: res.data,
                    },
                );

                return true;
            })
            .catch(err => {
                return false;
            })
            .finally(res => {
                Activity.componentDone(dispatch, 'regency');

                return res;
            });
    };
}

export function $loadDistrict(regency_id, page = 1, limit = 15, search = '') {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'district');

        return RegionService.getDistrict(regency_id, page, limit, search)
            .then(res => {
                dispatch(
                    {
                        type: TYPE.FETCH_DISTRICT,
                        payload: res.data,
                    },
                );

                return true;
            })
            .catch(err => {
                return false;
            })
            .finally(res => {
                Activity.componentDone(dispatch, 'district');

                return res;
            });
    };
}

export function $loadVillage(district_id, page = 1, limit = 15, search = '') {
    return (dispatch) => {
        Activity.componentProcessing(dispatch, 'district');

        return RegionService.getVillage(district_id, page, limit, search)
            .then(res => {
                dispatch(
                    {
                        type: TYPE.FETCH_VILLAGE,
                        payload: res.data,
                    },
                );

                return true;
            })
            .catch(err => {
                return false;
            })
            .finally(res => {
                Activity.componentDone(dispatch, 'district');

                return res;
            });
    };
}
