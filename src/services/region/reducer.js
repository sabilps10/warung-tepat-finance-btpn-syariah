import { TYPE } from './action';

const INITIAL_STATE = {
    regencies: [],
    districts: [],
    villages: [],
};

export function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case TYPE.FETCH_REGENCY:
            return {
                ...state,
                regencies: action.payload,
            };
        case TYPE.FETCH_DISTRICT:
            return {
                ...state,
                districts: action.payload,
            };
        case TYPE.FETCH_VILLAGE:
            return {
                ...state,
                villages: action.payload,
            };
        default:
            return state;
    }
}
