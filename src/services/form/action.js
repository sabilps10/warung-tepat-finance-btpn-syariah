export const TYPE = {
    RESET_ERROR: 'form::reset',
};

export function $reset() {
    return (dispatch) => {
        dispatch({
            type: TYPE.RESET_ERROR,
        });
    };
}
