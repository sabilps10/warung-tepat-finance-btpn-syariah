import Rest from './rest';

export default class NotificationRest extends Rest {
    postRegister(body) {
        return this.POST('notif/device', body);
    }

    getAllMessage() {
        return this.GET('notif/message');
    }

    getCountMessage() {
        return this.GET('notif/message/count');
    }

    postRead(id) {
        return this.GET('notif/message/' + id);
    }

    postAllRead() {
        return this.PUT('notif/message');
    }
}
