import Rest from './rest';

export default class MemberRest extends Rest {
    //mendapatkan semua list kk
    get(page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
            order_by: 'name',
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('member', qs);
    }

    //mendapatkan detail kk
    show(id) {
        return this.GET('member/' + id);
    }

    //mengubah data kk
    update(id, data) {
        return this.PUT('member/' + id, data);
    }

    // menghapus kk
    delete(id) {
        return this.DELETE('member/' + id);
    }

    //buat kk baru
    create(data) {
        return this.POST('member', data);
    }
}
