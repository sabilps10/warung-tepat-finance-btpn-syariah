import Rest from './rest';

export default class DebitRest extends Rest {
    // Verify pin
    postVerifyPin(body) {
        return this.POST('esb/verify/transfer', body);
    }
}
