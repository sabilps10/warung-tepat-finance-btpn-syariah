import Rest from './rest';

export default class ProfileRest extends Rest {
    getLocation() {
        return this.GET('profile/address');
    }

    //lokasi
    saveLocation(data) {
        return this.PUT('profile/location', data);
    }

    putPassword(data) {
        return this.PUT('profile/password', data);
    }

    //foto
    putPicture(name, source) {
        return this.PUT('profile/picture', {
            name: name,
            image: source,
            scale: 300,
        });
    }

    getPaylaterBalance() {
        return this.GET('profile/balance/paylater');
    }

    getPaylaterHistory() {
        return this.GET('facility/enquiry-installment');
    }

    getAkadDetail(id) {
        return this.GET('facility/akad-pembiayaan/' + id);
    }

    getverifySaldoPin() {
        return this.GET('esb/balance');
    }

    postVerifyPinSaldo(body) {
        return this.POST('esb/connect', body);
    }
}
