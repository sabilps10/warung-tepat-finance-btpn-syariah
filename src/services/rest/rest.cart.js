import Rest from './rest';

export default class CartRest extends Rest {
    get() {
        return this.GET('cart');
    }

    checkCart(data) {
        return this.POST('cart/check', data);
    }

    getSummary() {
        return this.GET('cart/summary');
    }

    getCounter() {
        return this.GET('cart/counter');
    }

    getByCatalog(id) {
        return this.GET('cart/catalog/' + id);
    }

    getPaymentMethod() {
        return this.GET('cart/payment-methodv2');
    }

    show(id) {
        return this.GET('cart/' + id);
    }

    delete(id) {
        return this.DELETE('cart/' + id);
    }

    process(kkid, data) {
        return this.POST('cart/' + kkid, data);
    }

    finish(id) {
        let body = {
            id: id,
        };

        return this.POST('cart/' + id + '/finish', body);
    }

    checkout(data) {
        return this.POST('cart/checkoutv2', data);
    }

    verifyOrder(data) {
        return this.POST('cart/verify-order', data);
    }

    verify(data) {
        return this.POST('cart/verify', data);
    }
}
