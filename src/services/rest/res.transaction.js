import Rest from './rest';

export default class TransactionRest extends Rest {
    getPending(page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('transaction/pending', qs);
    }

    getProceed(page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('transaction/proceed', qs);
    }

    getFinished(page, limit, search) {
        let qs = {
            page: page,
            limit: limit,
        };

        if (search != '') {
            qs.search = search;
        }

        return this.GET('transaction/history', qs);
    }

    getOrders(id) {
        return this.GET('transaction/' + id + '/orders');
    }

    getItems(id) {
        return this.GET('transaction/' + id + '/items');
    }

    showDetail(id) {
        return this.GET('transaction/' + id);
    }

    cancel(id, body) {
        return this.PUT('transaction/' + id + '/cancel', body);
    }

    getGuide(id) {
        return this.GET('transaction/' + id + '/payment-method');
    }
}
