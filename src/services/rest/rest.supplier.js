import Rest from './rest';

export default class SupplierRest extends Rest {
    // mendapatkan data catalog berdasarkan supplier_id
    getCatalog(cid = 0, is_featured = 0, page = 1, limit = 10) {
        let qs = {
            page: page,
            limit: limit,
            order_by: '-id',
        };

        if (cid > 0) {
            qs.supplier_id = cid;
        }

        if (is_featured > 0) {
            qs.is_featured = 1;
        }

        return this.GET('catalog', qs);
    }

    // mendapatkan data detail catalog berdasarkan id catalog
    showCatalog(id) {
        return this.GET('catalog/' + id);
    }

    getRelated(id) {
        return this.GET('catalog/' + id + '/related');
    }

    //search keyword & suplier_id
    search(query, supplier_id, page = 1, limit = 10) {
        let qs = {
            page: page,
            limit: limit,
            search: query,
            supplier_id: supplier_id,
        };
        if (supplier_id > 0) {
            qs.supplier_id = supplier_id;
        }

        return this.GET('catalog/search', qs);
    }
}
