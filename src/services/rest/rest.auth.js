import Rest from './rest';

export default class AuthRest extends Rest {
    // Request login
    postLogin(body) {
        return this.POST('auth/signin', body);
    }

    // Username check
    postCheck(body) {
        return this.POST('auth/check', body);
    }

    // Request OTP code
    postRequestOTP(body) {
        return this.POST('auth/otp', body);
    }

    // Request verify OTP from user
    postVerifyOTP(body) {
        return this.POST('auth/verifyotp', body);
    }

    // Request New Password
    postNewPassword(body) {
        return this.POST('auth/password', body);
    }

    // Get user data
    getMe() {
        return this.GET('auth/me');
    }

    postRegister(body) {
        return this.POST('notif/device', body);
    }
}
