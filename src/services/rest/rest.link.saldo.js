import Rest from './rest';

export default class ConnectSaldoRest extends Rest {
    // Verify user
    getVerifyUser(body) {
        return this.GET('esb/balance', body);
    }

    // Connect user
    postConnectUser(body) {
        return this.POST('esb/connect', body);
    }
}
