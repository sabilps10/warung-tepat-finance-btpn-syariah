import Rest from './rest';

export default class VersionRest extends Rest {
    // Version check
    postCheckVersion(body) {
        return this.POST('version', body);
    }
}
