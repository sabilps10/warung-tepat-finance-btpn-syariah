import Rest from './rest';

export default class CatalogRest extends Rest {
    // mendapatkan data category dengan parent_id
    getCategory(pid) {
        return this.GET('catalog/category', { parent_id: pid });
    }

    // mendapatkan data catalog berdasarkan category_id
    getCatalog(cid = 0, is_featured = 0, page = 1, limit = 10) {
        let qs = {
            page: page,
            limit: limit,
            order_by: '-id',
        };

        if (cid > 0) {
            qs.category_id = cid;
        }

        if (is_featured > 0) {
            qs.is_featured = 1;
        }

        return this.GET('catalog', qs);
    }

    // mendapatkan data detail catalog berdasarkan id catalog
    showCatalog(id) {
        return this.GET('catalog/' + id);
    }

    // mendapatkan data slider
    getSlider() {
        return this.GET('catalog/slider');
    }

    getRelated(id) {
        return this.GET('catalog/' + id + '/related');
    }

    search(query, page = 1, limit = 10) {
        let qs = {
            page: page,
            limit: limit,
            search: query,
        };

        return this.GET('catalog/search-catalog', qs);
    }
}
