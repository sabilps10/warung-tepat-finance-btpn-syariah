import { NavigationActions } from 'react-navigation';
import { StackNavigator } from '../../router.main';
import { TYPE } from '../../modules/auth/rx/action';

const INITIAL_STATE = StackNavigator.router.getStateForAction(StackNavigator.router.getActionForPathAndParams('Boot'));

/**
 * Reducer Navigation
 *
 * @param state
 * @param action
 * @returns {*}
 */
export function reducer(state = INITIAL_STATE, action) {
    let NEXT_STATE = null;
    switch (action.type) {
        case TYPE.LOGIN_SUCCESS:
            NEXT_STATE = StackNavigator.router.getStateForAction(
                NavigationActions.navigate({
                    routeName: 'Main',
                }),
                state,
            );
            break;
        case TYPE.LOGOUT:
            NEXT_STATE = StackNavigator.router.getStateForAction(
                NavigationActions.navigate({
                    routeName: 'Login',
                }),
                state,
            );
            break;
        case TYPE.LOGIN_EFORM:
            NEXT_STATE = StackNavigator.router.getStateForAction(
                NavigationActions.navigate({
                    routeName: 'Eform',
                }),
                state,
            );
            break;
        default:
           NEXT_STATE = StackNavigator.router.getStateForAction(action, state);
    }

    return NEXT_STATE || INITIAL_STATE;
}
