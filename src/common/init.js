import './events';

import * as Logger from './logger';

if (process.env.NODE_ENV === 'development') {
    global.Logger = Logger;
}

/**
 * Setup Logger
 */

const PREFIX = 'Warung Tepat';
Logger.setup(PREFIX);

if (process.env.NODE_ENV === 'development') {
    Logger.enable(`${PREFIX}*`);
}
