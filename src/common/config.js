import { VersionBuild, VersionName } from './release';

// global.ENVI = 'dirty';
global.ENVI = 'alpha';
// global.ENVI = 'beta';
// global.ENVI = 'prod';

global.API_KEY = '246eb17a0b8524853c8668f00b357832';

export const Config = {
    // Countly
    appKey: {
        dirty: 'ab6e064aaf90de56c7962781baaed1216c33ff38',
        alpha: '92945a51485be423ab82d78b02c040fab3275974',
        beta: '76a42d78d196c3858f3f864396922471e880c9ad',
        prod: '03a870b1a7da324ff128efe2c98412cb650db9b0',
    },
    // Derawan
    firebaseConfig: {
        dirty: firebaseConfigDev,
        alpha: firebaseConfigDev,
        beta: firebaseConfigDev,
        prod: firebaseConfigProd,
    },
    source: 'https://s3.kora.id/app/app.config.json',
    playStoreURL: 'https://play.google.com/store/apps/details?id=com.btpns.lmd',
    versionName: VersionName,
    versionBuild: VersionBuild,
    forceUpdate: false,
    orderUrl: 'https://lmd-order-app.apps.btpnsyariah.com/',
    apiURL: {
        warungtepat: {
            dirty: 'https://lmd-api-fin-dirty.apps.nww.syariahbtpn.com/v1/',
            alpha: 'https://lmd-api-fin-alpha.apps.nww.syariahbtpn.com/v1/',
            beta: 'https://lmd-api-fin-beta.apps.nww.syariahbtpn.com/v1/',
            prod: 'https://lmd-api.apps.btpnsyariah.com/v1/',
        },
        // Halmahera
        eform: {
            dirty: 'https://ms-eform-cust-dirty.apps.nww.syariahbtpn.com/',
            alpha: 'https://ms-eform-cust-alpha.apps.nww.syariahbtpn.com/',
            beta: 'https://ms-eform-cust-beta.apps.nww.syariahbtpn.com/',
            prod: 'https://api-v2.apps.btpnsyariah.com/cust/ms-eform/',
        },
        storage: {
            dirty: 'https://storage-dirty.apps.nww.syariahbtpn.com/',
            alpha: 'https://storage-alpha.apps.nww.syariahbtpn.com/',
            beta: 'https://storage-beta.apps.nww.syariahbtpn.com/',
            prod: 'https://storage.apps.btpnsyariah.com/',
        },
    },
    apiKey: {
        google_places: 'AIzaSyDOvQMmsbX8qswsKtOlS_svToNngtxCbcM',
    },
    agentDefaultEmail: 'ebcc@mail.btpnsyariah.com',
};

// firebase development config
const firebaseConfigDev = {
    apiKey: 'AIzaSyAZiR68m7N3gMyzA-HCsYuET4IH-lEICRw',
    authDomain: 'notification-b0721.firebaseapp.com',
    projectId: 'notification-b0721',
    storageBucket: 'notification-b0721.appspot.com',
    messagingSenderId: '696861378193',
    appId: '1:696861378193:web:67f9b114ff88ad1600e130',
    measurementId: 'G-N5GMWR5XGV',
};

// firebase production config
const firebaseConfigProd = {
    apiKey: 'AIzaSyDUG07HHwsWr8wL7xtnIZa83g8Z50Q94IM',
    authDomain: 'notifiation-production.firebaseapp.com',
    projectId: 'notifiation-production',
    storageBucket: 'notifiation-production.appspot.com',
    messagingSenderId: '346971478895',
    appId: '1:346971478895:web:9fd65e82bc277a8ef1269a',
    measurementId: 'G-33W7NV0BD9',
};
