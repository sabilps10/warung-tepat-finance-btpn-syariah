import { StyleSheet } from 'react-native';

// import { CustomIcon } from '../Shared/components/CustomIcon';

/**
 * Fonts
 * inspect using https://opentype.js.org/font-inspector.html
 * make sure file name matches full name
 */

/**
 * NUNITO
 */
export const NUNITO = {
    regular: 'NunitoSans-Regular',
    bold: 'NunitoSans-Bold',
    extraBold: 'NunitoSans-ExtraBold',
};

/**
 * Lato
 */
export const LATO = {
    regular: 'Lato-Regular',
    bold: 'Lato-Bold',
    light: 'Lato-Light',
};

/**
 * Roboto
 */
export const ROBOTO = {
    regular: 'Roboto-Regular',
    bold: 'Roboto-Bold',
    light: 'Roboto-Light',
};
/**
 * Source Sans Pro
 */
export const SSP = {
    regular: 'SourceSansPro-Regular',
    bold: 'SourceSansPro-Bold',
    light: 'SourceSansPro-Light',
    semi_bold: 'SourceSansPro-SemiBold',
};
/**
 * MarkOT
 */
export const MARKOT = {
    regular: 'MarkOT',
    bold: 'MarkOT-Bold',
    book: 'MarkOT-Book',
};

/**
 * Colors
 */
export const COLOR = {
    // base color
    white: '#fff',
    darkBlack: '#333333',
    black: '#221C44',
    orange: '#f27510',
    red: '#aa0000',
    lightRed: '#ef3636',
    teal: '#1CAF9A',
    cyan: '#5B93D3',
    blue: '#4267B2',
    darkGreen: '#027479',
    green: '#23BF08',
    yellow: '#F49917',
    darkblue: '#0D0D6B',
    lightGray: '#999990',
    darkGray: '#bbb',
    greenLight: '#AEBF0E',
    greenDark: '#7ABB17',
    purple: '#BF6EAD',
    shadow: '#dadada',
    debug: 'rgba(0,0,0,0.1)',

    background: '#F0F1F6',
    backgroundCart: '#ededed',

    // Color
    primary: '#f4831f',
    secondary: '#fdae69',
    tertiary: '#00777a',
    info: '#5B93D3',
    success: '#5cb85c',
    danger: '#d9534f',
    warning: '#F8A601',
    dark: '#454F63',
    light: '#f4f4f4',
    off: '#999999',

    primaryLight: '#fdae69',
    primaryLighter: '#ffe0c4',
    primaryDark: '#c05900',
    primaryDarkest: '#803b00',
    primaryTransparent: '#fef0e5',
    tertiaryLight: '#00c781',
    accent: '#FFC107',
    inverse: '#FAFAFA',

    text: '#040404',
    textSecondary: '#333333',
    textGray: '#555555',

    textInverse: '#FAFAFA',
    textSecondaryInverse: '#CCCCCC',

    progressBarBlue: '#0092e6',
    warningRed: '#ffeeee',
    border: '#dbdbdb',
    searchBorder: '#DAD9DC',
};

export const FLEX = {
    full: 1,
};

export const WIDTH = {
    full: '100%',
};

export const MARGIN = {
    accordion: 64,
};

export const PADDING = {
    PADDING_TITLE: 3,
    PADDING_CARD: 5,
    PADDING_HORIZONTAL_CONTAINER: 15,
    PADDING_VERTICAL_CONTAINER: 10,
};

export const FONT_SIZE = {
    TINY: 9,
    EXTRA_SMALL: 10,
    SMALL: 12,
    MEDIUM: 14,
    LARGE: 16,
    EXTRA_LARGE: 18,
    XLARGE: 24,

    FONT_WEIGHT_LIGHT: '200',
    FONT_WEIGHT_MEDIUM: '300',
    FONT_WEIGHT_BOLD: '500',

    LINE_HEIGHT_TINY: 11,
    LINE_HEIGHT_EXTRA_SMALL: 12,
    LINE_HEIGHT_SMALL: 14,
    LINE_HEIGHT_MEDIUM: 18,
    LINE_HEIGHT_LARGE: 20,
};

/**
 * React Native Customization
 */

// const Text_render = Text.prototype.render;
// const Text_style = { fontFamily: FONT_REGULAR };
// Text.prototype.render = function (...args) {
//   const origin = Text_render.call(this, ...args);
//   return cloneElement(origin, {
//     style: [Text_style, origin.props.style],
//   });
// };

// TextInput.defaultProps.fontFamily = FONT_REGULAR;
// TextInput.defaultProps.selectionColor = COLOR.primary;

/**
 * Custom Icons
 */

// CustomIcon.defaultProps.size = Platform.OS === 'ios' ? 35 : 30;
// CustomIcon.defaultProps.color = COLOR.primary;

/**
 * Common Styles
 */

export const STYLE = StyleSheet.create({
    // General-purpose styles
    fit: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: COLOR.text,
    },
    textSecondary: {
        color: COLOR.textSecondary,
    },
    textInverse: {
        color: COLOR.textInverse,
    },

    // Shared styles

    // textRegular: {
    //   fontFamily: FONT_REGULAR,
    // },

    // textLight: {
    //   fontFamily: FONT_LIGHT,
    // },

    // textBold: {
    //   fontFamily: FONT_BOLD,
    // },
});
