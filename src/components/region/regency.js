import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FlatList, StyleSheet, View } from 'react-native';
import { Dialog, DialogButton, DialogFooter, DialogTitle, SlideAnimation } from 'react-native-popup-dialog';

import { $loadRegency } from '../../services/region/action';
import { COLOR, FONT_SIZE, PADDING, SSP } from '../../common/styles';
import Item from './item';

class RegencyPicker extends Component {
    constructor(props) {
        super(props);
        this.typingTimer = false;

        this.state = {
            selected: {},
            search: '',
        };
    }

    _onLoad(search = '') {
        this.props.$loadRegency(1, 100, search);
    }

    componentDidMount() {
        this._onLoad();
    }

    onSelected = () => {
        this.props.onSubmit(this.state.selected);
    };

    _renderFooter() {
        return (
            <DialogFooter>
                <DialogButton
                    text="BATAL"
                    onPress={this.props.onCancel}
                    style={styles.button}
                    key="button-1"
                    textStyle={{
                        ...styles.textButton,
                        color: COLOR.danger,
                    }}
                />
                <DialogButton
                    text="PILIH"
                    onPress={this.onSelected}
                    style={styles.button}
                    key="button-2"
                    textStyle={{
                        ...styles.textButton,
                        color: !this.state.selected.id ? COLOR.darkGray : COLOR.success,
                    }}
                    disabled={!this.state.selected.id}
                />
            </DialogFooter>
        );
    }

    _onPressItem = (item) => {
        this.setState({ selected: item });
    };

    _flatItem = ({ index, item }) => (
        <Item onSelected={this._onPressItem} selected={this.state.selected.id === item.id} item={item} />
    );

    _flatKey = (item, index) => index.toString();

    _flatReff = (ref) => (this.listRef = ref);

    render() {
        const { visible, regencies, onCancel } = this.props;

        return (
            <Dialog
                visible={visible}
                onTouchOutside={onCancel}
                footer={this._renderFooter()}
                dialogAnimation={new SlideAnimation()}
                onHardwareBackPress={onCancel}
                width={0.8}
                dialogTitle={
                    <DialogTitle
                        title="PILIH KOTA / KABUPATEN"
                        hasTitleBar={true}
                        style={styles.titleContainer}
                        textStyle={styles.textTitle}
                    />
                }
            >
                <View style={styles.regencyContainer}>
                    <FlatList
                        data={regencies}
                        keyExtractor={this._flatKey}
                        renderItem={this._flatItem}
                        extraData={this.state}
                        initialNumToRender={15}
                        showsVerticalScrollIndicator={true}
                    />
                </View>
            </Dialog>
        );
    }
}

RegencyPicker.propTypes = {
    visible: PropTypes.bool,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.Activity.page,
        regencies: state.Region.regencies,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators({ $loadRegency }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(RegencyPicker);

const styles = StyleSheet.create({
    regencyContainer: {
        height: 300,
    },
    titleContainer: {
        paddingVertical: 10,
    },
    textTitle: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.darkGray,
        fontFamily: SSP.semi_bold,
    },
    dialogContainer: {
        padding: 0,
        paddingHorizontal: 0,
        margin: 0,
    },
    textContent: {
        textAlign: 'center',
        fontSize: FONT_SIZE.SMALL,
        fontFamily: SSP.semi_bold,
    },
    button: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    textButton: {
        fontSize: FONT_SIZE.SMALL,
        color: COLOR.success,
        fontFamily: SSP.semi_bold,
    },
    searchContainer: {
        backgroundColor: 'white',
        paddingHorizontal: PADDING.PADDING_HORIZONTAL_CONTAINER,
        margin: 0,
        borderTopWidth: 0,
        borderColor: COLOR.searchBorder,
        borderBottomWidth: 0.7,
    },
    inputContainer: {
        backgroundColor: COLOR.light,
        borderColor: COLOR.searchBorder,
        borderWidth: 0.5,
        borderBottomWidth: 0.5,
    },
    searchInput: {
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.black,
        padding: 0,
    },
    itemContainer: {
        padding: 10,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: COLOR.light,
    },
});
