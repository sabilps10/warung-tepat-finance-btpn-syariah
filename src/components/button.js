import React, { PureComponent } from 'react';
import { Button } from 'react-native-elements';
import { Dimensions, StyleSheet, View } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../common/styles';
import PropTypes from 'prop-types';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 40;

export default class PkaButton extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <Button
                    buttonStyle={{
                        ...styles.button,
                        backgroundColor: this.props.color,
                    }} onPress={this.props.onPress}
                    title={this.props.title.toUpperCase()}
                    loading={this.props.loading}
                    disabled={this.props.loading}
                    titleStyle={styles.text}
                />
            </View>
        );
    }
}

PkaButton.propTypes = {
    onPress: PropTypes.func,
    title: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    loading: PropTypes.bool,
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        height: MARGIN,
        borderRadius: 20,
        zIndex: 100,
        width: DEVICE_WIDTH - MARGIN,
    },
    text: {
        color: COLOR.white,
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        letterSpacing: 0.8,
    },
});
