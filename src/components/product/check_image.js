export const checkImage = (image_default) => {
    if (image_default === '') {
        return 'https://via.placeholder.com/300?width=300&height=300';
    } else {
        return image_default + '?width=300&height=300';
    }
};
