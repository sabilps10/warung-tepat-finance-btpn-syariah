import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Dimensions, Text } from 'react-native';
import { COLOR, FONT_SIZE, SSP } from '../common/styles';
import Icon from '../components/custom_icon';
import { HeaderBackButton } from 'react-navigation-stack';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const deviceWidth = Dimensions.get('screen').width;

class Headers extends Component {
    constructor(props) {
        super(props);
    }

    toCart = () => {
        this.props.navigation.push('Cart');
    };

    toNotif = () => {
        this.props.navigation.push('Notification');
    };

    render() {
        let { counter, counterMessage } = this.props;

        return (
            <View
                style={this.props.solid ? { ...styles.container, ...styles.containerSolid } : { ...styles.container }}
            >
                <View>
                    <HeaderBackButton
                        tintColor={COLOR.secondary}
                        style={styles.backBtn}
                        onPress={() => this.props.navigation.goBack(null)}
                    />
                </View>
                <View style={styles.searchbar} />
                <View style={styles.iconbar}>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.toCart}>
                        <View style={styles.containerCounter}>
                            <Icon name="bag" color={COLOR.secondary} size={20} />
                            <View style={styles.counter}>
                                <Text style={styles.counterText}>{counter !== undefined ? counter.total : 0}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.toNotif}>
                        <View style={styles.containerCounter}>
                            <Icon name="bell-o" color={COLOR.secondary} size={20} />
                            {counterMessage !== 0 ? (
                                <View style={styles.counter}>
                                    <Text style={styles.counterText}>{counterMessage}</Text>
                                </View>
                            ) : null}
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

Headers.propTypes = {
    navigation: PropTypes.object.isRequired,
    solid: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {
    return {
        counter: state.Cart.counter,
        counterMessage: state.Notification.countMessage,
    };
};

export default connect(mapStateToProps)(Headers);

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-around',
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 0,
        paddingLeft: 0,
        position: 'absolute',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0,
        borderBottomWidth: 0,
        backgroundColor: 'transparent',
        width: deviceWidth,
        flexDirection: 'row',
        alignItems: 'center',
    },
    containerSolid: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        elevation: 1,
    },
    backBtn: {
        margin: 0,
        padding: 0,
    },
    searchbar: {
        justifyContent: 'center',
        width: deviceWidth - 130,
        height: 30,
    },
    iconbar: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: 62,
    },
    textSearch: {
        color: COLOR.darkGray,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    },
    containerCounter: { flexDirection: 'row' },
    counter: {
        position: 'relative',
        width: 18,
        height: 15,
        right: 10,
        borderRadius: 50,
        bottom: 10,
        backgroundColor: COLOR.red,
        zIndex: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
    counterText: {
        color: COLOR.white,
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
});
