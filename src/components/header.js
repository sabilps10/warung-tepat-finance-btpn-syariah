import React, { PureComponent } from 'react';
import { Icon } from 'react-native-elements';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Countly from 'countly-sdk-react-native-bridge';

import { COLOR, FONT_SIZE, SSP } from '../common/styles';
import Icons from '../components/custom_icon';

const deviceWidth = Dimensions.get('screen').width;

class Headers extends PureComponent {
    toSearch = () => {
        this.props.navigation.navigate('CatalogSearch');
    };

    toCart = () => {
        this.props.navigation.navigate('Cart');
    };

    toNotif = () => {
        const { session } = this.props;
        const { user } = session;
        const eventNotification = { eventName: 'Open Notif' };
        eventNotification.segments = {
            Phone: user.username,
        };
        Countly.sendEvent(eventNotification);
        this.props.navigation.navigate('Notification');
    };

    render() {
        let { counter, counterMessage } = this.props;

        return (
            <View style={styles.container}>
                {!this.props.title ? (
                    <View style={styles.searchbar}>
                        <TouchableOpacity activeOpacity={0.5} style={styles.search} onPress={this.toSearch}>
                            <Icon name="search" size={19} color={COLOR.darkGray} />
                            <Text style={styles.textSearch}>&nbsp;Ketik di sini untuk cari Produk ... </Text>
                        </TouchableOpacity>
                    </View>
                ) : (
                    <Text style={styles.title}>{this.props.title}</Text>
                )}
                <View style={styles.containerIcon}>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.toCart}>
                        <View style={styles.containerCounter}>
                            <Icons name="bag" type="Feather" color={COLOR.secondary} size={20} />
                            <View style={styles.counter}>
                                <Text style={styles.counterText}>{counter !== undefined ? counter.total : 0}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.toNotif}>
                        <View style={styles.containerCounter}>
                            <Icons name="bell-o" color={COLOR.secondary} size={20} />
                            {counterMessage !== 0 ? (
                                <View style={styles.counter}>
                                    <Text style={styles.counterText}>{counterMessage}</Text>
                                </View>
                            ) : null}
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

Headers.propTypes = {
    navigation: PropTypes.object.isRequired,
    title: PropTypes.string,
};

const mapStateToProps = (state, ownProps) => {
    return {
        counter: state.Cart.counter,
        counterMessage: state.Notification.countMessage,
        session: state.Auth.session,
    };
};

export default connect(mapStateToProps)(Headers);

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR.white,
        elevation: 4,
        height: 55,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        paddingLeft: 15,
        flexDirection: 'row',
        width: deviceWidth,
        alignItems: 'center',
    },
    searchbar: {
        justifyContent: 'center',
        width: deviceWidth - 95,
        height: 30,
        marginLeft: 5,
        marginRight: 8,
    },
    search: {
        backgroundColor: COLOR.light,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 4,
        borderRadius: 3,
    },
    textSearch: {
        color: COLOR.darkGray,
        fontFamily: SSP.regular,
        fontSize: FONT_SIZE.SMALL,
    },
    title: {
        fontFamily: SSP.semi_bold,
        fontSize: FONT_SIZE.MEDIUM,
        color: COLOR.primary,
        width: deviceWidth - 95,
    },
    containerIcon: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    containerCounter: { flexDirection: 'row' },
    counter: {
        position: 'relative',
        width: 18,
        height: 15,
        right: 10,
        borderRadius: 50,
        bottom: 10,
        backgroundColor: COLOR.red,
        zIndex: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
    counterText: {
        color: COLOR.white,
        fontSize: FONT_SIZE.EXTRA_SMALL,
    },
});
