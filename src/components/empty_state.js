import React, { PureComponent } from 'react';
import { StyleSheet, Image, Text, ScrollView, View } from 'react-native';
import { COLOR, FONT_SIZE, NUNITO, MARKOT } from '../common/styles';
import PropTypes from 'prop-types';
import empty from '@assets/img/empty_products.png';

export default class EmptyState extends PureComponent {
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container} refreshControl={this.props.refreshControl}>
                <View>
                    <Image source={empty} style={styles.iconEmptyImage} resizeMode="contain" />
                    <Text style={styles.title}>{this.props.title}</Text>
                    <View>
                        <Text style={styles.emptySubitleText}>{this.props.subtitle1}</Text>
                        <Text style={styles.emptySubitleText}>{this.props.subtitle2}</Text>
                        <Text style={styles.emptySubitleText}>{this.props.subtitle3}</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

EmptyState.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
};

const styles = StyleSheet.create({
    container: {
        height: '100%',
        padding: 0,
        margin: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconEmptyImage: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: 400,
        width: 400,
        marginHorizontal: '2%',
        marginVertical: '-10%',
    },
    title: {
        fontFamily: MARKOT.bold,
        fontSize: 20,
        lineHeight: FONT_SIZE.LINE_HEIGHT_LARGE,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginVertical: '1%',
        marginBottom: '5%',
    },
    emptySubitleText: {
        fontFamily: NUNITO.regular,
        color: COLOR.textGray,
        fontSize: FONT_SIZE.LARGE,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
});
