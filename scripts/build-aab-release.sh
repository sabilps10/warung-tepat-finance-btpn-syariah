#!/usr/bin/env bash

set -xeo pipefail

source $(dirname $0)/config.sh

$(dirname $0)/update-release.sh

cd ./android

./gradlew clean bundleRelease

cd ..

mkdir -p ./build/

cp ./android/app/build/outputs/bundle/release/app-release.aab ./build/app-release.aab

echo -e '\n ==> ./build/app-release.aab'
