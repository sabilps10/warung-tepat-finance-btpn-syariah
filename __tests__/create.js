const address1 = '<%%>';
const address2 = '<%Jalan Roda%>';

const checkAddress = (text) => {
    return text.replace(/[\!\<\%\>]/g, '');
};

test('Address Result will return empty string', () => {
    const addressResult = checkAddress(address1);
    expect(addressResult).toBe('');
});

test('Address Result will be trimmed and show the original address', () => {
    const addressResult = checkAddress(address2);
    expect(addressResult).toBe('Jalan Roda');
});
