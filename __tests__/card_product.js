import { checkImage } from '../src/components/product/check_image';

const image_default = 'https://lmd-storage.apps.btpnsyariah.com/bucket03/beta/v1/catalog/48.jpg';

test('Image is empty, should output the placeholder', () => {
    const imageUri = checkImage('');
    expect(imageUri).toBe('https://via.placeholder.com/300?width=300&height=300');
});

test('Image is available, should output the correct image', () => {
    const imageUri = checkImage(image_default);
    expect(imageUri).toBe(
        'https://lmd-storage.apps.btpnsyariah.com/bucket03/beta/v1/catalog/48.jpg?width=300&height=300',
    );
});
